use std::{
    collections::HashSet,
    path::Path,
    sync::Arc,
};

use crossbeam::queue::SegQueue;
use legion::prelude::*;
use notify::{
    {EventKind, RecursiveMode, Watcher},
    event::{CreateKind, ModifyKind},
};
use shrev::*;

use crate::core::time::Ticker;

/// The directory where where game assets are stored.
///
pub const ASSET_DIRECTORY: &str = "./res";


/// An event emitted when a file inside the asset directory is modified.
///
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct FileChangedEvent {
    pub file: String,
}


/// # Resource
///
/// Data used by the file change listener system.
///
struct FileChangeListenerData {
    /// A queue that stores `FileChangedEvent`s ready for processing by the system.
    ///
    queue: Arc<SegQueue<FileChangedEvent>>,

    /// A ticker used to determine when the system is ran.
    ///
    ticker: Ticker,

    /// The watcher that listens for file system changes.
    ///
    #[allow(unused)]
    watcher: notify::ReadDirectoryChangesWatcher,
}


/// # Initializer
///
/// Initializes resources used by the file change listener system.
///
/// - `FileChangeListenerData` - internal data used by the system.
/// - `EventChannel::<FileChangedEvent>` - the channel that `FileChangeEvent`s are written to.
///
pub fn init_file_change_listener(world: &mut World) {
    // Create send/receive queue pointers.
    let queue = Arc::new(crossbeam::queue::SegQueue::new());
    let remote = Arc::clone(&queue);

    // The system runs every 200ms.
    let ticker = Ticker::new(200);

    // Create and bind the filesystem listener.
    let canonical_asset_directory = std::path::Path::new(ASSET_DIRECTORY)
        .canonicalize()
        .expect("Could not canonicalize path to asset directory!");
    let mut watcher = notify::immediate_watcher(move |event: Result<notify::Event, _>| {
        if let Ok(event) = event {
            if let EventKind::Create(CreateKind::Any)
            | EventKind::Modify(ModifyKind::Any) = event.kind {
                event.paths.into_iter()
                    .filter_map(|path| path_to_asset_id(&path, &canonical_asset_directory))
                    .map(|file| FileChangedEvent { file })
                    .for_each(|event| remote.push(event));
            }
        }
    }).expect("Could not create file change listener!");
    watcher.watch(ASSET_DIRECTORY, RecursiveMode::Recursive)
        .expect("Could not bind file change listener to asset directory!");

    // Insert resources
    world.resources.insert(FileChangeListenerData { queue, ticker, watcher });
    world.resources.insert(EventChannel::<FileChangedEvent>::default());
}


/// # System
///
/// Listens for file changes and emits `FileChangedEvent`s.
///
pub fn sys_file_change_listener() -> Box<dyn Schedulable> {
    let mut event_set = HashSet::new();
    SystemBuilder::new("AssetFileChangeListenerSystem")
        .write_resource::<FileChangeListenerData>()
        .write_resource::<EventChannel<FileChangedEvent>>()
        .build(move |_, _, (data, channel), _| {
            // The system runs on a set tick rate.
            if data.ticker.tick() {
                // Using HashSet here so duplicate events are only emitted once.
                event_set.extend(std::iter::from_fn(|| data.queue.pop().ok()));
                channel.iter_write(event_set.drain());
            }
        })
}

/// Converts a `&Path` to an asset identifier string.
///
fn path_to_asset_id(path: &Path, canonical_asset_directory: &Path) -> Option<String> {
    path.canonicalize().ok()?
        .strip_prefix(&canonical_asset_directory).ok()?
        .to_string_lossy()
        .replace('\\', "/")
        .into()
}

use std::{
    collections::{HashMap, HashSet, VecDeque},
    num::Wrapping,
};

use crossbeam::{Receiver, Sender};
use legion::prelude::*;
use shrev::*;

use crate::{
    asset::{
        {Asset, Event, Get, Handle, ResultRef, Storage, Weak},
        listener::FileChangedEvent,
        proxy::{Queue, Request},
        storage::HandleLike,
    },
    util::progress::Progress,
};

/// An error produced if an asset could not be loaded successfully.
///
pub type LoadError = Box<dyn std::error::Error + Send + Sync + 'static>;


/// The result of an asset loader task.
/// Contains either the loaded asset or an error returned from the loader.
///
#[allow(type_alias_bounds)]
pub type LoadResult<T: Asset> = Result<T, LoadError>;


/// An asset loader task that returns an `AssetLoadResult` when executed.
/// This is an alias for a pinned, heap allocated dynamic `Future` that can be executed in the
/// `async_std::task` runtime.
///
/// A new task can be constructed using the following syntax:
///
/// ```
/// futures::future::FutureExt::boxed(async move {
///     // Your code here
/// });
/// ```
///
#[allow(type_alias_bounds)]
pub type LoadTask<T: Asset> = futures::future::BoxFuture<'static, LoadResult<T>>;


/// An alias for a tuple used to store asset data in a load queue.
/// Three important pieces of data are stored:
///
/// 1. The unique `Id` of the asset.
/// 2. The `Handle` reserved for the asset in a storage instance.
/// 3. A `Progress` instance used to report loading progress.
///
#[allow(type_alias_bounds)]
pub type Queued<T: Asset> = (T::Id, Handle<T>, Progress);


/// A function to execute with an asset when it finishes loading.
///
#[allow(type_alias_bounds)]
pub(super) type With<T: Asset> = Box<dyn for<'a> FnOnce(ResultRef<'a, T>) + Send + Sync + 'static>;


/// A loader used to queue load requests for assets of type `T`.
/// Handles to already requested assets are also stored and returned in case a load is requested
/// multiple times for the same asset.
///
pub struct Loader<T: Asset> {
    queued: VecDeque<Queued<T>>,
    handles: HashMap<T::Id, Handle<T>>,
    ids: HashMap<Weak<T>, T::Id>,
    current_dispatch: HashMap<Weak<T>, u64>,
    dispatch_index: Wrapping<u64>,
    dependencies: HashMap<String, HashSet<T::Id>>,
    result_sender: Sender<(u64, Handle<T>, LoadResult<T>)>,
    result_receiver: Receiver<(u64, Handle<T>, LoadResult<T>)>,
    rid_file_change: ReaderId<FileChangedEvent>,
    with_queue: Vec<(Handle<T>, With<T>)>,
}

impl<T: Asset> Loader<T> {
    pub fn new(world: &mut World) -> Self {
        let (result_sender, result_receiver) = crossbeam::unbounded();
        let rid_file_change = world.resources
            .get_mut_or_default::<EventChannel<_>>()
            .unwrap()
            .register_reader();
        Self {
            queued: Default::default(),
            handles: Default::default(),
            ids: Default::default(),
            current_dispatch: Default::default(),
            dispatch_index: Wrapping(0),
            dependencies: Default::default(),
            result_sender,
            result_receiver,
            rid_file_change,
            with_queue: Default::default(),
        }
    }

    /// Requests the asset with the provided Id to be loaded.
    ///
    /// If the asset is already loading or has finished loading, the previously reserved handle
    /// is returned to the user.
    ///
    /// Otherwise a new handle is reserved and the asset is queued for loading.
    ///
    pub fn load(&mut self, id: T::Id, storage: &mut Storage<T>) -> Handle<T> {
        // If there is already a handle reserved for this asset id, return it.
        if let Some(handle) = self.handles.get(&id) {
            return handle.clone();
        }

        // Register this asset for updates on file change if it has a file name.
        if let Some(virtual_file) = T::virtual_file_name(&id) {
            self.add_virtual_file_dependency(id.clone(), virtual_file);
        }

        // Reserve a new handle.
        let (handle, progress) = storage.reserve();
        self.handles.insert(id.clone(), handle.clone());
        self.ids.insert(handle.weak(), id.clone());

        // Push the asset data to the load queue.
        self.queued.push_back((id, handle.clone(), progress));

        // Return the new handle.
        handle
    }

    /// Upgrades a `WeakHandle` to a `Handle`.
    ///
    /// Returns `None` if the asset that the handle points to has been deleted.
    ///
    pub fn upgrade_weak(&self, weak: Weak<T>) -> Option<Handle<T>> {
        self.handles.get(self.ids.get(&weak)?).cloned()
    }

    /// Process incoming requests from the proxy queue.
    ///
    pub(super) fn process_proxy_requests(
        &mut self,
        proxy: &Queue,
        storage: &mut Storage<T>,
    ) {
        while let Some(request) = proxy.next::<T>() {
            match request {
                Request::Load(id, sender) => {
                    let handle = self.load(id, storage);
                    sender.send(handle).expect("Asset proxy handle channel is closed!");
                }
                Request::With(handle, with) => {
                    if let Get::Finished(result) = storage.get(&handle) {
                        with(result);
                    } else {
                        self.with_queue.push((handle, with));
                    }
                }
                Request::Depend(handle, virtual_file) => {
                    if let Some(id) = self.ids.get(&handle.weak()).cloned() {
                        self.add_virtual_file_dependency(id, virtual_file);
                    }
                }
            }
        }
    }

    /// Read through virtual file change events and queues a load request for assets associated with
    /// them updated virtual files.
    ///
    pub(super) fn process_file_changes(&mut self, channel: &EventChannel<FileChangedEvent>) {
        let mut to_reload = Vec::new();
        for event in channel.read(&mut self.rid_file_change) {
            if let Some(dependencies) = self.dependencies.get(&event.file) {
                for id in dependencies {
                    to_reload.push(id.clone());
                }
            }
        }
        for id in to_reload {
            self.remove_virtual_file_dependencies(&id);
            if let Some(virtual_file) = T::virtual_file_name(&id) {
                self.add_virtual_file_dependency(id.clone(), virtual_file);
            }

            let handle = self.handles
                .get(&id)
                .expect("No handle for asset id!")
                .clone();
            self.queued.push_back((id, handle, Progress::default()));
        }
    }

    /// Dispatch loaders for all queued assets.
    ///
    pub(super) fn process_queued(&mut self, queue: &Queue) {
        for (id, handle, progress) in self.queued.drain(..) {
            // Set dispatch index
            let dispatch_index = self.dispatch_index.0;
            self.current_dispatch.insert(handle.weak(), dispatch_index);
            self.dispatch_index += Wrapping(1);

            // Spawn loader task
            let sender = self.result_sender.clone();
            let proxy = queue.for_asset(handle.clone());
            async_std::task::spawn(async move {
                let result = T::loader(id.clone(), progress, proxy).await;
                if let Err(..) = sender.send((dispatch_index, handle, result)) {
                    spg_warn!("[Asset] Could not send load result!");
                }
            });
        }
    }

    /// Process all load results from the result channel.
    ///
    pub(super) fn process_finished(
        &mut self,
        storage: &mut Storage<T>,
        asset_channel: &mut EventChannel<Event<T>>,
    ) {
        while let Ok((dispatch_index, handle, result)) = self.result_receiver.try_recv() {
            // Check if the loader returned the latest dispatch index.
            if Some(&dispatch_index) == self.current_dispatch.get(&handle.weak()) {
                self.current_dispatch.remove(&handle.weak());
            } else {
                return;
            }

            // Store the load result.
            storage.store(&handle, result);

            // Go through any pending with requests for this asset.
            let new_queue = self.with_queue.drain(..)
                .filter_map(|(h, with)| {
                    if h == handle {
                        if let Get::Finished(result) = storage.get(&handle) {
                            with(result);
                        }
                        None
                    } else {
                        Some((h, with))
                    }
                })
                .collect();
            self.with_queue = new_queue;

            // Emit a `Finished` event.
            asset_channel.single_write(Event::Finished(handle));
        }
    }

    /// Filter out deletable handles and remove them from the handle map and their file names
    /// from the file name map.
    ///
    /// A handle is considered to be deletable when it is unique, meaning that there are no other
    /// handles to the same asset present inside the program.
    ///
    /// Returns an iterator over the removed handles.
    ///
    pub(super) fn process_deletable(
        &mut self,
        storage: &mut Storage<T>,
        asset_channel: &mut EventChannel<Event<T>>,
    ) {
        let deletable = self.handles.iter()
            .filter(|(_, handle)| handle.is_unique())
            .map(|(id, _)| id.clone())
            .collect::<Vec<_>>();
        for id in deletable {
            let handle = self.handles.remove(&id)
                .expect("[Asset] No handle for id!");
            self.ids.remove(&handle.weak())
                .expect("[Asset] No id for handle!");
            self.remove_virtual_file_dependencies(&id);
            storage.delete(&handle);
            asset_channel.single_write(Event::Deleted(handle.weak()));
        }
    }

    /// Add the given virtual file as a dependency for the asset with the given id.
    ///
    fn add_virtual_file_dependency(&mut self, id: T::Id, virtual_file: String) {
        self.dependencies
            .entry(virtual_file)
            .or_default()
            .insert(id);
    }

    /// Remove all dependencies for the asset with the given id.
    ///
    fn remove_virtual_file_dependencies(&mut self, id: &T::Id) {
        self.dependencies.retain(|_, ids| {
            ids.remove(&id);
            !ids.is_empty()
        });
    }
}

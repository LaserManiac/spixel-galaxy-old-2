use std::hash::Hash;

use legion::prelude::*;
use shrev::*;

pub use loader::{Loader, LoadError, LoadResult, LoadTask};
pub use proxy::Proxy;
pub use storage::{Get, Handle, ResultRef, Storage, Weak};

use crate::{
    asset::{
        listener::FileChangedEvent,
        proxy::Queue,
    },
};
pub use crate::util::progress::Progress;

pub mod storage;
pub mod loader;
pub mod proxy;
pub mod listener;


/// An asset type.
///
pub trait Asset: Sized + Send + Sync + 'static {
    /// The type of id this asset uses.
    ///
    type Id: Clone + Send + Sync + Hash + Eq + 'static;

    /// Constructs a loader instance for this asset.
    ///
    fn loader(id: Self::Id, progress: Progress, proxy: Proxy<Self>) -> LoadTask<Self>;

    /// A file name of this asset relative to the "./res" directory.
    /// This is used to reload the asset when the file is updated.
    ///
    fn virtual_file_name(_id: &Self::Id) -> Option<String> { None }
}


/// # Event
///
/// An event in the lifetime of an asset.
///
pub enum Event<T: Asset> {
    /// An asset has finished loading.
    ///
    Finished(Handle<T>),

    /// An asset has been deleted.
    ///
    Deleted(Weak<T>),
}


/// Fetches the loader and the storage for asset type `T` from `world`.
/// Queues a new load with `id` and returns the generated `Handle<T>`.
///
pub fn load<T: Asset, I: Into<T::Id>>(id: I, world: &mut legion::prelude::World) -> Handle<T> {
    let mut loader = world.resources
        .get_mut::<Loader<T>>()
        .expect("No asset loader resource!");
    let mut storage = world.resources
        .get_mut::<Storage<T>>()
        .expect("No asset storage resource!");
    loader.load(id.into(), &mut storage)
}


/// # Initializer
///
/// Initializes the asset manager for assets of type `T`.
///
pub fn init_asset_manager<T: Asset>(world: &mut World) {
    let loader = Loader::<T>::new(world);
    world.resources.insert(loader);
    world.resources.insert(Storage::<T>::default());
    world.resources.insert(EventChannel::<Event<T>>::default());
    world.resources.get_or_default::<Queue>();
}

/// # System
///
/// Asset manager system for assets of type `T`.
///
pub fn sys_asset_manager<T: Asset>(asset_name: &str) -> Box<dyn Schedulable> {
    // Create the system.
    let system_name = format!("AssetManagerSystem<{}>", asset_name);
    legion::prelude::SystemBuilder::new(system_name)
        .write_resource::<Loader<T>>()
        .write_resource::<Storage<T>>()
        .write_resource::<EventChannel<Event<T>>>()
        .read_resource::<Queue>()
        .read_resource::<EventChannel<FileChangedEvent>>()
        .build(move |_, _, resources, _| {
            let (loader, storage, asset_channel, proxy, file_channel) = resources;
            loader.process_proxy_requests(proxy, storage);
            loader.process_file_changes(file_channel);
            loader.process_queued(proxy);
            loader.process_finished(storage, asset_channel);
            loader.process_deletable(storage, asset_channel);
        })
}

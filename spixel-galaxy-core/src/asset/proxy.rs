use std::{
    any::{Any, TypeId},
    collections::HashMap,
    sync::{Arc, RwLock},
};

use crossbeam::queue::SegQueue;
use futures::channel::oneshot::Sender as OneshotSender;

use crate::asset::{
    {Asset, ResultRef},
    loader::With,
    storage::Handle,
};

/// A proxy request that executes a certain action on the loader thread.
///
pub(super) enum Request<T: Asset> {
    /// Loads an asset and sends back the handle.
    ///
    Load(T::Id, OneshotSender<Handle<T>>),

    /// Executes a callback and sends back the result.
    ///
    With(Handle<T>, With<T>),

    /// Adds a dependency for this asset.
    ///
    Depend(Handle<T>, String),
}


/// An alias for a proxy queue for any asset type.
///
type BoxedDynQueue = Box<dyn Any + Send + Sync + 'static>;


/// A proxy used to make requests from the loader thread.
///
pub struct Proxy<T: Asset> {
    queue: Queue,
    handle: Handle<T>,
}

impl<T: Asset> std::ops::Deref for Proxy<T> {
    type Target = Queue;

    fn deref(&self) -> &Self::Target {
        &self.queue
    }
}

impl<T: Asset> std::ops::DerefMut for Proxy<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.queue
    }
}

impl<T: Asset> Proxy<T> {
    /// Add a virtual file dependency for this asset.
    ///
    /// This will make the asset reload whenever the file is updated.
    ///
    pub fn depend(&self, file: String) {
        self.push(Request::Depend(self.handle.clone(), file));
    }
}


/// # Resource
///
/// A queue that holds proxy requests until they are read.
///
#[derive(Clone, Default)]
pub struct Queue {
    queues: Arc<RwLock<HashMap<TypeId, BoxedDynQueue>>>,
}

impl Queue {
    /// Queue a load for the given asset id, and return the generated handle.
    ///
    pub async fn load<T: Asset>(&mut self, id: T::Id) -> Handle<T> {
        let (sender, receiver) = futures::channel::oneshot::channel();
        self.push::<T>(Request::Load(id, sender));
        receiver.await
            .expect("Asset proxy handle channel is closed!")
    }

    /// Execute the provided callback with the provided asset and return the result.
    ///
    /// If the asset is not loaded yet, the callback is executed when it finishes loading.
    ///
    pub async fn with<T, R, F>(&mut self, handle: Handle<T>, with: F) -> R
        where T: Asset,
              R: Send + 'static,
              F: for<'a> FnOnce(ResultRef<'a, T>) -> R + Send + Sync + 'static {
        let (sender, receiver) = futures::channel::oneshot::channel();
        let with = Box::new(move |result: ResultRef<T>| {
            let returned = with(result);
            sender.send(returned).ok()
                .expect("Asset proxy handle channel is closed!");
        });
        self.push::<T>(Request::With(handle, with));
        receiver.await
            .expect("Asset proxy handle channel is closed!")
    }

    /// Queue a load for the given asset id, execute the provided callback when it finishes
    /// loading, and return the result.
    ///
    pub async fn load_and_with<T, R, F>(&mut self, id: T::Id, with: F) -> R
        where T: Asset,
              R: Send + 'static,
              F: for<'a> FnOnce(ResultRef<'a, T>) -> R + Send + Sync + 'static {
        let handle = self.load(id).await;
        self.with(handle, with).await
    }

    /// Create a new proxy for a specific asset from this queue
    ///
    pub(super) fn for_asset<T: Asset>(&self, handle: Handle<T>) -> Proxy<T> {
        Proxy { queue: self.clone(), handle }
    }

    /// Push a new request to the appropriate queue.
    ///
    pub(super) fn push<T: Asset>(&self, request: Request<T>) {
        self.queues.write()
            .expect("RwLock poisoned!")
            .entry(TypeId::of::<T>())
            .or_insert_with(|| Box::new(SegQueue::<Request<T>>::new()) as BoxedDynQueue)
            .downcast_ref::<SegQueue<Request<T>>>()
            .expect("Wrong type of asset proxy queue!")
            .push(request);
    }

    /// Pop the next request from the appropriate queue.
    ///
    pub(super) fn next<T: Asset>(&self) -> Option<Request<T>> {
        self.queues.read()
            .expect("RwLock poisoned!")
            .get(&TypeId::of::<T>())?
            .downcast_ref::<SegQueue<Request<T>>>()
            .expect("Wrong type of asset proxy queue!")
            .pop()
            .ok()
    }
}

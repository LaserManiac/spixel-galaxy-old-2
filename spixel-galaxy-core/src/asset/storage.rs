use std::{
    marker::PhantomData,
    sync::Arc,
};

use slotmap::DenseSlotMap;

use crate::{
    asset::{Asset, LoadResult},
    util::progress::Progress,
};


/// A trait implemented by types that can be used as asset handles.
///
pub trait HandleLike<A> {
    /// Returns the internal asset storage key.
    ///
    fn key(&self) -> Key;

    /// Constructs a weak handle for the asset.
    ///
    fn weak(&self) -> Weak<A> {
        Weak(self.key(), PhantomData)
    }

    /// Compares two handle-like types by checking their keys.
    ///
    fn is_same(&self, other: &dyn HandleLike<A>) -> bool {
        self.key() == other.key()
    }
}


/// A handle pointing to an asset inside an `AssetStorage` instance.
///
/// **IMPORTANT:** A handle is not tied to a particular instance of `AssetStorage`, so care must be
/// taken to ensure that a handle is only used with the instance from which it originated. In
/// regular use this should not be a problem, since only a single instance should ever exist,
/// as a resource inside the game world.
///
pub struct Handle<T>(Arc<Key>, PhantomData<*const T>);

impl<T: Asset> Handle<T> {
    /// A handle is unique when there are no other handles for the same asset.
    ///
    pub fn is_unique(&self) -> bool {
        Arc::strong_count(&self.0) == 1
    }
}

impl<T> HandleLike<T> for Handle<T> {
    fn key(&self) -> Key {
        *self.0
    }
}

impl<T> Clone for Handle<T> {
    fn clone(&self) -> Self {
        Handle(Arc::clone(&self.0), PhantomData)
    }
}

impl<A, T: HandleLike<A>> PartialEq<T> for Handle<A> {
    fn eq(&self, other: &T) -> bool {
        self.is_same(other)
    }
}

impl<T> Eq for Handle<T> {}

impl<T> std::fmt::Debug for Handle<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.key().fmt(f)
    }
}

impl<T> std::hash::Hash for Handle<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.key().hash(state)
    }
}

/// A handle is thread safe because it uses an `Arc` internally.
///
unsafe impl<T: Asset> Send for Handle<T> {}

unsafe impl<T: Asset> Sync for Handle<T> {}


/// A weak handle for an asset.
/// This handle does not keep the asset alive like a regular handle.
/// To create a weak handle from a regular handle, use `Handle::weak()`
/// To convert a weak handle back to a regular handle, use `Loader::upgrade_weak()`.
///
pub struct Weak<T>(Key, PhantomData<*const T>);

impl<T> HandleLike<T> for Weak<T> {
    fn key(&self) -> Key {
        self.0
    }
}

impl<T: Asset> Clone for Weak<T> {
    fn clone(&self) -> Self {
        Self(self.0, PhantomData)
    }
}

impl<T: Asset> Copy for Weak<T> {}

impl<A, T: HandleLike<A>> PartialEq<T> for Weak<A> {
    fn eq(&self, other: &T) -> bool {
        self.is_same(other)
    }
}

impl<T> Eq for Weak<T> {}

impl<T> std::fmt::Debug for Weak<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.key().fmt(f)
    }
}

impl<T> std::hash::Hash for Weak<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.key().hash(state)
    }
}

/// A weak handle is thread safe because it can never directly access the asset.
///
unsafe impl<T: Asset> Send for Weak<T> {}

unsafe impl<T: Asset> Sync for Weak<T> {}


/// A register used to track the asset lifetime state.
///
enum Register<T: Asset> {
    /// While an asset is loading, a progress tracker is stored here.
    ///
    Loading(Progress),

    /// When an asset is loaded, the result is stored here.
    ///
    Finished(LoadResult<T>),
}


/// An asset loading error.
///
pub type AssetError = dyn std::error::Error + Send + Sync + 'static;


/// A result that holds a reference to either the loaded asset or the loading error.
///
#[allow(type_alias_bounds)]
pub type ResultRef<'a, T: Asset> = Result<&'a T, &'a AssetError>;


/// A getter struct that holds either the loading progress of an asset, or a result reference.
///
pub enum Get<'a, T: Asset> {
    Loading(f32),
    Finished(ResultRef<'a, T>),
}

impl<'a, T: Asset> Get<'a, T> {
    /// Returns the loading progress if the asset is in the loading stage.
    ///
    pub fn progress(&self) -> Option<f32> {
        if let Get::Loading(progress) = self {
            Some(*progress)
        } else {
            None
        }
    }

    /// Returns the asset if it was successfully loaded.
    ///
    pub fn ok(&self) -> Option<&'a T> {
        if let Get::Finished(ResultRef::Ok(asset)) = self {
            Some(*asset)
        } else {
            None
        }
    }

    /// Returns the loading error if the asset could not be loaded.
    ///
    pub fn err(&self) -> Option<&'a AssetError> {
        if let Get::Finished(ResultRef::Err(err)) = self {
            Some(*err)
        } else {
            None
        }
    }
}


slotmap::new_key_type! {
    /// A key type for the internal asset storage slot map.
    ///
    pub struct Key;
}


/// A storage structure used to store assets of type `T` and access them using `Handle` instances.
/// The assets are stored inside `Register` instances which track their lifetime state.
///
pub struct Storage<T: Asset> {
    assets: DenseSlotMap<Key, Register<T>>,
}

/// The default constructor creates an empty storage instance.
///
impl<T: Asset> Default for Storage<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Asset> Storage<T> {
    /// Returns a new empty `AssetStorage` instance.
    ///
    pub fn new() -> Self {
        Self {
            assets: Default::default(),
        }
    }

    /// Get a reference to the asset state.
    ///
    pub fn get(&self, handle: &Handle<T>) -> Get<T> {
        self.get_weak(handle.weak())
            .expect("Invalid asset handle!")
    }

    /// Get a reference to the asset state using a weak handle.
    ///
    /// Returns `None` if the handle is no longer valid.
    ///
    pub fn get_weak(&self, Weak(index, ..): Weak<T>) -> Option<Get<T>> {
        match self.assets.get(index)? {
            Register::Loading(progress) => Some(Get::Loading(progress.get())),
            Register::Finished(result) => Some(Get::Finished(result.as_ref()
                .map_err(|e| std::ops::Deref::deref(e)))),
        }
    }

    /// Creates a new asset register and a handle for it.
    /// The register is initialized to the `Loading` state with a new `Progress` instance.
    ///
    /// Returns the newly created handle and the progress tracker that should be used to report
    /// progress for this particular asset instance.
    ///
    pub(super) fn reserve(&mut self) -> (Handle<T>, Progress) {
        let progress = Progress::default();
        let register = Register::Loading(progress.clone());
        let index = self.assets.insert(register);
        let handle = Handle(Arc::new(index), PhantomData);
        (handle, progress)
    }

    /// Stores a load result into the appropriate register using the provided handle.
    /// If the result variant is `Err`, the asset register is set to the `Error` state.
    /// If the result variant is `Ok`, the register is set to the `Loaded` state.
    ///
    pub(super) fn store(&mut self, handle: &Handle<T>, result: LoadResult<T>) {
        let register = self.assets.get_mut(*handle.0)
            .expect("Invalid handle!");
        *register = Register::Finished(result);
    }

    /// Deleted an asset register from the storage using the provided handle.
    ///
    pub(super) fn delete(&mut self, handle: &Handle<T>) {
        self.assets.remove(*handle.0);
    }
}

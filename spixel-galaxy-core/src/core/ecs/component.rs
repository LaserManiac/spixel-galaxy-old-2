#[derive(Clone, Eq, PartialEq)]
pub struct Name {
    pub value: String,
}

impl Name {
    pub fn new(value: impl ToString) -> Self {
        Self { value: value.to_string() }
    }
}


#[derive(Copy, Clone, PartialEq)]
pub struct Position2D {
    pub x: f32,
    pub y: f32,
}

impl Position2D {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}


#[derive(Copy, Clone, PartialEq)]
pub struct Size2D {
    pub width: f32,
    pub height: f32,
}

impl Size2D {
    pub fn new(width: f32, height: f32) -> Self {
        Self { width, height }
    }
}

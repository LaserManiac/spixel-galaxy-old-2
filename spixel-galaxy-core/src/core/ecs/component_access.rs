use legion::{
    borrow::{Ref, RefMut},
    prelude::*,
    storage::Component,
    system::SubWorld,
};

pub trait ComponentAccess {
    fn get_component<T: Component>(&self, entity: Entity) -> Option<Ref<T>>;
    fn get_component_mut<T: Component>(&mut self, entity: Entity) -> Option<RefMut<T>>;
    unsafe fn get_component_mut_unchecked<T: Component>(&self, entity: Entity) -> Option<RefMut<T>>;
}

impl ComponentAccess for World {
    fn get_component<T: Component>(&self, entity: Entity) -> Option<Ref<T>> {
        self.get_component(entity)
    }

    fn get_component_mut<T: Component>(&mut self, entity: Entity) -> Option<RefMut<T>> {
        self.get_component_mut(entity)
    }

    unsafe fn get_component_mut_unchecked<T: Component>(&self, entity: Entity) -> Option<RefMut<T>> {
        self.get_component_mut_unchecked(entity)
    }
}

impl ComponentAccess for SubWorld {
    fn get_component<T: Component>(&self, entity: Entity) -> Option<Ref<T>> {
        self.get_component(entity)
    }

    fn get_component_mut<T: Component>(&mut self, entity: Entity) -> Option<RefMut<T>> {
        self.get_component_mut(entity)
    }

    unsafe fn get_component_mut_unchecked<T: Component>(&self, entity: Entity) -> Option<RefMut<T>> {
        self.get_component_mut_unchecked(entity)
    }
}

impl ComponentAccess for super::SystemWorld<'_, '_> {
    fn get_component<T: Component>(&self, entity: Entity) -> Option<Ref<T>> {
        self.world.get_component(entity)
    }

    fn get_component_mut<T: Component>(&mut self, entity: Entity) -> Option<RefMut<T>> {
        self.world.get_component_mut(entity)
    }

    unsafe fn get_component_mut_unchecked<T: Component>(&self, entity: Entity) -> Option<RefMut<T>> {
        self.world.get_component_mut_unchecked(entity)
    }
}

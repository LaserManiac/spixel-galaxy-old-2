use legion::prelude::*;
use legion::system::SubWorld;

pub trait EntityAlive {
    fn is_alive(&self, entity: Entity) -> bool;
}

impl EntityAlive for World {
    fn is_alive(&self, entity: Entity) -> bool {
        self.is_alive(entity)
    }
}

impl EntityAlive for SubWorld {
    fn is_alive(&self, entity: Entity) -> bool {
        self.is_alive(entity)
    }
}

impl EntityAlive for super::SystemWorld<'_, '_> {
    fn is_alive(&self, entity: Entity) -> bool {
        self.world.is_alive(entity)
    }
}

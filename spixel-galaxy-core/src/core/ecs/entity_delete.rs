use legion::prelude::*;

pub trait EntityDelete {
    fn delete(&mut self, entity: Entity);
}

impl EntityDelete for World {
    fn delete(&mut self, entity: Entity) {
        World::delete(self, entity);
    }
}

impl EntityDelete for CommandBuffer {
    fn delete(&mut self, entity: Entity) {
        CommandBuffer::delete(self, entity);
    }
}

impl EntityDelete for super::SystemWorld<'_, '_> {
    fn delete(&mut self, entity: Entity) {
        self.cmd.delete(entity);
    }
}

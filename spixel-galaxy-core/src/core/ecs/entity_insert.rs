use legion::{
    filter::{ChunksetFilterData, Filter},
    prelude::*,
    world::{IntoComponentSource, TagLayout, TagSet},
};

pub trait EntityInsert {
    fn insert<T, C>(&mut self, tags: T, components: C)
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static;
}

impl EntityInsert for World {
    fn insert<T, C>(&mut self, tags: T, components: C)
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static {
        World::insert(self, tags, components);
    }
}

impl EntityInsert for CommandBuffer {
    fn insert<T, C>(&mut self, tags: T, components: C)
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static {
        CommandBuffer::insert(self, tags, components);
    }
}

impl EntityInsert for super::SystemWorld<'_, '_> {
    fn insert<T, C>(&mut self, tags: T, components: C)
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static {
        self.cmd.insert(tags, components);
    }
}


pub trait EntityInsertImmediate {
    fn insert<T, C>(&mut self, tags: T, components: C) -> &[Entity]
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static;
}

impl EntityInsertImmediate for World {
    fn insert<T, C>(&mut self, tags: T, components: C) -> &[Entity]
        where T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>> + 'static,
              C: IntoComponentSource + 'static {
        World::insert(self, tags, components)
    }
}

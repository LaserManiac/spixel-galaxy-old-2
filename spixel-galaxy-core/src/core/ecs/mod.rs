use std::sync::Arc;

use crossbeam::queue::SegQueue;
pub use legion::event::EntityEvent;
use legion::prelude::*;
use legion::system::SubWorld;
use shrev::*;

pub use self::component::{Name, Position2D, Size2D};
pub use self::component_access::ComponentAccess;
pub use self::entity_alive::EntityAlive;
pub use self::entity_delete::EntityDelete;
pub use self::entity_insert::{EntityInsert, EntityInsertImmediate};

pub mod component_access;
pub mod component;
pub mod entity_alive;
pub mod entity_delete;
pub mod entity_insert;


/// Adds an initialized flag to the world for this particular initializer.
/// If the flag already exists, it returns from the current function;
///
#[macro_export]
macro_rules! initialize_once {
    ($world:ident) => {
        struct Initialized;
        if $world.resources.contains::<Initialized>() { return; }
        $world.resources.insert(Initialized);
    };
    ($world:ident, $ret:expr) => {
        struct Initialized;
        if $world.resources.contains::<Initialized>() { return $ret; }
        $world.resources.insert(Initialized);
    }
}


/// # Resource
///
/// A queue used to hold entity events captured from the world entity channel.
///
struct EntityEventCaptureQueue(Arc<SegQueue<legion::event::EntityEvent>>);

/// # Initializer
///
/// Initializes resources needed to send entity events between the world callback thread and the
/// system thread.
///
/// - `EntityEventCaptureQueue` - the queue that world entity events are pushed to for processing.
/// - `EventChannel<EntityEvent>` - the channel that entity events are written to.
///
pub fn init_entity_event_capture(world: &mut World) {
    let queue = Arc::new(SegQueue::new());
    let remote = Arc::clone(&queue);
    world.resources.get_or_default::<EventChannel<EntityEvent>>();
    world.resources.insert(EntityEventCaptureQueue(queue));
    world.entity_channel().bind_exec(Box::new(move |event| {
        remote.push(event);
        None
    }));
}

/// # System
///
/// Reads `EntityEvent`s from the `EntityEventCaptureQueue` resource and writes them to the
/// `EventChannel<EntityEvent>` resource.
///
pub fn sys_entity_event_capture() -> Box<dyn Schedulable> {
    SystemBuilder::new("EntityEventCaptureSystem")
        .read_resource::<EntityEventCaptureQueue>()
        .write_resource::<EventChannel<EntityEvent>>()
        .build(move |_, _, (queue, channel), _| {
            while let Ok(event) = queue.0.pop() {
                EventChannel::single_write(channel, event);
            }
        })
}


pub struct SystemWorld<'a, 'b> {
    pub cmd: &'a mut CommandBuffer,
    pub world: &'b mut SubWorld,
}


pub trait Spawn<'a> {
    type Args: 'a;

    fn spawn(&self, world: &mut World, args: Self::Args) -> Entity;
    fn can_spawn_delayed(&self) -> bool { false }
    fn spawn_delayed(&self, _: &CommandBuffer, _: Self::Args) {
        panic!("Delayed spawning unsupported!");
    }
}

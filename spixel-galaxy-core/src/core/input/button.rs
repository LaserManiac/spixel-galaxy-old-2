#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Button {
    Left,
    Right,
    Middle,
}

impl Button {
    pub fn try_from_glutin(button: glutin::event::MouseButton) -> Option<Self> {
        use glutin::event::MouseButton as GlutinButton;
        match button {
            GlutinButton::Left => Some(Self::Left),
            GlutinButton::Right => Some(Self::Right),
            GlutinButton::Middle => Some(Self::Middle),
            _ => None,
        }
    }
}

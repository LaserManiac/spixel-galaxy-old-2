use std::collections::HashSet;

use legion::prelude::*;
use shrev::*;

pub use button::Button;
pub use key::Key;
pub use state::State;

use crate::core::state::Event;

pub mod key;
pub mod button;
pub mod state;

/// # Resource
///
/// TODO: Comment
///
pub struct Input {
    keys_pressed: HashSet<Key>,
    keys_just_pressed: HashSet<Key>,
    keys_just_released: HashSet<Key>,
    buttons_pressed: HashSet<Button>,
    buttons_just_pressed: HashSet<Button>,
    buttons_just_released: HashSet<Button>,
    cursor_x: u32,
    cursor_y: u32,
    mouse_delta_x: f32,
    mouse_delta_y: f32,
    scroll: i32,
    typed: String,
    rid_input: ReaderId<Event>,
}

impl Input {
    pub fn key_pressed(&self, key: Key) -> bool {
        self.keys_pressed.contains(&key)
    }

    pub fn key_just_pressed(&self, key: Key) -> bool {
        self.keys_just_pressed.contains(&key)
    }

    pub fn key_just_released(&self, key: Key) -> bool {
        self.keys_just_released.contains(&key)
    }

    pub fn button_pressed(&self, button: Button) -> bool {
        self.buttons_pressed.contains(&button)
    }

    pub fn button_just_pressed(&self, button: Button) -> bool {
        self.buttons_just_pressed.contains(&button)
    }

    pub fn button_just_released(&self, button: Button) -> bool {
        self.buttons_just_released.contains(&button)
    }

    pub fn cursor_position(&self) -> (u32, u32) {
        (self.cursor_x, self.cursor_y)
    }

    pub fn mouse_delta(&self) -> (f32, f32) {
        (self.mouse_delta_x, self.mouse_delta_y)
    }

    pub fn scroll(&self) -> i32 {
        self.scroll
    }

    pub fn typed(&self) -> &str {
        &self.typed
    }
}


/// # Initializer
///
/// TODO: Comment
///
pub fn init_input(world: &mut World) {
    Input::new_in_world(world);
}

/// # System
///
/// TODO: Comment
///
pub fn sys_input() -> Box<dyn Schedulable> {
    SystemBuilder::new("InputSystem")
        .read_resource::<EventChannel<Event>>()
        .write_resource::<Input>()
        .build(move |_, _, (cnl_input, input), _| input.process_events(cnl_input))
}


impl Input {
    fn new_in_world(world: &mut World) {
        let rid_input = world.resources
            .get_mut_or_default::<EventChannel<Event>>()
            .unwrap()
            .register_reader();
        let input = Self {
            keys_pressed: Default::default(),
            keys_just_pressed: Default::default(),
            keys_just_released: Default::default(),
            buttons_pressed: Default::default(),
            buttons_just_pressed: Default::default(),
            buttons_just_released: Default::default(),
            cursor_x: Default::default(),
            cursor_y: Default::default(),
            mouse_delta_x: Default::default(),
            mouse_delta_y: Default::default(),
            scroll: Default::default(),
            typed: Default::default(),
            rid_input,
        };
        world.resources.insert(input);
    }

    fn process_events(&mut self, events: &EventChannel<Event>) {
        self.reset();
        let events = events.read(&mut self.rid_input)
            .collect::<Vec<_>>();
        for event in events {
            self.process_event(*event);
        }
    }

    fn reset(&mut self) {
        self.keys_just_pressed.clear();
        self.keys_just_released.clear();
        self.buttons_just_pressed.clear();
        self.buttons_just_released.clear();
        self.mouse_delta_x = 0.0;
        self.mouse_delta_y = 0.0;
        self.scroll = 0;
        self.typed.clear();
    }

    fn process_event(&mut self, event: Event) {
        match event {
            Event::Key { key, state: State::Pressed } => self.process_key_press(key),
            Event::Key { key, state: State::Released } => self.process_key_release(key),
            Event::Button { button, state: State::Pressed } => self.process_button_press(button),
            Event::Button { button, state: State::Released } => self.process_button_release(button),
            Event::Cursor { x, y } => self.process_cursor_position(x, y),
            Event::Scroll { amount } => self.process_scroll(amount),
            Event::Text { character } => self.process_char(character),
            Event::MouseDelta { x, y } => self.process_mouse_delta(x, y),
            _ => (),
        }
    }

    fn process_key_press(&mut self, key: Key) {
        if self.keys_pressed.insert(key) {
            self.keys_just_pressed.insert(key);
        }
    }

    fn process_key_release(&mut self, key: Key) {
        if self.keys_pressed.remove(&key) {
            self.keys_just_released.insert(key);
        }
    }

    fn process_button_press(&mut self, button: Button) {
        if self.buttons_pressed.insert(button) {
            self.buttons_just_pressed.insert(button);
        }
    }

    fn process_button_release(&mut self, button: Button) {
        if self.buttons_pressed.remove(&button) {
            self.buttons_just_released.insert(button);
        }
    }

    fn process_cursor_position(&mut self, x: u32, y: u32) {
        self.cursor_x = x;
        self.cursor_y = y;
    }

    fn process_mouse_delta(&mut self, x: f32, y: f32) {
        self.mouse_delta_x += x;
        self.mouse_delta_y += y;
    }

    fn process_scroll(&mut self, amount: i8) {
        self.scroll += amount as i32;
    }

    fn process_char(&mut self, chr: char) {
        self.typed.push(chr);
    }
}

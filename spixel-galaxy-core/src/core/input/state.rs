#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum State {
    Pressed,
    Released,
}

impl From<glutin::event::ElementState> for State {
    fn from(state: glutin::event::ElementState) -> Self {
        use glutin::event::ElementState as GlutinState;
        match state {
            GlutinState::Pressed => Self::Pressed,
            GlutinState::Released => Self::Released,
        }
    }
}

impl From<bool> for State {
    fn from(state: bool) -> Self {
        match state {
            true => Self::Pressed,
            false => Self::Released,
        }
    }
}

impl Into<bool> for State {
    fn into(self) -> bool {
        match self {
            Self::Pressed => true,
            Self::Released => false,
        }
    }
}

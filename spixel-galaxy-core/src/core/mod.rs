/// TODO: Comment

#[macro_use]
pub mod ecs;

pub mod state;
pub mod window;
pub mod input;
pub mod time;

pub trait GameDriver {
    type Window: window::Window;
    fn run<D, S>(data: D, args: S::Args, window_info: window::ConstructInfo) -> !
        where S: state::GameState<D> + state::StateConstructor<D> + 'static,
              D: 'static;
}


pub struct ContextView<'a, D> {
    data: &'a mut D,
    window: &'a mut dyn window::Window,
}

impl<'a, D> std::ops::Deref for ContextView<'a, D> {
    type Target = D;

    fn deref(&self) -> &Self::Target {
        &*self.data
    }
}

impl<'a, D> std::ops::DerefMut for ContextView<'a, D> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl<'a, D> ContextView<'a, D> {
    pub fn window(&self) -> &dyn window::Window {
        self.window
    }

    pub fn window_mut(&mut self) -> &mut dyn window::Window {
        self.window
    }
}


impl window::Window for glutin::window::Window {
    fn size(&self) -> window::WindowSize {
        let size = self.inner_size();
        window::WindowSize {
            width: size.width,
            height: size.height,
        }
    }

    fn set_cursor_grab(&mut self, grab: bool) {
        let _ = glutin::window::Window::set_cursor_grab(self, grab);
    }

    fn set_size(&mut self, width: u32, height: u32) {
        let size = glutin::dpi::PhysicalSize::new(width, height);
        glutin::window::Window::set_inner_size(self, size);
    }

    fn set_title(&mut self, title: String) {
        glutin::window::Window::set_title(self, &title);
    }

    fn set_decorations(&mut self, decorations: bool) {
        glutin::window::Window::set_decorations(self, decorations);
    }

    fn set_fullscreen(&mut self, fullscreen: bool) {
        let fullscreen = if fullscreen {
            let monitor = self.current_monitor();
            let fullscreen = glutin::window::Fullscreen::Borderless(monitor);
            Some(fullscreen)
        } else {
            None
        };
        glutin::window::Window::set_fullscreen(self, fullscreen);
    }

    fn set_minimized(&mut self, minimized: bool) {
        glutin::window::Window::set_minimized(self, minimized);
    }
}


pub struct GlutinDriver;

impl GameDriver for GlutinDriver {
    type Window = glutin::window::Window;

    fn run<D, S>(mut data: D, args: S::Args, window_info: window::ConstructInfo) -> !
        where S: state::GameState<D> + state::StateConstructor<D> + 'static,
              D: 'static {
        let mode = if cfg!(feature="debug") { "debug" } else { "release" };
        spg_info!("Running SPG core v{} in {} mode!", crate::version(), mode);

        // Create main event loop
        let event_loop = glutin::event_loop::EventLoop::new();

        // Create main window
        let mut window = glutin::window::WindowBuilder::new()
            .with_always_on_top(false)
            .with_title(window_info.title);
        if window_info.fullscreen {
            let monitor = event_loop.primary_monitor();
            let fullscreen = glutin::window::Fullscreen::Borderless(monitor);
            window = window.with_fullscreen(Some(fullscreen))
        } else {
            let size = glutin::dpi::PhysicalSize::new(window_info.width, window_info.height);
            window = window.with_inner_size(size);
        }

        // Create main OpenGL context
        let gl_context = glutin::ContextBuilder::new()
            .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (4, 5)))
            .with_gl_profile(glutin::GlProfile::Core)
            .with_srgb(true)
            .build_windowed(window, &event_loop)
            .expect("Could not build OpenGL context!");
        let (gl_context, mut window) = unsafe { glutin::WindowedContext::split(gl_context) };

        // Initialize OpenGL
        let gl_context = unsafe { gl_context.make_current() }
            .expect("Could not make OpenGL context current!");
        gl::load_with(|symbol| gl_context.get_proc_address(symbol) as *const _);

        // Create initial game context
        let mut state_stack = {
            let mut context_view = ContextView { data: &mut data, window: &mut window };
            state::Stack::new::<S>(args, &mut context_view)
        };

        // Run main event loop
        event_loop.run(move |event, _, control_flow| {
            match event {
                // Window input
                glutin::event::Event::WindowEvent { window_id, event } => {
                    if window_id == window.id() {
                        if let Some(event) = GlutinDriver::convert_window_event(event) {
                            let mut context = ContextView {
                                data: &mut data,
                                window: &mut window,
                            };
                            state_stack.event(&mut context, &event);
                        }
                    }
                }

                // Mouse moved
                glutin::event::Event::DeviceEvent { event, .. } => {
                    use glutin::event::DeviceEvent;
                    if let DeviceEvent::MouseMotion { delta: (x, y) } = event {
                        let x = x as f32;
                        let y = y as f32;
                        let event = state::Event::MouseDelta { x, y };
                        let mut context = ContextView {
                            data: &mut data,
                            window: &mut window,
                        };
                        state_stack.event(&mut context, &event);
                    }
                }

                // Update
                glutin::event::Event::NewEvents(glutin::event::StartCause::Poll) => {
                    let mut context = ContextView {
                        data: &mut data,
                        window: &mut window,
                    };
                    state_stack.run(&mut context);
                    state_stack.update(&mut context);
                    if state_stack.empty() {
                        *control_flow = glutin::event_loop::ControlFlow::Exit;
                    }
                    gl_context.swap_buffers()
                        .expect("Could not swap buffers!");
                }

                // Exit
                glutin::event::Event::LoopDestroyed => {
                    // Clean up.
                }
                _ => (),
            }
        });
    }
}

impl GlutinDriver {
    fn convert_window_event(event: glutin::event::WindowEvent) -> Option<state::Event> {
        use glutin::{event::WindowEvent, dpi};
        match event {
            WindowEvent::CloseRequested =>
                return Some(state::Event::Close),
            WindowEvent::Resized(dpi::PhysicalSize { width, height }) =>
                return Some(state::Event::Resize { width, height }),
            WindowEvent::KeyboardInput { input, .. } =>
                if let Some(Some(key)) = input.virtual_keycode.map(input::Key::try_from_glutin) {
                    return Some(state::Event::Key {
                        key,
                        state: input.state.into(),
                    });
                },
            WindowEvent::MouseInput { button, state, .. } =>
                if let Some(button) = input::Button::try_from_glutin(button) {
                    return Some(state::Event::Button {
                        button,
                        state: state.into(),
                    });
                },
            WindowEvent::MouseWheel { delta, .. } => {
                return Some(state::Event::Scroll {
                    amount: match delta {
                        glutin::event::MouseScrollDelta::LineDelta(_, y) => y as i8,
                        glutin::event::MouseScrollDelta::PixelDelta(d) => d.y as i8,
                    }
                });
            }
            WindowEvent::CursorMoved { position, .. } =>
                return Some(state::Event::Cursor {
                    x: position.x as u32,
                    y: position.y as u32,
                }),
            WindowEvent::ReceivedCharacter(character) =>
                return Some(state::Event::Text { character }),
            _ => (),
        };
        None
    }
}

use super::{ContextView, input};

pub trait GameState<D> {
    /// Called once per frame when the state is on top of the stack.
    /// Returns a control flow transition command.
    ///
    fn run(&mut self, _: &mut ContextView<D>) -> Trans<D> { Trans::None }

    /// Called once per frame regardless of where in the stack the state is.
    ///
    /// There will be an active OpenGL context on the current thread at the time of calling this
    /// method.
    ///
    fn update(&mut self, _: &mut ContextView<D>) {}

    /// Called when the context window receives an event.
    /// Optionally returns a control flow transition command.
    ///
    /// The returned value should be `Some(..)` if the event is to be considered as processed,
    /// or `None` if it should be propagated further down the stack.
    ///
    fn event(&mut self, _: &mut ContextView<D>, _: &Event) -> Option<Trans<D>> { None }

    /// Called when a new state is pushed to the stack on top of this one.
    ///
    fn sink(&mut self, _: &mut ContextView<D>) { }

    /// Called when a state above this one is popped from the stack.
    ///
    fn rise(&mut self, _: &mut ContextView<D>) { }

    /// Called when the state is popped from the stack.
    ///
    fn end(self: Box<Self>, _: &mut ContextView<D>) { }

    /// Should return a name for this state used for logging.
    ///
    fn name(&self) -> &'static str;
}


/// A constructor for a game state.
/// The associated type `Args` is the constructor arguments object type.
/// The generic parameter `D` is the type of game data that the state uses.
///
pub trait StateConstructor<D> {
    type Args: 'static;

    fn construct(args: Self::Args, data: &mut ContextView<D>) -> Self;
}


/// Holds `GameState` constructor arguments before the state is initialized.
///
pub struct StateInitializer<D>(Box<dyn FnOnce(&mut ContextView<D>) -> Box<dyn GameState<D>>>);

impl<D> StateInitializer<D> {
    /// Creates a typeless constructor for the given state using the provided data.
    ///
    pub fn new<S>(args: S::Args) -> Self
        where S: GameState<D> + StateConstructor<D> + 'static,
              S::Args: 'static {
        Self(Box::new(move |data| Box::new(S::construct(args, data))))
    }

    /// Constructs a new state using the stored closure.
    /// The state is returned as a dynamic object.
    ///
    pub fn init(self, data: &mut ContextView<D>) -> Box<dyn GameState<D>> {
        self.0(data)
    }
}


/// An event that is processed by the state stack.
///
#[derive(Copy, Clone, Debug)]
pub enum Event {
    Hide,
    Show,
    Close,
    Resize {
        width: u32,
        height: u32,
    },
    Key {
        key: input::Key,
        state: input::State,
    },
    Button {
        button: input::Button,
        state: input::State,
    },
    Scroll {
        amount: i8,
    },
    Cursor {
        x: u32,
        y: u32,
    },
    Text {
        character: char,
    },
    MouseDelta {
        x: f32,
        y: f32,
    },
}


/// A transition between states with game data `D`.
///
pub enum Trans<D> {
    /// No transition.
    ///
    None,

    /// Switch to a different state.
    ///
    Switch(StateInitializer<D>),

    /// Push a new active state to the stack.
    ///
    Push(StateInitializer<D>),

    /// Pop the active state from the stack.
    ///
    Pop,

    /// Exit the program.
    ///
    Exit,
}

impl<D> Trans<D> {
    /// Constructs a new `Trans::Switch` instance targeting the specified state.
    ///
    pub fn switch<S>(args: S::Args) -> Self
        where S: GameState<D> + StateConstructor<D> + 'static,
              S::Args: 'static {
        Self::Switch(StateInitializer::new::<S>(args))
    }

    /// Constructs a new `Trans::Push` instance targeting the specified state.
    ///
    pub fn push<S>(args: S::Args) -> Self
        where S: GameState<D> + StateConstructor<D> + 'static,
              S::Args: 'static {
        Self::Push(StateInitializer::new::<S>(args))
    }
}


/// A stack based state machine.
///
pub struct Stack<D> {
    states: Vec<Box<dyn GameState<D>>>
}

impl<D> Stack<D> {
    /// Constructs a new stack with the given state as the top state.
    ///
    pub fn new<S>(args: S::Args, context: &mut ContextView<D>) -> Self
        where S: GameState<D> + StateConstructor<D> + 'static,
              S::Args: 'static {
        let mut initial_state = Box::new(S::construct(args, context));
        initial_state.rise(context);
        Self { states: vec![initial_state] }
    }

    /// Calls `run` on the top state and executes the returned transition.
    ///
    pub fn run(&mut self, context: &mut ContextView<D>) {
        if let Some(state) = self.states.last_mut() {
            let trans = state.run(context);
            self.transition(trans, context);
        }
    }

    /// Calls `update` on all states in the stack, bottom to top.
    ///
    /// The implementation should ensure that there is an active OpenGL context on the current
    /// thread at the time of calling this method.
    ///
    pub fn update(&mut self, context: &mut ContextView<D>) {
        for state in self.states.iter_mut() {
            state.update(context);
        }
    }

    /// Propagates the event down the state stack until it is processed by some state.
    ///
    /// Returns `true` if the event was processed, `false` otherwise.
    ///
    pub fn event(&mut self, context: &mut ContextView<D>, event: &Event) -> bool {
        for state in self.states.iter_mut().rev() {
            if let Some(trans) = state.event(context, event) {
                self.transition(trans, context);
                return true;
            }
        }
        false
    }

    /// Returns `true` if the stack is empty, `false` otherwise.
    ///
    pub fn empty(&self) -> bool {
        self.states.is_empty()
    }

    /// Performs the given state transition.
    ///
    fn transition(&mut self, trans: Trans<D>, context: &mut ContextView<D>) {
        match trans {
            Trans::None => (),
            Trans::Switch(new) => {
                if let Some(mut old) = self.states.pop() {
                    old.sink(context);
                    old.end(context);
                }
                let mut new = new.init(context);
                new.rise(context);
                self.states.push(new);
            }
            Trans::Push(new) => {
                if let Some(old) = self.states.last_mut() {
                    old.sink(context);
                }
                let mut new = new.init(context);
                new.rise(context);
                self.states.push(new);
            }
            Trans::Pop => {
                if let Some(mut old) = self.states.pop() {
                    old.sink(context);
                    old.end(context);
                }
                if let Some(new) = self.states.last_mut() {
                    new.rise(context);
                }
            }
            Trans::Exit => {
                while let Some(mut state) = self.states.pop() {
                    state.sink(context);
                    state.end(context);
                }
            }
        }
    }
}

//! TODO: Comment

use std::time::{Duration, Instant};

use legion::prelude::*;

/// # Resource
///
/// A wrapper around `std::time::Duration`.
///
#[derive(Copy, Clone, PartialEq, Default)]
pub struct DeltaTime(Duration);

impl DeltaTime {
    /// Get the amount of seconds elapsed.
    ///
    pub fn as_secs(&self) -> f32 {
        self.0.as_secs_f32()
    }

    /// Get the amount of milliseconds elapsed.
    ///
    pub fn as_millis(&self) -> u32 {
        self.0.as_millis() as u32
    }
}


/// Used to measure elapsed time.
///
pub struct Clock(Instant);

impl Clock {
    /// Creates a new clock.
    ///
    pub fn new() -> Self {
        Clock(Instant::now())
    }

    /// Returns the time elapsed since the last call to `reset` or since the clock was created.
    ///
    pub fn reset(&mut self) -> DeltaTime {
        let now = Instant::now();
        let delta = now - std::mem::replace(&mut self.0, now);
        DeltaTime(delta)
    }
}


/// Used to measure elapsed time in periods called _ticks_.
///
pub struct Ticker {
    period: Duration,
    accum: Duration,
    cp: Instant,
}

impl Ticker {
    /// Create a new ticker with a period of `ms` milliseconds.
    ///
    pub fn new(ms: u64) -> Self {
        assert_ne!(ms, 0, "Cannot have a period of zero ms!");
        let period = Duration::from_millis(ms);
        let accum = Duration::from_millis(0);
        let cp = Instant::now();
        Self { period, accum, cp }
    }

    /// Create a new ticker that ticks `tps` times per second.
    ///
    pub fn with_tps(tps: u32) -> Self {
        assert_ne!(tps, 0, "Cannot have zero TPS!");
        let period = Duration::from_micros(1_000_000 / tps as u64);
        let accum = Duration::from_millis(0);
        let cp = Instant::now();
        Self { period, accum, cp }
    }

    /// Updates the ticker and returns `true` if a ticker's period of time has elapsed.
    ///
    pub fn tick(&mut self) -> bool {
        let now = Instant::now();
        let elapsed = now - std::mem::replace(&mut self.cp, now);
        self.accum += elapsed;
        if self.accum > self.period {
            self.accum -= self.period;
            true
        } else {
            false
        }
    }
}


/// # Resource
///
/// A clock used by the timer system to measure time elapsed between system runs.
///
struct TimerClock(Clock);

/// # Initializer
///
/// Initializes resources needed for the timer system.
///
/// - `TimerClock` - the clock used by the timer system.
/// - `DeltaTime` - the delta time value updated by the timer system.
///
pub fn init_timer(world: &mut World) {
    world.resources.insert(TimerClock(Clock::new()));
    world.resources.insert(DeltaTime::default());
}

/// # System
///
/// Updates the `DeltaTime` resource with the latest value received from the `TimerClock` resource.
///
pub fn sys_timer() -> Box<dyn Schedulable> {
    legion::prelude::SystemBuilder::new("TimerSystem")
        .write_resource::<TimerClock>()
        .write_resource::<DeltaTime>()
        .build(move |_, _, (clock, time), _| **time = clock.0.reset())
}

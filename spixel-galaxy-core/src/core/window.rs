use legion::prelude::*;
use shrev::*;

use super::state::Event;

pub struct ConstructInfo {
    pub width: u32,
    pub height: u32,
    pub title: String,
    pub decorations: bool,
    pub fullscreen: bool,

}

impl Default for ConstructInfo {
    fn default() -> Self {
        Self {
            width: 640,
            height: 480,
            title: String::from("SPG window"),
            decorations: true,
            fullscreen: false,
        }
    }
}


pub trait Window {
    fn size(&self) -> WindowSize;
    fn set_size(&mut self, width: u32, height: u32);
    fn set_title(&mut self, title: String);
    fn set_decorations(&mut self, decorations: bool);
    fn set_fullscreen(&mut self, fullscreen: bool);
    fn set_minimized(&mut self, minimized: bool);
    fn set_cursor_grab(&mut self, grab: bool);
}


/// # Resource
///
/// The current size of the game window.
///
pub struct WindowSize {
    pub width: u32,
    pub height: u32,
}


/// # Resource
///
/// The event reader used to capture window resize events.
///
struct WindowSizeEventReader(ReaderId<Event>);


/// # Initializer
///
/// Initializes resources used by window systems.
///
pub fn init_window(world: &mut World, window_size: WindowSize) {
    world.resources.insert(window_size);

    let reader_id = world.resources
        .get_mut_or_default::<EventChannel<_>>()
        .unwrap()
        .register_reader();
    world.resources.insert(WindowSizeEventReader(reader_id));
}


/// # System
///
/// Captures window resize events and updates the `WindowSize` resource.
///
pub fn sys_window_size_update() -> Box<dyn Schedulable> {
    SystemBuilder::new("WindowSizeUpdateSystem")
        .read_resource::<EventChannel<Event>>()
        .write_resource::<WindowSizeEventReader>()
        .write_resource::<WindowSize>()
        .build(move |_, _, (channel, reader, size), _| {
            for event in channel.read(&mut reader.0) {
                if let Event::Resize { width, height } = event {
                    size.width = *width;
                    size.height = *height;
                }
            }
        })
}

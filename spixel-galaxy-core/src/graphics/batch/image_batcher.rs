//! TODO: Comment

use crate::{
    asset::Handle,
    graphics::{
        batch::{Batch, Primitive},
        color::Color,
        image::Images,
        nine_patch::NinePatch,
        opengl::GlShader,
        shader::{Shader, Shaders},
        sprite::Sprite,
        text::Text,
        vertex::*,
        virtual_atlas::VirtualTexture,
    },
    util::math::{Alignment, Rect},
};

#[derive(Copy, Clone, Default)]
#[repr(packed)]
#[allow(unused)]
struct ImageVertex {
    position: [f32; 2],
    tex_coord: [f32; 2],
    color: [u8; 4],
    texture_index: u32,
}

impl Vertex for ImageVertex {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            size: AttributeSize::Vec2,
            kind: AttributeType::F32,
            offset: 0,
            divisor: None,
        },
        Attribute {
            size: AttributeSize::Vec2,
            kind: AttributeType::F32,
            offset: 8,
            divisor: None,
        },
        Attribute {
            size: AttributeSize::Vec4,
            kind: AttributeType::U8(AttributeNorm::Normalized),
            offset: 16,
            divisor: None,
        },
        Attribute {
            size: AttributeSize::Scalar,
            kind: AttributeType::U32(AttributeNorm::NotNormalized),
            offset: 20,
            divisor: None,
        }
    ];
}


pub struct ImageBatcher {
    batch: Batch<ImageVertex>,
    shader: Handle<Shader>,
}

impl ImageBatcher {
    pub fn new(shader: Handle<Shader>) -> Self {
        let batch = Batch::new(6 * 2000);
        Self { batch, shader }
    }

    pub fn begin<'a, 'b, 'c>(
        &'a mut self,
        view_projection: glm::Mat4,
        shaders: &'b Shaders,
        images: &'c Images,
    ) -> Option<ImageBatch<'a, 'b, 'c>> {
        let shader = shaders.get(&self.shader)?;
        let batch = &mut self.batch;
        Some(ImageBatch { batch, shader, images, view_projection })
    }
}


pub struct ImageBatch<'a, 'b, 'c> {
    batch: &'a mut Batch<ImageVertex>,
    shader: &'b GlShader,
    images: &'c Images,
    view_projection: glm::Mat4,
}

impl<'a, 'b, 'c> ImageBatch<'a, 'b, 'c> {
    pub fn set_view_projection(&mut self, view_projection: glm::Mat4) {
        self.flush();
        self.view_projection = view_projection;
    }

    pub fn draw_texture(
        &mut self,
        texture: &VirtualTexture,
        rect: impl Into<Rect<f32>>,
        region: impl Into<Rect<f32>>,
        color: impl Into<Color>,
    ) {
        self.push_quad(texture.index(), rect, region, color);
    }

    pub fn draw_sprite(
        &mut self,
        sprite: &Sprite,
        rect: impl Into<Rect<f32>>,
        color: impl Into<Color>,
    ) {
        if let Some(texture) = self.images.get_texture(&sprite.image) {
            let color = sprite.color * color.into();
            let region = sprite.region.to_primitive::<f32>().unwrap();
            self.push_quad(texture.index(), rect, region, color);
        }
    }

    pub fn draw_nine_patch(
        &mut self,
        nine_patch: &NinePatch,
        rect: impl Into<Rect<f32>>,
        color: impl Into<Color>,
    ) {
        let rect = rect.into();
        let rx = rect.x;
        let ry = rect.y;
        let rw = rect.w;
        let rh = rect.h;

        let px = nine_patch.region.x as f32;
        let py = nine_patch.region.y as f32;
        let pw = nine_patch.region.w as f32;
        let ph = nine_patch.region.h as f32;
        let pt = nine_patch.sides.t as f32;
        let pb = nine_patch.sides.b as f32;
        let pl = nine_patch.sides.l as f32;
        let pr = nine_patch.sides.r as f32;

        let color = nine_patch.color * color.into();

        if let Some(texture) = self.images.get_texture(&nine_patch.image) {
            let index = texture.index();
            self.push_quad(
                index,
                Rect::new(rx, ry, pl, pt),
                Rect::new(px, py, pl, pt),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx, ry + rh - pb, pl, pb),
                Rect::new(px, py + ph - pb, pl, pb),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx + rw - pr, ry + rh - pb, pr, pb),
                Rect::new(px + pw - pr, py + ph - pb, pr, pb),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx + rw - pr, ry, pr, pt),
                Rect::new(px + pw - pr, py, pr, pt),
                color,
            );

            self.push_quad(
                index,
                Rect::new(rx + pl, ry, rw - pl - pr, pt),
                Rect::new(px + pl, py, pw - pl - pr, pt),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx + pl, ry + rh - pb, rw - pl - pr, pb),
                Rect::new(px + pl, py + ph - pb, pw - pl - pr, pb),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx, ry + pt, pl, rh - pt - pb),
                Rect::new(px, py + pt, pl, ph - pt - pb),
                color,
            );
            self.push_quad(
                index,
                Rect::new(rx + rw - pr, ry + pt, pr, rh - pt - pb),
                Rect::new(px + pw - pr, py + pt, pr, ph - pt - pb),
                color,
            );

            self.push_quad(
                index,
                Rect::new(rx + pl, ry + pt, rw - pl - pr, rh - pt - pb),
                Rect::new(px + pl, py + pt, pw - pl - pr, ph - pt - pb),
                color,
            );
        }
    }

    pub fn draw_text(
        &mut self,
        text: &Text,
        x: f32,
        y: f32,
        color: Color,
        halign: Alignment,
        valign: Alignment,
    ) {
        if let Some(texture) = self.images.get_texture(&text.image) {
            let texture_index = texture.index();
            let init_y = y - valign.get_aligned(0.0, text.height());
            for line in &text.lines {
                let y = init_y + line.pos_y;
                let init_x = x - halign.get_aligned(0.0, line.width);
                for chr in &text.chars[line.start..line.end] {
                    let x = init_x + chr.pos_x;
                    let w = text.glyph_width;
                    let h = text.glyph_height;
                    let rect = Rect { x, y, w, h };
                    let region = chr.region.to_primitive::<f32>().unwrap();
                    self.push_quad(texture_index, rect, region, color);
                }
            }
        }
    }

    fn push_quad(
        &mut self,
        texture_index: u32,
        rect: impl Into<Rect<f32>>,
        region: impl Into<Rect<f32>>,
        color: impl Into<Color>,
    ) {
        if !self.batch.has_capacity(6) {
            self.flush();
        }

        let rect = rect.into();
        let region = region.into().to_primitive().unwrap();
        let color = color.into();

        let v0 = ImageVertex {
            position: [rect.x, rect.y],
            tex_coord: [region.x, region.y],
            color: color.into(),
            texture_index,
        };
        let v1 = ImageVertex {
            position: [rect.x + rect.w, rect.y],
            tex_coord: [region.x + region.w, region.y],
            color: color.into(),
            texture_index,
        };
        let v2 = ImageVertex {
            position: [rect.x + rect.w, rect.y + rect.h],
            tex_coord: [region.x + region.w, region.y + region.h],
            color: color.into(),
            texture_index,
        };
        let v3 = ImageVertex {
            position: [rect.x, rect.y + rect.h],
            tex_coord: [region.x, region.y + region.h],
            color: color.into(),
            texture_index,
        };

        self.batch.push(&[v2, v1, v0, v3, v2, v0]);
    }

    fn flush(&mut self) {
        self.shader.bind();
        self.shader.set_i32(0, 0); // Atlas texture array is bound to texture 0
        self.shader.set_mat4(1, &self.view_projection);
        self.batch.flush(Primitive::Triangles);
    }
}

impl<'a, 'b, 'c> Drop for ImageBatch<'a, 'b, 'c> {
    fn drop(&mut self) {
        self.flush();
    }
}

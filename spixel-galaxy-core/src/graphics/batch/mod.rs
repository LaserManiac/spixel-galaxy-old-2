use crate::graphics::{
    opengl::{GlDynamicBuffer, GlVertexArray},
    vertex::Vertex,
};

pub mod image_batcher;
pub mod shape_batcher;

pub enum Primitive {
    Triangles,
    Lines,
    Points,
}

pub struct Batch<V: Vertex> {
    buffer: Box<[V]>,
    size: usize,
    vbo: GlDynamicBuffer<V>,
    vao: GlVertexArray,
}

impl<V: Vertex> Batch<V> {
    pub fn new(size: usize) -> Self where V: Default {
        let buffer = vec![V::default(); size].into_boxed_slice();
        let vbo = GlDynamicBuffer::with_capacity(size);
        let mut vao = GlVertexArray::new();
        vao.attach_vertex_buffer(&vbo, 0);
        Self { buffer, vbo, vao, size: 0 }
    }

    pub fn has_capacity(&self, capacity: usize) -> bool {
        self.size + capacity <= self.buffer.len()
    }

    pub fn push(&mut self, vertices: &[V]) {
        let range = self.size..self.size + vertices.len();
        let slice = &mut self.buffer[range];
        slice.copy_from_slice(vertices);
        self.size += vertices.len();
    }

    pub fn flush(&mut self, mode: Primitive) {
        if self.size == 0 {
            return;
        }

        self.vbo.upload(&self.buffer[..self.size]);
        self.vao.bind();
        let mode = match mode {
            Primitive::Triangles => gl::TRIANGLES,
            Primitive::Lines => gl::LINES,
            Primitive::Points => gl::POINTS,
        };
        gl_call!("Batch::flush", {
            gl::DrawArrays(mode, 0, self.size as _);
        });

        self.size = 0;
    }
}
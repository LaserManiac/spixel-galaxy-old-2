use crate::{
    asset::Handle,
    graphics::{
        batch::{Batch, Primitive},
        color::Color,
        opengl::GlShader,
        shader::{Shader, Shaders},
        vertex::*,
    },
    util::math::Rect,
};

#[derive(Copy, Clone, Default)]
#[repr(packed)]
#[allow(unused)]
struct ShapeVertex {
    position: [f32; 3],
    color: [u8; 4],
}

impl Vertex for ShapeVertex {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            size: AttributeSize::Vec3,
            kind: AttributeType::F32,
            offset: 0,
            divisor: None,
        },
        Attribute {
            size: AttributeSize::Vec4,
            kind: AttributeType::U8(AttributeNorm::Normalized),
            offset: 12,
            divisor: None,
        }
    ];
}


#[derive(Copy, Clone)]
pub enum ShapeType {
    Fill,
    Line,
}

impl ShapeType {
    pub fn get_primitive(&self) -> Primitive {
        match self {
            ShapeType::Fill => Primitive::Triangles,
            ShapeType::Line => Primitive::Lines,
        }
    }
}


pub struct ShapeBatcher {
    batch: Batch<ShapeVertex>,
    shader: Handle<Shader>,
}

impl ShapeBatcher {
    pub fn new(shader: Handle<Shader>) -> Self {
        let batch = Batch::new(10_000);
        Self { batch, shader }
    }

    pub fn begin<'a, 'b>(
        &'a mut self,
        view_projection: glm::Mat4,
        shape_type: ShapeType,
        shaders: &'b Shaders,
    ) -> Option<ShapeBatch<'a, 'b>> {
        Some(ShapeBatch {
            batch: &mut self.batch,
            shader: shaders.get(&self.shader)?,
            shape_type,
            view_projection,
        })
    }
}


pub struct ShapeBatch<'a, 'b> {
    batch: &'a mut Batch<ShapeVertex>,
    shader: &'b GlShader,
    shape_type: ShapeType,
    view_projection: glm::Mat4,
}

impl<'a, 'b> ShapeBatch<'a, 'b> {
    pub fn shader(&self) -> &GlShader {
        self.shader
    }

    pub fn rect(&mut self, rect: Rect<f32>, color: Color) {
        let v0 = ShapeVertex {
            position: [rect.x, rect.y, 0.0],
            color: color.into(),
        };
        let v1 = ShapeVertex {
            position: [rect.x + rect.w, rect.y, 0.0],
            color: color.into(),
        };
        let v2 = ShapeVertex {
            position: [rect.x + rect.w, rect.y + rect.h, 0.0],
            color: color.into(),
        };
        let v3 = ShapeVertex {
            position: [rect.x, rect.y + rect.h, 0.0],
            color: color.into(),
        };

        let required_capacity = match self.shape_type {
            ShapeType::Fill => 6,
            ShapeType::Line => 8,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        match self.shape_type {
            ShapeType::Fill => self.batch.push(&[v2, v1, v0, v3, v2, v0]),
            ShapeType::Line => self.batch.push(&[v1, v0, v2, v1, v3, v2, v0, v3]),
        }
    }

    pub fn circle(&mut self, x: f32, y: f32, r: f32, segments: u32, color: Color) {
        assert_debug!(segments > 2, "A circle must have at least 3 segments!");

        let required_capacity = match self.shape_type {
            ShapeType::Fill => segments as usize * 3,
            ShapeType::Line => segments as usize * 2,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        let center = ShapeVertex {
            position: [x, y, 0.0],
            color: color.into(),
        };

        let mut prev = Default::default();
        for seg in 0..segments + 1 {
            let ang = seg as f32 / segments as f32 * std::f32::consts::PI * 2.0;
            let x = x + ang.sin() * r;
            let y = y + ang.cos() * r;
            let curr = ShapeVertex {
                position: [x, y, 0.0],
                color: color.into(),
            };
            if seg != 0 {
                match self.shape_type {
                    ShapeType::Fill => self.batch.push(&[curr, prev, center]),
                    ShapeType::Line => self.batch.push(&[curr, prev]),
                }
            }
            prev = curr;
        }
    }

    pub fn polygon_2d(&mut self, points: &[na::Point2<f32>], color: Color) {
        let len = points.len();
        let required_capacity = match self.shape_type {
            ShapeType::Fill => {
                assert_debug!(len > 2, "A filled 2d polygon must have at least 3 vertices!");
                6 * (len - 2)
            }
            ShapeType::Line => {
                assert_debug!(len > 1, "A line 2d polygon must have at least 2 vertices!");
                2 * (len - 1)
            }
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        let color = color.into();

        match self.shape_type {
            ShapeType::Fill => {
                for idx in 2..points.len() {
                    let p0 = &points[idx - 2];
                    let p1 = &points[idx - 1];
                    let p2 = &points[idx];
                    let v0 = ShapeVertex { position: [p0.x, p0.y, 0.0], color };
                    let v1 = ShapeVertex { position: [p1.x, p1.y, 0.0], color };
                    let v2 = ShapeVertex { position: [p2.x, p2.y, 0.0], color };
                    if idx % 2 == 0 {
                        self.batch.push(&[v2, v1, v0]);
                    } else {
                        self.batch.push(&[v0, v1, v2]);
                    }
                }
            }
            ShapeType::Line => {
                for idx in 1..points.len() {
                    let p0 = &points[idx - 1];
                    let p1 = &points[idx];
                    let v0 = ShapeVertex { position: [p0.x, p0.y, 0.0], color };
                    let v1 = ShapeVertex { position: [p1.x, p1.y, 0.0], color };
                    self.batch.push(&[v0, v1]);
                }
            }
        }
    }

    pub fn cuboid(&mut self, from: na::Point3<f32>, to: na::Point3<f32>, color: Color) {
        let color = color.into();
        let v00 = ShapeVertex { position: [from.x, from.y, from.z], color };
        let v01 = ShapeVertex { position: [to.x, from.y, from.z], color };
        let v02 = ShapeVertex { position: [to.x, to.y, from.z], color };
        let v03 = ShapeVertex { position: [from.x, to.y, from.z], color };

        let v10 = ShapeVertex { position: [from.x, from.y, to.z], color };
        let v11 = ShapeVertex { position: [to.x, from.y, to.z], color };
        let v12 = ShapeVertex { position: [to.x, to.y, to.z], color };
        let v13 = ShapeVertex { position: [from.x, to.y, to.z], color };

        let required_capacity = match self.shape_type {
            ShapeType::Fill => 36,
            ShapeType::Line => 24,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        match self.shape_type {
            ShapeType::Fill => {
                self.batch.push(&[
                    // Z+
                    v10, v11, v12,
                    v10, v12, v13,
                    // Z-
                    v02, v01, v00,
                    v03, v02, v00,
                    // X+
                    v01, v02, v12,
                    v01, v12, v11,
                    // X-
                    v03, v00, v10,
                    v03, v10, v13,
                    // Y+
                    v02, v03, v13,
                    v02, v13, v12,
                    // Y-
                    v00, v01, v11,
                    v00, v11, v10,
                ])
            }
            ShapeType::Line => {
                self.batch.push(&[
                    // Sides
                    v00, v10,
                    v01, v11,
                    v02, v12,
                    v03, v13,
                    // Top
                    v10, v11,
                    v11, v12,
                    v12, v13,
                    v13, v10,
                    // Bottom
                    v00, v01,
                    v01, v02,
                    v02, v03,
                    v03, v00,
                ])
            }
        }
    }

    pub fn cylinder(
        &mut self,
        position: na::Point3<f32>,
        h: f32,
        r: f32,
        segments: u32,
        color: Color,
    ) {
        assert_debug!(segments > 2, "A cylinder must have at least 3 segments!");

        let required_capacity = match self.shape_type {
            ShapeType::Fill => 12 * segments as usize,
            ShapeType::Line => 6 * segments as usize,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        let color = color.into();
        let mb = ShapeVertex { position: [position.x, position.y, position.z], color };
        let mt = ShapeVertex { position: [position.x, position.y, position.z + h], color };
        let mut pt = Default::default();
        let mut pb = Default::default();
        for segment in 0..segments + 1 {
            let ang = segment as f32 / segments as f32 * std::f32::consts::PI * 2.0;
            let x = position.x + ang.cos() * r;
            let y = position.y + ang.sin() * r;
            let ct = ShapeVertex { position: [x, y, position.z + h], color };
            let cb = ShapeVertex { position: [x, y, position.z], color };
            if segment != 0 {
                match self.shape_type {
                    ShapeType::Fill =>
                        self.batch.push(&[cb, ct, pt, pb, cb, pt, cb, pb, mb, pt, ct, mt]),
                    ShapeType::Line =>
                        self.batch.push(&[pt, ct, pb, cb, cb, ct]),
                }
            }
            pt = ct;
            pb = cb;
        }
    }

    pub fn sphere(&mut self, position: na::Point3<f32>, r: f32, segments: u32, color: Color) {
        assert_debug!(segments > 2, "A sphere must have at least 3 segments!");

        let color = color.into();

        let layers = segments / 2 + 1;
        let mut prev_layer = vec![na::Point3::<f32>::origin(); segments as usize];
        let mut this_layer = vec![na::Point3::<f32>::origin(); segments as usize];

        let required_capacity = match self.shape_type {
            ShapeType::Fill => 6 * segments as usize * layers as usize,
            ShapeType::Line => 6 * segments as usize * layers as usize,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        for layer in 0..layers + 1 {
            let ang = layer as f32 / layers as f32 * std::f32::consts::PI;
            let z = position.z + ang.cos() * r;
            let r = ang.sin() * r;
            for segment in 0..segments as usize {
                let point = &mut this_layer[segment];
                let ang = segment as f32 / segments as f32 * std::f32::consts::PI * 2.0;
                point.z = z;
                point.x = position.x + ang.cos() * r;
                point.y = position.y + ang.sin() * r;
            }

            if layer != 0 {
                for segment in 0..segments as usize {
                    let p0 = &prev_layer[segment as usize];
                    let p1 = &prev_layer[(segment + 1) % segments as usize];
                    let p2 = &this_layer[(segment + 1) % segments as usize];
                    let p3 = &this_layer[segment as usize];
                    let v0 = ShapeVertex { position: [p0.x, p0.y, p0.z], color };
                    let v1 = ShapeVertex { position: [p1.x, p1.y, p1.z], color };
                    let v2 = ShapeVertex { position: [p2.x, p2.y, p2.z], color };
                    let v3 = ShapeVertex { position: [p3.x, p3.y, p3.z], color };
                    match self.shape_type {
                        ShapeType::Fill => self.batch.push(&[v2, v1, v0, v3, v2, v0]),
                        ShapeType::Line => self.batch.push(&[v1, v2, v3, v0, v2, v3]),
                    }
                }
            }

            std::mem::swap(&mut this_layer, &mut prev_layer);
        }
    }

    pub fn capsule(
        &mut self,
        position: na::Point3<f32>,
        h: f32,
        r: f32,
        segments: u32,
        color: Color,
    ) {
        assert_debug!(segments > 2, "A capsule must have at least 3 segments!");

        let layers = segments / 2 + 1;

        let required_capacity = match self.shape_type {
            ShapeType::Fill => (1 + 2 * layers as usize) * 6 * segments as usize,
            ShapeType::Line => (1 + 2 * layers as usize) * 6 * segments as usize,
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        let color = color.into();
        let mut pt = Default::default();
        let mut pb = Default::default();
        for segment in 0..segments + 1 {
            let ang = segment as f32 / segments as f32 * std::f32::consts::PI * 2.0;
            let x = position.x + ang.cos() * r;
            let y = position.y + ang.sin() * r;
            let ct = ShapeVertex { position: [x, y, position.z + h], color };
            let cb = ShapeVertex { position: [x, y, position.z], color };
            if segment != 0 {
                match self.shape_type {
                    ShapeType::Fill => self.batch.push(&[cb, ct, pt, pb, cb, pt]),
                    ShapeType::Line => self.batch.push(&[pt, ct, pb, cb, cb, ct]),
                }
            }
            pt = ct;
            pb = cb;
        }

        let mut prev_layer = vec![na::Point3::<f32>::origin(); segments as usize];
        let mut this_layer = vec![na::Point3::<f32>::origin(); segments as usize];
        for layer in 0..layers as usize + 1 {
            let ang = layer as f32 / layers as f32 * std::f32::consts::FRAC_PI_2;
            let z = ang.sin() * r;
            let r = ang.cos() * r;
            for segment in 0..segments {
                let ang = segment as f32 / segments as f32 * std::f32::consts::PI * 2.0;
                let point = &mut this_layer[segment as usize];
                point.x = position.x + ang.cos() * r;
                point.y = position.y + ang.sin() * r;
                point.z = z;
            }

            if layer != 0 {
                for segment in 0..segments as usize {
                    let p0 = &prev_layer[segment % segments as usize];
                    let p1 = &prev_layer[(segment + 1) % segments as usize];
                    let p2 = &this_layer[(segment + 1) % segments as usize];
                    let p3 = &this_layer[segment % segments as usize];
                    let vt0 = ShapeVertex { position: [p0.x, p0.y, position.z + h + p0.z], color };
                    let vt1 = ShapeVertex { position: [p1.x, p1.y, position.z + h + p1.z], color };
                    let vt2 = ShapeVertex { position: [p2.x, p2.y, position.z + h + p2.z], color };
                    let vt3 = ShapeVertex { position: [p3.x, p3.y, position.z + h + p3.z], color };
                    let vb0 = ShapeVertex { position: [p0.x, p0.y, position.z - p0.z], color };
                    let vb1 = ShapeVertex { position: [p1.x, p1.y, position.z - p1.z], color };
                    let vb2 = ShapeVertex { position: [p2.x, p2.y, position.z - p2.z], color };
                    let vb3 = ShapeVertex { position: [p3.x, p3.y, position.z - p3.z], color };
                    match self.shape_type {
                        ShapeType::Fill =>
                            self.batch.push(&[
                                // Top
                                vt0, vt1, vt2,
                                vt0, vt2, vt3,
                                // Bottom
                                vb2, vb1, vb0,
                                vb3, vb2, vb0,
                            ]),
                        ShapeType::Line =>
                            self.batch.push(&[
                                // Top
                                vt1, vt2,
                                vt3, vt0,
                                vt2, vt3,
                                // Bottom
                                vb1, vb2,
                                vb3, vb0,
                                vb2, vb3,
                            ]),
                    }
                }
            }

            std::mem::swap(&mut prev_layer, &mut this_layer);
        }
    }

    pub fn polygon_3d(&mut self, points: &[na::Point3<f32>], color: Color) {
        let len = points.len();
        let required_capacity = match self.shape_type {
            ShapeType::Fill => {
                assert_debug!(len > 2, "A filled 3d polygon must have at least 3 vertices!");
                6 * (len - 2)
            }
            ShapeType::Line => {
                assert_debug!(len > 1, "A line 3d polygon must have at least 2 vertices!");
                2 * (len - 1)
            }
        };
        if !self.batch.has_capacity(required_capacity) {
            self.flush();
        }

        let color = color.into();

        match self.shape_type {
            ShapeType::Fill => {
                for idx in 2..points.len() {
                    let p0 = &points[idx - 2];
                    let p1 = &points[idx - 1];
                    let p2 = &points[idx];
                    let v0 = ShapeVertex { position: [p0.x, p0.y, p0.z], color };
                    let v1 = ShapeVertex { position: [p1.x, p1.y, p1.z], color };
                    let v2 = ShapeVertex { position: [p2.x, p2.y, p2.z], color };
                    if idx % 2 == 0 {
                        self.batch.push(&[v2, v1, v0]);
                    } else {
                        self.batch.push(&[v0, v1, v2]);
                    }
                }
            }
            ShapeType::Line => {
                for idx in 1..points.len() {
                    let p0 = &points[idx - 1];
                    let p1 = &points[idx];
                    let v0 = ShapeVertex { position: [p0.x, p0.y, p0.z], color };
                    let v1 = ShapeVertex { position: [p1.x, p1.y, p1.z], color };
                    self.batch.push(&[v0, v1]);
                }
            }
        }
    }

    fn flush(&mut self) {
        self.shader.bind();
        self.shader.set_mat4(0, &self.view_projection);
        self.batch.flush(self.shape_type.get_primitive());
    }
}

impl<'a, 'b> Drop for ShapeBatch<'a, 'b> {
    fn drop(&mut self) {
        self.flush();
    }
}

use legion::prelude::*;

use crate::{
    core::{
        input::{Button, Input, Key},
        time::DeltaTime,
    },
    graphics::camera::{ActiveCamera, Camera},
};

/// # Resource
///
/// TODO: Comment
///
pub struct FlyCameraController {
    speed: f32,
    look_x: f32,
    look_y: f32,
}


/// # Initializer
///
/// TODO: Comment
///
pub fn init_fly_camera_controlller(speed: f32, world: &mut World) {
    world.resources.insert(FlyCameraController {
        speed,
        look_x: 0.0,
        look_y: 0.0,
    });
}

/// # System
///
/// TODO: Comment
///
pub fn sys_fly_camera_controller() -> Box<dyn Schedulable> {
    SystemBuilder::new("FlyCameraControllerSystem")
        .read_resource::<DeltaTime>()
        .read_resource::<Input>()
        .read_resource::<ActiveCamera>()
        .write_resource::<FlyCameraController>()
        .write_component::<Camera>()
        .build(move |_, world, (delta, input, camera, controller), _| {
            if let Some(mut camera) = world.get_component_mut::<Camera>(camera.0) {
                if input.button_pressed(Button::Left) {
                    let (dx, dy) = input.mouse_delta();
                    controller.look_x = (controller.look_x + dx * 0.02) % 360.0;
                    controller.look_y = (controller.look_y - dy * 0.02).max(-89.0).min(89.0);
                    camera.direction = na::Unit::new_unchecked(glm::Vec3::x());
                    camera.direction = na::Unit::new_unchecked(
                        glm::rotate_vec3(
                            &camera.direction,
                            controller.look_y.to_radians(),
                            &glm::Vec3::y(),
                        )
                    );
                    camera.direction = na::Unit::new_unchecked(
                        glm::rotate_vec3(
                            &camera.direction,
                            controller.look_x.to_radians(),
                            &glm::Vec3::z(),
                        )
                    );
                }

                let speed = if input.key_pressed(Key::LeftShift) {
                    controller.speed * 200.0
                } else {
                    controller.speed
                };

                let delta = delta.as_secs();
                let f = input.key_pressed(Key::W) as i8;
                let b = input.key_pressed(Key::S) as i8;
                let fb = (f - b) as f32 * delta * speed;
                camera.position = camera.position + camera.direction.into_inner() * fb;

                let l = input.key_pressed(Key::A) as i8;
                let r = input.key_pressed(Key::D) as i8;
                let lr = (r - l) as f32 * delta * speed;
                let cross = glm::cross(&camera.direction.into_inner(), &glm::Vec3::z());
                let side = glm::normalize(&cross);
                camera.position = camera.position + side * lr;

                camera.update();
            }
        })
}

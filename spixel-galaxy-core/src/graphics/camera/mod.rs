use legion::prelude::*;

pub mod fly_controller;


/// # Resource
///
/// A pointer to the entity which has the camera component that should be used to render the scene.
///
pub struct ActiveCamera(pub Entity);


/// # Component
///
/// A camera component that can be attached to an entity and used to display the scene.
///
#[derive(Clone)]
pub struct Camera {
    pub position: na::Point3<f32>,
    pub direction: na::Unit<glm::Vec3>,
    pub up: na::Unit<glm::Vec3>,
    view: glm::Mat4,
    projection: glm::Mat4,
    combined: glm::Mat4,
}

impl Camera {
    pub fn new_orthographic_2d(x: f32, y: f32, w: f32, h: f32, yup: bool, depth: f32) -> Self {
        let h = if yup { -h } else { h };
        let mut camera = Self {
            position: [x + w * 0.5, y + h * 0.5, 0.0].into(),
            direction: -na::Unit::new_unchecked(glm::Vec3::z()),
            up: na::Unit::new_unchecked(glm::Vec3::y()),
            view: glm::Mat4::identity(),
            projection: glm::Mat4::identity(),
            combined: glm::Mat4::identity(),
        };
        camera.set_projection_orthographic(w, h, depth);
        camera.update();
        camera
    }

    pub fn new_perspective_3d(
        position: na::Point3<f32>,
        direction: na::Unit<glm::Vec3>,
        up: na::Unit<glm::Vec3>,
        aspect: f32,
        fov: f32,
        near: f32,
        far: f32,
    ) -> Self {
        let mut camera = Self {
            position,
            direction,
            up,
            view: glm::Mat4::identity(),
            projection: glm::Mat4::identity(),
            combined: glm::Mat4::identity(),
        };
        camera.set_projection_perspective(aspect, near, far, fov);
        camera.update();
        camera
    }

    pub fn set_projection_orthographic(&mut self, width: f32, height: f32, depth: f32) {
        let hw = width * 0.5;
        let hh = height * 0.5;
        self.projection = glm::ortho(-hw, hw, hh, -hh, 0.0, depth);
    }

    pub fn set_projection_perspective(&mut self, aspect: f32, near: f32, far: f32, fov: f32) {
        self.projection = glm::perspective(aspect, fov, near, far);
    }

    pub fn get_right(&self) -> na::Unit<glm::Vec3> {
        na::Unit::new_normalize(glm::cross(&self.direction, &self.up))
    }

    pub fn look_at(&mut self, target: na::Point3<f32>, up: na::Unit<glm::Vec3>) {
        self.direction = na::Unit::new_normalize(target - self.position);
        self.up = up;
    }

    pub fn compute_view(&mut self) {
        let look_at = self.position + self.direction.into_inner();
        self.view = glm::look_at(&self.position.coords, &look_at.coords, &self.up);
    }

    pub fn compute_combined(&mut self) {
        self.combined = self.projection * self.view;
    }

    pub fn update(&mut self) {
        self.compute_view();
        self.compute_combined();
    }

    pub fn combined(&self) -> &glm::Mat4 {
        &self.combined
    }
}

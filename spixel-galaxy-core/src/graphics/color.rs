use std::convert::TryFrom;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Default for Color {
    fn default() -> Self {
        Self {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
        }
    }
}

impl From<[u8; 4]> for Color {
    fn from([r, g, b, a]: [u8; 4]) -> Self {
        Self { r, g, b, a }
    }
}

impl Into<[u8; 4]> for Color {
    fn into(self) -> [u8; 4] {
        [self.r, self.g, self.b, self.a]
    }
}

impl From<[f32; 4]> for Color {
    fn from([r, g, b, a]: [f32; 4]) -> Self {
        Self {
            r: (r * 255.0) as u8,
            g: (g * 255.0) as u8,
            b: (b * 255.0) as u8,
            a: (a * 255.0) as u8,
        }
    }
}

impl Into<[f32; 4]> for Color {
    fn into(self) -> [f32; 4] {
        [
            self.r as f32 / 255.0,
            self.g as f32 / 255.0,
            self.b as f32 / 255.0,
            self.a as f32 / 255.0,
        ]
    }
}

impl Color {
    pub fn rgb(r: f32, g: f32, b: f32) -> Self {
        [r, g, b, 1.0].into()
    }

    pub fn rgba(r: f32, g: f32, b: f32, a: f32) -> Self {
        [r, g, b, a].into()
    }

    pub fn with_alpha_multiplied(&self, alpha: f32) -> Color {
        Self {
            r: self.r,
            g: self.g,
            b: self.b,
            a: (self.a as f32 * alpha) as u8,
        }
    }

    pub fn try_from_hex(hex: &str) -> Option<Self> {
        if hex.chars().any(|chr| !chr.is_ascii_hexdigit()) {
            return None;
        }

        let h = hex.as_bytes();
        match h.len() {
            3 => {
                let r = crate::util::math::hex_digit(h[0] as char)? as f32 / 15.0;
                let g = crate::util::math::hex_digit(h[1] as char)? as f32 / 15.0;
                let b = crate::util::math::hex_digit(h[2] as char)? as f32 / 15.0;
                Some([r, g, b, 1.0].into())
            }
            4 => {
                let r = crate::util::math::hex_digit(h[0] as char)? as f32 / 15.0;
                let g = crate::util::math::hex_digit(h[1] as char)? as f32 / 15.0;
                let b = crate::util::math::hex_digit(h[2] as char)? as f32 / 15.0;
                let a = crate::util::math::hex_digit(h[3] as char)? as f32 / 15.0;
                Some([r, g, b, a].into())
            }
            6 => {
                let r0 = crate::util::math::hex_digit(h[0] as char)?;
                let r1 = crate::util::math::hex_digit(h[1] as char)?;
                let g0 = crate::util::math::hex_digit(h[2] as char)?;
                let g1 = crate::util::math::hex_digit(h[3] as char)?;
                let b0 = crate::util::math::hex_digit(h[4] as char)?;
                let b1 = crate::util::math::hex_digit(h[5] as char)?;
                let r = r0 << 4 | r1;
                let g = g0 << 4 | g1;
                let b = b0 << 4 | b1;
                Some([r, g, b, 255].into())
            }
            8 => {
                let r0 = crate::util::math::hex_digit(h[0] as char)?;
                let r1 = crate::util::math::hex_digit(h[1] as char)?;
                let g0 = crate::util::math::hex_digit(h[2] as char)?;
                let g1 = crate::util::math::hex_digit(h[3] as char)?;
                let b0 = crate::util::math::hex_digit(h[4] as char)?;
                let b1 = crate::util::math::hex_digit(h[5] as char)?;
                let a0 = crate::util::math::hex_digit(h[6] as char)?;
                let a1 = crate::util::math::hex_digit(h[7] as char)?;
                let r = r0 << 4 | r1;
                let g = g0 << 4 | g1;
                let b = b0 << 4 | b1;
                let a = a0 << 4 | a1;
                Some([r, g, b, a].into())
            }
            _ => None,
        }
    }
}

impl TryFrom<&str> for Color {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "white" => Ok(Color::rgb(1.0, 1.0, 1.0)),
            "black" => Ok(Color::rgb(0.0, 0.0, 0.0)),
            "gray" => Ok(Color::rgb(0.5, 0.5, 0.5)),
            "red" => Ok(Color::rgb(1.0, 0.0, 0.0)),
            "lime" => Ok(Color::rgb(0.0, 1.0, 0.0)),
            "blue" => Ok(Color::rgb(0.0, 0.0, 1.0)),
            "darkred" => Ok(Color::rgb(0.5, 0.0, 0.0)),
            "green" => Ok(Color::rgb(0.0, 0.5, 0.0)),
            "darkblue" => Ok(Color::rgb(0.0, 0.0, 0.5)),
            "yellow" => Ok(Color::rgb(1.0, 1.0, 0.0)),
            "cyan" => Ok(Color::rgb(0.0, 1.0, 1.0)),
            "fuchsia" => Ok(Color::rgb(1.0, 0.0, 1.0)),
            "magenta" => Ok(Color::rgb(1.0, 0.0, 0.5)),
            "orange" => Ok(Color::rgb(1.0, 0.5, 0.0)),
            "yellowgreen" => Ok(Color::rgb(0.5, 1.0, 0.0)),
            "ocean" => Ok(Color::rgb(0.0, 0.5, 1.0)),
            "purple" => Ok(Color::rgb(0.5, 0.0, 1.0)),
            value => Color::try_from_hex(value).ok_or(()),
        }
    }
}

impl std::ops::Mul for Color {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let [sr, sg, sb, sa]: [u8; 4] = self.into();
        let [or, og, ob, oa]: [u8; 4] = rhs.into();
        Self {
            r: (sr as u16 * or as u16 / 255) as u8,
            g: (sg as u16 * og as u16 / 255) as u8,
            b: (sb as u16 * ob as u16 / 255) as u8,
            a: (sa as u16 * oa as u16 / 255) as u8,
        }
    }
}

impl std::ops::Mul<f32> for Color {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        let [r, g, b, a]: [f32; 4] = self.into();
        [r * rhs, g * rhs, b * rhs, a * rhs].into()
    }
}

impl std::fmt::Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let [r, g, b, a]: [f32; 4] = (*self).into();
        f.write_fmt(format_args!("({}, {}, {}, {})", r, g, b, a))
    }
}

impl<'a> serde::Deserialize<'a> for Color {
    fn deserialize<D: serde::Deserializer<'a>>(deserializer: D) -> Result<Self, D::Error> {
        use serde::de;
        use std::fmt;
        struct Visitor;
        impl<'b> de::Visitor<'b> for Visitor {
            type Value = Color;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a color value")
            }

            fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
                Color::try_from(v)
                    .map_err(|_| de::Error::invalid_value(de::Unexpected::Str(v), &self))
            }
        }

        deserializer.deserialize_any(Visitor)
    }
}

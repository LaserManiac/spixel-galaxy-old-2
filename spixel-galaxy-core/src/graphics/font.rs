//! TODO: Comment

use std::collections::HashMap;

use serde::Deserialize;

use crate::{
    asset::{Asset, Handle, LoadTask, Proxy},
    graphics::image::Image,
    util::{
        math::Rect,
        progress::Progress,
    },
};

pub struct Font {
    pub image: Handle<Image>,
    pub glyph_width: u32,
    pub glyph_height: u32,
    pub separation_x: u32,
    pub separation_y: u32,
    pub glyphs: HashMap<char, Rect<u32>>,
}

impl Asset for Font {
    type Id = String;

    fn loader(id: Self::Id, _: Progress, mut proxy: Proxy<Self>) -> LoadTask<Self> {
        Box::pin(async move {
            let path = format!("./res/{}", id);
            let data = async_std::fs::read_to_string(path).await
                .map_err(|err| format!("Could not read font from disk: {}", err))?;
            let data = serde_json::from_str::<FontData>(&data)
                .map_err(|err| format!("Could not parse font: {}", err))?;

            let total_width = data.glyph_width + data.padding_x;
            let total_height = data.glyph_height + data.padding_y;
            let glyphs_per_row = data.region.w / total_width;
            let glyphs = data.glyphs.char_indices()
                .map(|(idx, chr): (usize, char)| {
                    let grid_x = idx as u32 % glyphs_per_row;
                    let grid_y = idx as u32 / glyphs_per_row;
                    let region = Rect::new(
                        data.region.x + grid_x * total_width + data.padding_x,
                        data.region.y + grid_y * total_height + data.padding_y,
                        data.glyph_width,
                        data.glyph_height,
                    );
                    (chr, region)
                })
                .collect();

            let image = proxy.load(data.image).await;

            Ok(Font {
                image,
                glyph_width: data.glyph_width,
                glyph_height: data.glyph_height,
                separation_x: data.separation_x,
                separation_y: data.separation_y,
                glyphs,
            })
        })
    }

    fn virtual_file_name(id: &Self::Id) -> Option<String> {
        Some(id.clone())
    }
}


#[derive(Deserialize)]
struct FontData {
    image: String,
    region: Rect<u32>,
    glyph_width: u32,
    glyph_height: u32,
    separation_x: u32,
    separation_y: u32,
    padding_x: u32,
    padding_y: u32,
    glyphs: String,
}

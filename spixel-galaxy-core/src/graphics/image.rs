use image::RgbaImage;
use legion::prelude::*;
use shrev::*;
use slotmap::SecondaryMap;

use crate::{
    asset::{
        {Asset, Event, Get, LoadTask, Proxy, Storage},
        storage::{Key, HandleLike},
    },
    graphics::virtual_atlas::{VirtualAtlas, VirtualTexture},
    util::progress::Progress,
};

/// # Asset
///
/// Image data stored as RGBA.
///
pub struct Image {
    image: RgbaImage,
}

impl std::ops::Deref for Image {
    type Target = RgbaImage;

    fn deref(&self) -> &Self::Target {
        &self.image
    }
}

impl Asset for Image {
    /// An image is identified by it's file name relative to the "./res" directory.
    ///
    type Id = String;

    fn loader(id: Self::Id, progress: Progress, _: Proxy<Self>) -> LoadTask<Self> {
        futures::future::FutureExt::boxed(async move {
            let image = {
                let path = format!("./res/{}", id);
                let data = async_std::fs::read(path).await?;
                progress.set(50.0);
                let image = image::load_from_memory(&data)?;
                progress.set(100.0);
                image.to_rgba()
            };
            Ok(Image { image })
        })
    }

    fn virtual_file_name(id: &Self::Id) -> Option<String> {
        Some(id.clone())
    }
}


/// # Resource
///
/// Stores virtual texture handles mapped to their image asset handles.
///
/// __This resource must be accessed from the main thread only!__
///
#[derive(Default)]
pub struct Images(SecondaryMap<Key, VirtualTexture>);

impl Images {
    pub fn get_texture(&self, handle: &dyn HandleLike<Image>) -> Option<&VirtualTexture> {
        self.0.get(handle.key())
    }

    pub fn get_texture_weak(&self, handle: &dyn HandleLike<Image>) -> Option<&VirtualTexture> {
        self.0.get(handle.key())
    }
}


/// # Resource
///
/// The event reader used to capture image asset lifetime events.
///
struct ImageEventReader(ReaderId<Event<Image>>);


/// # Initializer
///
/// Initialize resources used by image systems.
///
pub fn init_images(world: &mut World) {
    world.resources.insert(Images::default());

    world.resources.get_or_default::<VirtualAtlas>();

    let reader_id = world.resources
        .get_mut_or_default::<EventChannel<Event<Image>>>()
        .unwrap()
        .register_reader();
    world.resources.insert(ImageEventReader(reader_id));
}


/// # System
///
/// Captures image asset lifetime events and creates or destroys their virtual textures.
/// The virtual texture handles are stored in the `Images` resource.
///
/// __This system must be run on the main thread only!__
///
pub fn sys_image_upload() -> Box<dyn Runnable> {
    SystemBuilder::new("ImageUploadSystem")
        .read_resource::<Storage<Image>>()
        .read_resource::<EventChannel<Event<Image>>>()
        .write_resource::<ImageEventReader>()
        .write_resource::<VirtualAtlas>()
        .write_resource::<Images>()
        .build_thread_local(move |_, _, (storage, channel, reader, atlas, images), _| {
            for event in channel.read(&mut reader.0) {
                match event {
                    Event::Finished(handle) => match storage.get(handle) {
                        Get::Finished(Ok(image)) => {
                            let (w, h) = image.dimensions();
                            if let Some(texture) = atlas.allocate(image) {
                                let old_texture = images.0.insert(handle.key(), texture);
                                if old_texture.is_some() {
                                    spg_info!("[Image] Reallocated virtual texture {}x{}!", w, h);
                                } else {
                                    spg_info!("[Image] Allocated virtual texture {}x{}!", w, h);
                                }
                            } else {
                                spg_warn!("[Image] Could not alloc virtual texture {}x{}!", w, h);
                            }
                        }
                        Get::Finished(Err(err)) => spg_warn!("[Image] Load error: {}", err),
                        _ => (),
                    },
                    Event::Deleted(handle) => {
                        if let Some(texture) = images.0.remove(handle.key()) {
                            atlas.deallocate(texture);
                            spg_info!("[Image] Deleted!");
                        } else {
                            spg_warn!("[Image] Attempted to delete nonexisting!");
                        }
                    }
                }
            }
        })
}

use serde::Deserialize;

use crate::{
    asset::{Asset, Handle, LoadTask, Proxy},
    graphics::{
        color::Color,
        image::Image,
    },
    util::{
        math::{Rect, Sides},
        progress::Progress,
    },
};

pub struct NinePatch {
    pub image: Handle<Image>,
    pub region: Rect<u32>,
    pub sides: Sides<u32>,
    pub color: Color,
}

impl Asset for NinePatch {
    type Id = String;

    fn loader(id: Self::Id, _: Progress, mut proxy: Proxy<Self>) -> LoadTask<Self> {
        Box::pin(async move {
            let path = format!("./res/{}", id);
            let data = async_std::fs::read_to_string(path).await
                .map_err(|err| format!("Could not read nine patch from disk: {}", err))?;
            let data = serde_json::from_str::<NinePatchData>(&data)
                .map_err(|err| format!("Could not parse nine patch: {}", err))?;
            let image = proxy.load(data.image).await;
            let region = data.region;
            let sides = data.sides;
            let color = data.color;
            Ok(NinePatch { image, region, sides, color })
        })
    }

    fn virtual_file_name(id: &Self::Id) -> Option<String> {
        Some(id.clone())
    }
}


#[derive(Deserialize)]
struct NinePatchData {
    image: String,
    #[serde(flatten)]
    region: Rect<u32>,
    #[serde(flatten)]
    sides: Sides<u32>,
    color: Color,
}

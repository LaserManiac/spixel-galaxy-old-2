/// TODO: Comment

use crate::graphics::{
    opengl::{GlBuffer, GlBufferHandle},
    vertex::Vertex,
};

pub trait GlArrayBuffer<V: Vertex> {
    fn bind(&self);
}

impl<B, V> GlArrayBuffer<V> for B
    where B: GlBuffer<V>,
          V: Vertex {
    fn bind(&self) {
        let handle = self.handle();
        gl_call!("GlArrayBuffer::bind", {
            gl::BindBuffer(gl::ARRAY_BUFFER, handle);
        });
    }
}

/// TODO: Comment

use gl::types::*;

use super::{GlBuffer, GlBufferHandle};

pub struct GlDynamicBuffer<T> {
    buffer: GlBufferHandle<T>
}

impl<T> GlBuffer<T> for GlDynamicBuffer<T> {
    fn handle(&self) -> GLuint {
        self.buffer.handle()
    }

    fn size(&self) -> usize {
        self.buffer.size()
    }
}

impl<T> GlDynamicBuffer<T> {
    pub fn new(data: &[T]) -> Self {
        let buffer = GlBufferHandle::new(data, gl::DYNAMIC_STORAGE_BIT);
        Self { buffer }
    }

    pub fn with_capacity(size: usize) -> Self {
        let buffer = GlBufferHandle::with_capacity(size, gl::DYNAMIC_STORAGE_BIT);
        Self { buffer }
    }

    pub fn upload(&self, data: &[T]) {
        self.upload_offset(data, 0);
    }

    pub fn upload_offset(&self, data: &[T], offset: usize) {
        let data_reach = offset + data.len();
        let buffer_size = self.buffer.size();
        assert_debug!(data_reach <= buffer_size, "Uploading outside of buffer bounds!");

        let handle = self.buffer.handle();
        let element_size = std::mem::size_of::<T>();
        let offset = (offset * element_size) as _;
        let size = (data.len() * element_size) as _;
        let ptr = data.as_ptr() as _;
        gl_call!("GlDynamicBuffer::upload_offset", {
            gl::NamedBufferSubData(handle, offset, size, ptr);
        });
    }
}

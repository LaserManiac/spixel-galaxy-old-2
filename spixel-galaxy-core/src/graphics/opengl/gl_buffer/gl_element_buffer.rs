//! TODO: Comment

use gl::types::*;

use crate::graphics::{
    opengl::{GlBuffer, GlBufferHandle},
    vertex::Vertex,
};

pub trait GlElementIndex {
    const INDEX_TYPE: GLenum;
}

impl GlElementIndex for u8 {
    const INDEX_TYPE: GLenum = gl::UNSIGNED_BYTE;
}

impl GlElementIndex for u16 {
    const INDEX_TYPE: GLenum = gl::UNSIGNED_SHORT;
}

impl GlElementIndex for u32 {
    const INDEX_TYPE: GLenum = gl::UNSIGNED_INT;
}


pub trait GlElementBuffer<E: GlElementIndex> {
    fn bind(&self);
}

impl<E, B> GlElementBuffer<E> for B
    where B: GlBuffer<E>,
          E: GlElementIndex {
    fn bind(&self) {
        let handle = self.handle();
        gl_call!("GlElementBuffer::bind", {
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, handle);
        });
    }
}
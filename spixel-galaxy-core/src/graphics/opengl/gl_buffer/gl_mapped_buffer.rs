/// TODO: Comment

use gl::types::*;

use super::{GlBuffer, GlBufferHandle};

pub struct GlMappedBuffer<T> {
    buffer: GlBufferHandle<T>,
    ptr: *mut T,
    coherent: bool,
}


impl<T> GlBuffer<T> for GlMappedBuffer<T> {
    fn handle(&self) -> GLuint {
        self.buffer.handle()
    }

    fn size(&self) -> usize {
        self.buffer.size()
    }
}

impl<T> std::ops::Deref for GlMappedBuffer<T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        if !self.ptr.is_null() {
            unsafe { std::slice::from_raw_parts(self.ptr, self.buffer.size) }
        } else {
            &[]
        }
    }
}

impl<T> std::ops::DerefMut for GlMappedBuffer<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        if !self.ptr.is_null() {
            unsafe { std::slice::from_raw_parts_mut(self.ptr, self.buffer.size) }
        } else {
            &mut []
        }
    }
}

impl<T> GlMappedBuffer<T> {
    pub fn new(data: &[T], coherent: bool) -> Self {
        Self::new_internal(data.as_ptr() as _, data.len(), coherent)
    }

    pub unsafe fn with_capacity(size: usize, coherent: bool) -> Self {
        Self::new_internal(std::ptr::null(), size, coherent)
    }

    fn new_internal(ptr: *const std::ffi::c_void, size: usize, coherent: bool) -> Self {
        let mut flags = gl::MAP_READ_BIT
            | gl::MAP_WRITE_BIT
            | gl::MAP_PERSISTENT_BIT;

        if coherent {
            flags |= gl::MAP_COHERENT_BIT;
        }

        let buffer = GlBufferHandle::new_internal(size, std::mem::size_of::<T>(), ptr, flags);
        let handle = buffer.handle();
        let length = (size * std::mem::size_of::<T>()) as _;

        if !coherent {
            flags |= gl::MAP_FLUSH_EXPLICIT_BIT;
        }

        let ptr = gl_call!("GlMappedBuffer::new", {
            gl::MapNamedBufferRange(handle, 0, length, flags)
        }) as *mut T;

        Self { buffer, ptr, coherent }
    }

    pub fn coherent(&self) -> bool {
        self.coherent
    }

    pub fn flush(&self) {
        self.flush_range(0, self.size());
    }

    pub fn flush_range(&self, offset: usize, size: usize) {
        assert!(!self.coherent, "Cannot flush a coherent buffer!");
        assert!(offset + size <= self.size(), "Flushing outside of buffer bounds!");
        let handle = self.buffer.handle();
        let element_size = std::mem::size_of::<T>();
        let offset = (offset * element_size) as _;
        let size = (size * element_size) as _;
        gl_call!("GlMappedBuffer::flush", {
            gl::FlushMappedNamedBufferRange(handle, offset, size);
        });
    }
}

/// TODO: Comment

use gl::types::*;

use super::{GlBuffer, GlBufferHandle};

pub struct GlStaticBuffer<T> {
    buffer: GlBufferHandle<T>
}

impl<T> GlBuffer<T> for GlStaticBuffer<T> {
    fn handle(&self) -> GLuint {
        self.buffer.handle()
    }

    fn size(&self) -> usize {
        self.buffer.size()
    }
}

impl<T> GlStaticBuffer<T> {
    pub fn new(data: &[T]) -> Self {
        let buffer = GlBufferHandle::new(data, gl::NONE);
        Self { buffer }
    }
}

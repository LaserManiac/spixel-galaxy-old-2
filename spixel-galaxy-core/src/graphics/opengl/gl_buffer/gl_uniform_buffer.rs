/// TODO: Comment

use super::{GlBuffer, GlBufferHandle};

pub trait GlUniformBuffer<T> {
    fn bind(&self, index: u32);
    fn bind_range(&self, index: u32, offset: usize, size: usize);
}

impl<V, B: GlBuffer<V>> GlUniformBuffer<V> for B {
    fn bind(&self, index: u32) {
        let handle = self.handle();
        gl_call!("GlUniformBuffer::bind", {
            gl::BindBufferBase(gl::UNIFORM_BUFFER, index, handle);
        });
    }

    fn bind_range(&self, index: u32, offset: usize, size: usize) {
        let handle = self.handle();
        let element_size = std::mem::size_of::<V>();
        let offset = (offset * element_size) as _;
        let size = (size * element_size) as _;
        gl_call!("GlUniformBuffer::bind_range", {
            gl::BindBufferRange(gl::UNIFORM_BUFFER, index, handle, offset, size);
        });
    }
}

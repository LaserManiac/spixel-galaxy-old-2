#![allow(unused)]

/// TODO: Comment

use std::marker::PhantomData;

use gl::types::*;

pub use self::gl_array_buffer::GlArrayBuffer;
pub use self::gl_dynamic_buffer::GlDynamicBuffer;
pub use self::gl_element_buffer::{GlElementBuffer, GlElementIndex};
pub use self::gl_mapped_buffer::GlMappedBuffer;
pub use self::gl_static_buffer::GlStaticBuffer;
pub use self::gl_uniform_buffer::GlUniformBuffer;

mod gl_array_buffer;
mod gl_dynamic_buffer;
mod gl_element_buffer;
mod gl_mapped_buffer;
mod gl_static_buffer;
mod gl_uniform_buffer;


pub trait GlBuffer<T> {
    fn handle(&self) -> GLuint;
    fn size(&self) -> usize;
}


pub struct GlBufferHandle<T> {
    handle: GLuint,
    size: usize,
    _marker: PhantomData<[T]>,
}

impl<T> GlBuffer<T> for GlBufferHandle<T> {
    fn handle(&self) -> GLuint {
        self.handle
    }

    fn size(&self) -> usize {
        self.size
    }
}

impl<T> GlBufferHandle<T> {
    pub fn new(data: &[T], flags: GLenum) -> Self {
        let size = data.len();
        let element_size = std::mem::size_of::<T>();
        let ptr = data.as_ptr() as _;
        Self::new_internal(size, element_size, ptr, flags)
    }

    pub fn with_capacity(size: usize, flags: GLenum) -> Self {
        let element_size = std::mem::size_of::<T>();
        let ptr = std::ptr::null();
        Self::new_internal(size, element_size, ptr, flags)
    }

    fn new_internal(size: usize, element_size: usize, ptr: *const std::ffi::c_void, flags: GLenum)
                    -> Self {
        let byte_size = (size * element_size) as _;
        assert_debug!(byte_size > 0, "Buffer size cannot be 0!");

        let mut handle = 0;
        gl_call!("GlBuffer::new_internal glCreateBuffers", {
            gl::CreateBuffers(1, &mut handle as _);
        });

        gl_call!("GlBuffer::new_internal glNamedBufferStorage", {
            gl::NamedBufferStorage(handle, byte_size, ptr, flags);
        });

        Self { handle, size, _marker: PhantomData }
    }
}

impl<T> Drop for GlBufferHandle<T> {
    fn drop(&mut self) {
        gl_call!("GlBuffer::drop", {
            gl::DeleteBuffers(1, &self.handle as _);
        });
    }
}

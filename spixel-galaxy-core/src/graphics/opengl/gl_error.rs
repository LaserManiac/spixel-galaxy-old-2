use gl::types::*;

use spg_macro::debug;

#[debug]
#[macro_export]
macro_rules! gl_call {
    ($x:literal, $y:expr) => {unsafe {
        $crate::graphics::opengl::clear_errors();
        let value = $y;
        $crate::graphics::opengl::panic_if_error($x);
        value
    }};
}

#[release]
#[macro_export]
macro_rules! gl_call {
    ($x:literal, $y:expr) => {unsafe {
        let value = $y;
        value
    }};
}

#[allow(unused)]
pub fn clear_errors() {
    while unsafe { gl::GetError() != gl::NO_ERROR } {};
}

pub fn panic_if_error(msg: &str) {
    if let Some(error) = get_error() {
        panic!("OpenGL {}: {}", error, msg);
    }
}

pub fn get_error() -> Option<&'static str> {
    match unsafe { gl::GetError() } {
        gl::INVALID_ENUM => Some("INVALID_ENUM"),
        gl::INVALID_VALUE => Some("INVALID_VALUE"),
        gl::INVALID_OPERATION => Some("INVALID_OPERATION"),
        gl::INVALID_FRAMEBUFFER_OPERATION => Some("INVALID_FRAMEBUFFER_OPERATION"),
        gl::OUT_OF_MEMORY => Some("OUT_OF_MEMORY"),
        gl::STACK_UNDERFLOW => Some("STACK_UNDERFLOW"),
        gl::NO_ERROR | _ => None,
    }
}


pub fn init_error_callback() {
    gl_call!("init_error_callback", {
        gl::Enable(gl::DEBUG_OUTPUT);
        gl::DebugMessageCallback(Some(error_callback), std::ptr::null());
    });
}

extern "system" fn error_callback(source: GLenum,
                                  gl_type: GLenum,
                                  _id: GLuint,
                                  severity: GLenum,
                                  length: GLsizei,
                                  message: *const GLchar,
                                  _user_param: *mut std::ffi::c_void) {
    match gl_type {
        gl::DEBUG_TYPE_ERROR
        | gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR
        | gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => (),
        _ => return,
    }

    let source = match source {
        gl::DEBUG_SOURCE_API => "API",
        gl::DEBUG_SOURCE_WINDOW_SYSTEM => "WINDOW_SYSTEM",
        gl::DEBUG_SOURCE_SHADER_COMPILER => "SHADER_COMPILER",
        gl::DEBUG_SOURCE_THIRD_PARTY => "THIRD_PARTY",
        gl::DEBUG_SOURCE_APPLICATION => "APPLICATION",
        gl::DEBUG_SOURCE_OTHER => "OTHER",
        _ => unreachable!(),
    };

    let gl_type = match gl_type {
        gl::DEBUG_TYPE_ERROR => "ERROR",
        gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => "DEPRECATED_BEHAVIOR",
        gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => "UNDEFINED_BEHAVIOR",
        gl::DEBUG_TYPE_PORTABILITY => "PORTABILITY",
        gl::DEBUG_TYPE_PERFORMANCE => "PERFORMANCE",
        gl::DEBUG_TYPE_MARKER => "MARKER",
        gl::DEBUG_TYPE_PUSH_GROUP => "PUSH_GROUP",
        gl::DEBUG_TYPE_POP_GROUP => "POP_GROUP",
        gl::DEBUG_TYPE_OTHER => "OTHER",
        _ => unreachable!(),
    };

    let severity = match severity {
        gl::DEBUG_SEVERITY_LOW => "LOW",
        gl::DEBUG_SEVERITY_MEDIUM => "MEDIUM",
        gl::DEBUG_SEVERITY_HIGH => "HIGH",
        gl::DEBUG_SEVERITY_NOTIFICATION => "NOTIFICATION",
        _ => unreachable!(),
    };

    let message = unsafe {
        let message = std::slice::from_raw_parts(message as *const u8, length as usize);
        std::str::from_utf8_unchecked(message)
    };

    spg_error!("[OpenGL|{}|{}|{}] {}", source, gl_type, severity, message);
}

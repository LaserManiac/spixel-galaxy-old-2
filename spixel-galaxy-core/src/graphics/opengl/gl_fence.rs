use gl::types::*;

pub struct GlFence {
    handle: GLsync
}

impl GlFence {
    pub fn new() -> Self {
        let handle = gl_call!("GlFence::new", {
            gl::FenceSync(gl::SYNC_GPU_COMMANDS_COMPLETE, 0)
        });
        Self { handle }
    }

    pub fn signaled(&self) -> bool {
        gl_call!("GlFence::signaled", {
            match gl::ClientWaitSync(self.handle, gl::SYNC_FLUSH_COMMANDS_BIT, 0) {
                gl::CONDITION_SATISFIED | gl::ALREADY_SIGNALED => true,
                _ => false,
            }
        })
    }
}

impl Drop for GlFence {
    fn drop(&mut self) {
        gl_call!("GlFence::drop", {
            gl::DeleteSync(self.handle);
        })
    }
}

#![allow(unused)]

use gl::types::*;

use super::{GlImageFormat, GlRenderbuffer, GlTexture, GlTextureDimension2D, GlTextureDimension3D};

#[derive(Copy, Clone)]
pub enum GlFramebufferAttachment {
    Color(u8),
    Depth,
    Stencil,
    DepthStencil,
}

impl GlFramebufferAttachment {
    pub fn to_gl_enum(&self) -> GLenum {
        match self {
            Self::Color(idx) => gl::COLOR_ATTACHMENT0 + *idx as GLenum,
            Self::Depth => gl::DEPTH_ATTACHMENT,
            Self::Stencil => gl::STENCIL_ATTACHMENT,
            Self::DepthStencil => gl::DEPTH_STENCIL_ATTACHMENT,
        }
    }
}


pub struct GlFramebuffer {
    handle: GLuint,
}

impl GlFramebuffer {
    pub fn new() -> Self {
        let mut handle = 0;

        gl_call!("GlFramebuffer::new", {
            gl::CreateFramebuffers(1, &mut handle as _);
        });

        Self { handle }
    }

    pub fn handle(&self) -> GLuint {
        self.handle
    }

    pub fn attach_renderbuffer<F>(&self, attachment: GlFramebufferAttachment, renderbuffer: &GlRenderbuffer<F>)
        where F: GlImageFormat {
        let attachment = attachment.to_gl_enum();
        let renderbuffer = renderbuffer.handle();
        gl_call!("GlFramebuffer::attach_renderbuffer", {
            gl::NamedFramebufferRenderbuffer(self.handle, attachment, gl::RENDERBUFFER, renderbuffer);
        });
    }

    pub fn attach_texture<T>(&self, attachment: GlFramebufferAttachment, texture: &T)
        where T: GlTexture + GlTextureDimension2D {
        let attachment = attachment.to_gl_enum();
        let texture = texture.handle();
        gl_call!("GlFramebuffer::attach_texture", {
            gl::NamedFramebufferTexture(self.handle, attachment, texture, 0);
        });
    }

    pub fn attach_texture_layer<T>(&self, attachment: GlFramebufferAttachment, texture: &T, layer: u32)
        where T: GlTexture + GlTextureDimension3D {
        let attachment = attachment.to_gl_enum();
        let texture = texture.handle();
        let layer = layer as _;
        gl_call!("GlFramebuffer::attach_texture_layer", {
            gl::NamedFramebufferTextureLayer(self.handle, attachment, texture, 0, layer);
        });
    }

    pub fn detach(&self, attachment: GlFramebufferAttachment) {
        let attachment = attachment.to_gl_enum();
        gl_call!("GlFramebuffer::detach", {
            gl::NamedFramebufferRenderbuffer(self.handle, attachment, gl::RENDERBUFFER, gl::NONE);
        });
    }

    pub fn set_draw_buffers(&self, attachments: &[GlFramebufferAttachment]) {
        let attachments = attachments.iter()
            .map(GlFramebufferAttachment::to_gl_enum)
            .collect::<Vec<_>>();
        let count = attachments.len();
        gl_call!("GlFramebuffer::set_draw_buffers", {
            gl::NamedFramebufferDrawBuffers(self.handle, count as _, attachments.as_ptr());
        });
    }

    pub fn completeness_error(&self) -> Option<&'static str> {
        let status = gl_call!("GlFramebuffer::check_completeness", {
            gl::CheckNamedFramebufferStatus(self.handle, gl::FRAMEBUFFER)
        });
        match status {
            gl::FRAMEBUFFER_INCOMPLETE_ATTACHMENT =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"),
            gl::FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"),
            gl::FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"),
            gl::FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"),
            gl::FRAMEBUFFER_INCOMPLETE_MULTISAMPLE =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"),
            gl::FRAMEBUFFER_INCOMPLETE_READ_BUFFER =>
                Some("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"),
            gl::FRAMEBUFFER_COMPLETE => None,
            _ => Some("UNKNOWN_FRAMEBUFFER_ERROR"),
        }
    }

    pub fn bind(&self) {
        gl_call!("GlFramebuffer::bind", {
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.handle);
        });
    }
}

impl Drop for GlFramebuffer {
    fn drop(&mut self) {
        gl_call!("GlFramebuffer::drop", {
            gl::DeleteFramebuffers(1, &mut self.handle as _);
        });
    }
}

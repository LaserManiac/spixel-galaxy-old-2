use gl::types::*;

pub trait GlImageFormat {
    const INTERNAL_FORMAT: GLenum;
    const FORMAT: GLenum;
    const COMPONENTS: u8;
    const DATA_FORMAT: GLenum;
    type DataType;
}


pub struct Rg8;

impl GlImageFormat for Rg8 {
    const INTERNAL_FORMAT: GLenum = gl::RG8;
    const FORMAT: GLenum = gl::RG;
    const COMPONENTS: u8 = 2;
    const DATA_FORMAT: GLenum = gl::UNSIGNED_BYTE;
    type DataType = u8;
}


pub struct Rgb8;

impl GlImageFormat for Rgb8 {
    const INTERNAL_FORMAT: GLenum = gl::RGB8;
    const FORMAT: GLenum = gl::RGB;
    const COMPONENTS: u8 = 3;
    const DATA_FORMAT: GLenum = gl::UNSIGNED_BYTE;
    type DataType = u8;
}


pub struct Rgba8;

impl GlImageFormat for Rgba8 {
    const INTERNAL_FORMAT: GLenum = gl::RGBA8;
    const FORMAT: GLenum = gl::RGBA;
    const COMPONENTS: u8 = 4;
    const DATA_FORMAT: GLenum = gl::UNSIGNED_BYTE;
    type DataType = u8;
}


pub struct Depth32;

impl GlImageFormat for Depth32 {
    const INTERNAL_FORMAT: GLenum = gl::DEPTH_COMPONENT32F;
    const FORMAT: GLenum = gl::DEPTH_COMPONENT;
    const COMPONENTS: u8 = 1;
    const DATA_FORMAT: GLenum = gl::FLOAT;
    type DataType = f32;
}

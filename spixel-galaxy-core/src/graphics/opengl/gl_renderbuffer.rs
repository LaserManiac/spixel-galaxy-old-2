/// TODO: Comment

use std::marker::PhantomData;

use gl::types::*;

use super::GlImageFormat;

pub struct GlRenderbuffer<F: GlImageFormat> {
    handle: GLuint,
    width: u32,
    height: u32,
    _marker: PhantomData<F>,
}

impl<F: GlImageFormat> GlRenderbuffer<F> {
    pub fn new(width: u32, height: u32) -> Self {
        let mut handle = 0;
        gl_call!("GlRenderbuffer::new glCreateRenderbuffers", {
            gl::CreateRenderbuffers(1, &mut handle as _);
        });

        gl_call!("GlRenderbuffer::new glNamedRenderbufferStorage", {
            gl::NamedRenderbufferStorage(handle, F::INTERNAL_FORMAT, width as _, height as _);
        });

        Self { handle, width, height, _marker: Default::default() }
    }

    pub fn handle(&self) -> GLuint {
        self.handle
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }
}

impl<F: GlImageFormat> Drop for GlRenderbuffer<F> {
    fn drop(&mut self) {
        gl_call!("GlRenderbuffer::drop", {
            gl::DeleteRenderbuffers(1, &mut self.handle as _);
        });
    }
}

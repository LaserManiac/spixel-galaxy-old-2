#![allow(unused)]

use gl::types::*;

pub struct GlShader {
    handle: GLuint
}

impl GlShader {
    pub fn new(vertex: &str, fragment: &str, geometry: Option<&str>) -> Result<Self, String> {
        let mut stages = Vec::<GlShaderStage>::new();
        stages.push(GlShaderStage::new(vertex, gl::VERTEX_SHADER)
            .map_err(|err| format!("Vertex shader failed to compile: {}", err))?);
        stages.push(GlShaderStage::new(fragment, gl::FRAGMENT_SHADER)
            .map_err(|err| format!("Fragment shader failed to compile: {}", err))?);
        if let Some(geometry) = geometry {
            stages.push(GlShaderStage::new(geometry, gl::GEOMETRY_SHADER)
                .map_err(|err| format!("Geometry shader failed to compile: {}", err))?);
        }

        let handle = gl_call!("GlShader::new create program", {
            gl::CreateProgram()
        });

        for stage in &stages {
            gl_call!("GlShader::new attach stage", gl::AttachShader(handle, stage.handle));
        }

        gl_call!("GlShader::new link program", {
            gl::LinkProgram(handle);
        });

        for stage in stages {
            gl_call!("GlShader::new detach stage", gl::DetachShader(handle, stage.handle));
        }

        let mut linked: GLint = 0;
        gl_call!("GlShader::new get link status", {
            gl::GetProgramiv(handle, gl::LINK_STATUS, &mut linked as _);
        });

        if linked == gl::TRUE as GLint {
            return Ok(Self { handle });
        }

        let mut log_len: GLint = 0;
        gl_call!("GlShader::new get info log length", {
            gl::GetProgramiv(handle, gl::INFO_LOG_LENGTH, &mut log_len as _);
        });

        let mut buffer = vec![0u8; log_len as usize];
        let buffer_ptr = buffer.as_mut_ptr() as *mut i8;
        gl_call!("GlShader::new get info log", {
            gl::GetProgramInfoLog(handle, log_len, std::ptr::null_mut(), buffer_ptr);
        });

        let log = unsafe { std::str::from_utf8_unchecked(&buffer) }.to_string();
        Err(log)
    }

    pub fn bind(&self) {
        gl_call!("GlShader::bind", {
            gl::UseProgram(self.handle);
        });
    }

    pub fn set_f32(&self, location: GLint, value: f32) {
        gl_call!("GlShader::set_f32", {
            gl::ProgramUniform1f(self.handle, location, value);
        });
    }

    pub fn set_i32(&self, location: GLint, value: i32) {
        gl_call!("GlShader::set_i32", {
            gl::ProgramUniform1i(self.handle, location, value);
        });
    }

    pub fn set_u32(&self, location: GLint, value: u32) {
        gl_call!("GlShader::set_u32", {
            gl::ProgramUniform1ui(self.handle, location, value);
        });
    }

    pub fn set_vec2(&self, location: GLint, value: glm::Vec2) {
        gl_call!("GlShader::set_vec2", {
            gl::ProgramUniform2f(self.handle, location, value.x, value.y);
        });
    }

    pub fn set_vec3(&self, location: GLint, value: glm::Vec3) {
        gl_call!("GlShader::set_vec3", {
            gl::ProgramUniform3f(self.handle, location, value.x, value.y, value.z);
        });
    }

    pub fn set_vec4(&self, location: GLint, value: glm::Vec4) {
        gl_call!("GlShader::set_vec4", {
            gl::ProgramUniform4f(self.handle, location, value.x, value.y, value.z, value.w);
        });
    }

    pub fn set_mat4(&self, location: GLint, value: &glm::Mat4) {
        gl_call!("GlShader::set_mat4", {
            gl::ProgramUniformMatrix4fv(self.handle, location, 1, gl::FALSE, value.as_ptr() as _);
        });
    }
}

impl Drop for GlShader {
    fn drop(&mut self) {
        gl_call!("GlShader::drop", {
            gl::DeleteProgram(self.handle);
        });
    }
}


struct GlShaderStage {
    handle: GLuint,
}

impl GlShaderStage {
    pub fn new(source: &str, stage: GLenum) -> Result<Self, String> {
        assert_debug!(source.is_ascii(), "Shader source must be ASCII!");

        let handle = gl_call!("GlShaderStage::new create stage", {
            gl::CreateShader(stage)
        });

        let src = source.as_ptr();
        let len = source.len() as GLint;
        let src_ptr = &src as *const *const u8 as *const *const GLchar;
        let len_ptr = &len as *const GLint;
        gl_call!("GlShaderStage::new compile stage", {
            gl::ShaderSource(handle, 1, src_ptr, len_ptr);
            gl::CompileShader(handle);
        });

        let mut success: GLint = 0;
        gl_call!("GlShaderStage::new get compile status", {
            gl::GetShaderiv(handle, gl::COMPILE_STATUS, &mut success as *mut _)
        });

        if success == gl::TRUE as GLint {
            return Ok(Self { handle });
        }

        let mut log_len: GLint = 0;
        gl_call!("GlShaderStage::new get info log length", {
            gl::GetShaderiv(handle, gl::INFO_LOG_LENGTH, &mut log_len as *mut _);
        });

        let mut buffer = vec![0u8; log_len as usize];
        let buffer_ptr = buffer.as_mut_ptr() as *mut i8;
        gl_call!("GlShaderStage::new get info log", {
            gl::GetShaderInfoLog(handle, log_len, std::ptr::null_mut(), buffer_ptr);
        });

        let log = unsafe { std::str::from_utf8_unchecked(&buffer) }.to_string();
        return Err(log);
    }
}

impl Drop for GlShaderStage {
    fn drop(&mut self) {
        gl_call!("GlShaderStage::drop", {
            gl::DeleteShader(self.handle);
        });
    }
}

use std::marker::PhantomData;

use super::{
    GlFormattedTexture,
    GlImageFormat,
    GlTexture,
    GlTextureDimension1D,
    GlTextureDimension2D,
    GlTextureDimension3D,
    GlTextureHandle,
    GlTextureSliceUpload2D,
};

pub struct GlArrayTexture2D<F: GlImageFormat> {
    handle: GlTextureHandle,
    width: u32,
    height: u32,
    layers: u32,
    _marker: PhantomData<F>,
}

impl<F: GlImageFormat> std::ops::Deref for GlArrayTexture2D<F> {
    type Target = GlTextureHandle;

    fn deref(&self) -> &Self::Target {
        &self.handle
    }
}

impl<F: GlImageFormat> GlFormattedTexture for GlArrayTexture2D<F> {
    type Format = F;
}

impl<F: GlImageFormat> GlTextureDimension1D for GlArrayTexture2D<F> {
    fn width(&self) -> u32 {
        self.width
    }
}

impl<F: GlImageFormat> GlTextureDimension2D for GlArrayTexture2D<F> {
    fn height(&self) -> u32 {
        self.height
    }
}

impl<F: GlImageFormat> GlTextureDimension3D for GlArrayTexture2D<F> {
    fn depth(&self) -> u32 {
        self.layers
    }
}

impl<F: GlImageFormat> GlArrayTexture2D<F> {
    pub fn new(width: u32, height: u32, layers: u32) -> Self {
        let handle = GlTextureHandle::new(gl::TEXTURE_2D_ARRAY);
        gl_call!("GlArrayTexture::new", {
            gl::TextureStorage3D(handle.handle(), 1, F::INTERNAL_FORMAT, width as _, height as _, layers as _);
        });
        Self { handle, width, height, layers, _marker: PhantomData }
    }
}

impl<F: GlImageFormat> GlTextureSliceUpload2D for GlArrayTexture2D<F> {}

use std::marker::PhantomData;

use gl::types::*;

use super::{
    GlFormattedTexture,
    GlImageFormat,
    GlTexture,
    GlTextureDimension1D,
    GlTextureDimension2D,
    GlTextureHandle,
    GlTextureUpload2D,
};

pub struct GlTexture2D<F: GlImageFormat> {
    handle: GlTextureHandle,
    width: u32,
    height: u32,
    _marker: PhantomData<F>,
}

impl<F: GlImageFormat> std::ops::Deref for GlTexture2D<F> {
    type Target = GlTextureHandle;

    fn deref(&self) -> &Self::Target {
        &self.handle
    }
}

impl<F: GlImageFormat> GlFormattedTexture for GlTexture2D<F> {
    type Format = F;
}

impl<F: GlImageFormat> GlTextureDimension1D for GlTexture2D<F> {
    fn width(&self) -> u32 {
        self.width
    }
}

impl<F: GlImageFormat> GlTextureDimension2D for GlTexture2D<F> {
    fn height(&self) -> u32 {
        self.height
    }
}

impl<F: GlImageFormat> GlTexture2D<F> {
    pub fn new(width: u32, height: u32) -> Self {
        let texture = GlTextureHandle::new(gl::TEXTURE_2D);
        gl_call!("GlTexture2D::new", {
            let width = width as _;
            let height = height as _;
            gl::TextureStorage2D(texture.handle(), 1, F::INTERNAL_FORMAT, width, height);
        });
        Self { handle: texture, width, height, _marker: PhantomData }
    }
}

impl<F: GlImageFormat> GlTextureUpload2D for GlTexture2D<F> {}

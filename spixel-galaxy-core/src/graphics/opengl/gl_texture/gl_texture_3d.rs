use std::marker::PhantomData;

use gl::types::*;

use super::{
    GlFormattedTexture,
    GlImageFormat,
    GlTexture,
    GlTextureDimension1D,
    GlTextureDimension2D,
    GlTextureDimension3D,
    GlTextureHandle,
    GlTextureSliceUpload2D,
    GlTextureUpload3D,
};

pub struct GlTexture3D<F: GlImageFormat> {
    handle: GlTextureHandle,
    width: u32,
    height: u32,
    depth: u32,
    _marker: PhantomData<F>,
}

impl<F: GlImageFormat> std::ops::Deref for GlTexture3D<F> {
    type Target = GlTextureHandle;

    fn deref(&self) -> &Self::Target {
        &self.handle
    }
}

impl<F: GlImageFormat> GlFormattedTexture for GlTexture3D<F> {
    type Format = F;
}

impl<F: GlImageFormat> GlTextureDimension1D for GlTexture3D<F> {
    fn width(&self) -> u32 {
        self.width
    }
}

impl<F: GlImageFormat> GlTextureDimension2D for GlTexture3D<F> {
    fn height(&self) -> u32 {
        self.height
    }
}

impl<F: GlImageFormat> GlTextureDimension3D for GlTexture3D<F> {
    fn depth(&self) -> u32 {
        self.depth
    }
}

impl<F: GlImageFormat> GlTexture3D<F> {
    pub fn new(width: u32, height: u32, depth: u32) -> Self {
        let texture = GlTextureHandle::new(gl::TEXTURE_3D);
        gl_call!("GlTexture3D::new", {
            let width = width as _;
            let height = height as _;
            let depth = depth as _;
            gl::TextureStorage3D(texture.handle(), 1, F::INTERNAL_FORMAT, width, height, depth);
        });
        Self { handle: texture, width, height, depth, _marker: PhantomData }
    }
}

impl<F: GlImageFormat> GlTextureSliceUpload2D for GlTexture3D<F> {}
impl<F: GlImageFormat> GlTextureUpload3D for GlTexture3D<F> {}

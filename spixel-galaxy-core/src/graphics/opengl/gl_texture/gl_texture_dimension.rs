use gl::types::*;

use super::{GlTexture, TextureWrap};

pub trait GlTextureDimension1D: GlTexture {
    fn width(&self) -> u32;

    fn set_wrap_x(&self, wrap: TextureWrap) {
        gl_call!("GlTextureDimension1D::set_wrap_x", {
            gl::TextureParameteri(self.handle(), gl::TEXTURE_WRAP_S, Into::<GLenum>::into(wrap) as _);
        })
    }
}

pub trait GlTextureDimension2D: GlTextureDimension1D {
    fn height(&self) -> u32;

    fn set_wrap_y(&self, wrap: TextureWrap) {
        gl_call!("GlTextureDimension2D::set_wrap_y", {
            gl::TextureParameteri(self.handle(), gl::TEXTURE_WRAP_T, Into::<GLenum>::into(wrap) as _);
        });
    }
}

pub trait GlTextureDimension3D: GlTextureDimension2D {
    fn depth(&self) -> u32;

    fn set_wrap_z(&self, wrap: TextureWrap) {
        gl_call!("GlTextureDimension3D::set_wrap_z", {
            gl::TextureParameteri(self.handle(), gl::TEXTURE_WRAP_R, Into::<GLenum>::into(wrap) as _);
        });
    }
}

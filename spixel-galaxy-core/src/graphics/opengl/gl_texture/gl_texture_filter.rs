use gl::types::*;

use super::GlTexture;

pub enum TextureFilter {
    Nearest,
    Linear,
}

impl Into<GLenum> for TextureFilter {
    fn into(self) -> GLenum {
        match self {
            Self::Nearest => gl::NEAREST,
            Self::Linear => gl::LINEAR,
        }
    }
}

use super::{GlFormattedTexture, GlImageFormat, GlTextureDimension2D, GlTextureDimension3D};

pub trait GlTextureUpload2D: GlFormattedTexture + GlTextureDimension2D {
    fn upload_2d(
        &self,
        x: u32,
        y: u32,
        width: u32,
        height: u32,
        data: &[<Self::Format as GlImageFormat>::DataType],
    ) {
        if cfg!(feature="debug") {
            let cx = x + width <= self.width();
            let cy = y + height <= self.height();
            assert_debug!(cx && cy, "Texture sub image is out of bounds!");

            let pixels = (width * height) as usize;
            let dl = pixels * Self::Format::COMPONENTS as usize == data.len();
            assert_debug!(dl, "Data length does not match region size!");
        }

        let handle = self.handle();
        let x = x as _;
        let y = y as _;
        let width = width as _;
        let height = height as _;
        let format = Self::Format::FORMAT;
        let typ = Self::Format::DATA_FORMAT;
        let ptr = data.as_ptr() as _;
        gl_call!("GlTextureUpload2D::upload for GlFormattedTexture + GlTextureDimension2D", {
            gl::TextureSubImage2D(handle, 0, x, y, width, height, format, typ, ptr);
        });
    }
}


pub trait GlTextureSliceUpload2D: GlFormattedTexture + GlTextureDimension3D {
    fn upload_slice_2d(
        &self,
        x: u32,
        y: u32,
        width: u32,
        height: u32,
        layer: u32,
        data: &[<Self::Format as GlImageFormat>::DataType],
    ) {
        if cfg!(feature="debug") {
            assert_debug!(layer < self.depth(), "Texture layer is out of bounds!");

            let cx = x + width <= self.width();
            let cy = y + height <= self.height();
            assert_debug!(cx && cy, "Texture sub image is out of bounds!");

            let pixels = (width * height) as usize;
            let dl = pixels * Self::Format::COMPONENTS as usize as usize == data.len();
            assert_debug!(dl, "Data length does not match region size!");
        }

        let handle = self.handle();
        let x = x as _;
        let y = y as _;
        let width = width as _;
        let height = height as _;
        let format = Self::Format::FORMAT;
        let layer = layer as _;
        let typ = Self::Format::DATA_FORMAT;
        let ptr = data.as_ptr() as _;
        gl_call!("GlTextureSliceUpload2D::upload for GlFormattedTexture + GlTextureDimension3D", {
            gl::TextureSubImage3D(handle, 0, x, y, layer, width, height, 1, format, typ, ptr);
        });
    }
}


pub trait GlTextureUpload3D: GlFormattedTexture + GlTextureDimension3D {
    fn upload_3d(
        &self,
        x: u32,
        y: u32,
        z: u32,
        width: u32,
        height: u32,
        depth: u32,
        data: &[<Self::Format as GlImageFormat>::DataType],
    ) {
        if cfg!(feature="debug") {
            let cx = x + width <= self.width();
            let cy = y + height <= self.height();
            let cz = z + depth <= self.depth();
            assert_debug!(cx && cy && cz, "Texture sub image is out of bounds!");

            let pixels = (width * height * depth) as usize;
            let dl = pixels * Self::Format::COMPONENTS as usize == data.len();
            assert_debug!(dl, "Data length does not match region size!");
        }

        let handle = self.handle();
        let x = x as _;
        let y = y as _;
        let z = z as _;
        let width = width as _;
        let height = height as _;
        let depth = depth as _;
        let format = Self::Format::FORMAT;
        let typ = Self::Format::DATA_FORMAT;
        let ptr = data.as_ptr() as _;
        gl_call!("GlTextureUpload3D::upload for GlFormattedTexture + GlTextureDimension3D", {
            gl::TextureSubImage3D(handle, 0, x, y, z, width, height, depth, format, typ, ptr);
        });
    }
}
use gl::types::*;

use super::{GlTexture, GlTextureDimension1D, GlTextureDimension2D, GlTextureDimension3D};

pub enum TextureWrap {
    Clamp,
    Repeat,
    Mirror,
}

impl Into<GLenum> for TextureWrap {
    fn into(self) -> GLenum {
        match self {
            Self::Clamp => gl::CLAMP_TO_EDGE,
            Self::Repeat => gl::REPEAT,
            Self::Mirror => gl::MIRRORED_REPEAT,
        }
    }
}

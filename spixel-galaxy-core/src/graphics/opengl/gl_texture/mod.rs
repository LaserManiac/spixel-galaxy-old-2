#![allow(unused)]

/// TODO: Comment

use gl::types::*;

pub use gl_array_texture_2d::GlArrayTexture2D;
pub use gl_texture_2d::GlTexture2D;
pub use gl_texture_3d::GlTexture3D;
pub use gl_texture_dimension::*;
pub use gl_texture_filter::TextureFilter;
pub use gl_texture_upload::*;
pub use gl_texture_wrap::TextureWrap;

use super::GlImageFormat;

pub mod gl_array_texture_2d;
pub mod gl_texture_2d;
pub mod gl_texture_3d;
pub mod gl_texture_dimension;
pub mod gl_texture_filter;
pub mod gl_texture_upload;
pub mod gl_texture_wrap;


pub trait GlTexture {
    fn handle(&self) -> GLuint;

    fn bind(&self, unit: u32) {
        gl_call!("GlTexture::bind", {
            gl::BindTextureUnit(unit as _, self.handle());
        });
    }

    fn set_min_filter(&self, filter: TextureFilter) {
        gl_call!("GlTexture::set_min_filter", {
            gl::TextureParameteri(self.handle(), gl::TEXTURE_MIN_FILTER, Into::<GLenum>::into(filter) as _);
        });
    }

    fn set_mag_filter(&self, filter: TextureFilter) {
        gl_call!("GlTexture::set_mag_filter", {
            gl::TextureParameteri(self.handle(), gl::TEXTURE_MAG_FILTER, Into::<GLenum>::into(filter) as _);
        });
    }
}

impl<T> GlTexture for T
    where T: std::ops::Deref,
          T::Target: GlTexture {
    fn handle(&self) -> GLuint {
        self.deref().handle()
    }
}


pub trait GlFormattedTexture: GlTexture {
    type Format: GlImageFormat;
}


pub struct GlTextureHandle {
    handle: GLuint
}

impl GlTextureHandle {
    pub fn new(target: GLenum) -> Self {
        let mut handle = 0;
        gl_call!("GlTexture::new", {
            gl::CreateTextures(target, 1, &mut handle as _)
        });
        Self { handle }
    }
}

impl GlTexture for GlTextureHandle {
    fn handle(&self) -> GLuint {
        self.handle
    }
}

impl Drop for GlTextureHandle {
    fn drop(&mut self) {
        gl_call!("GlTexture::drop", {
            gl::DeleteTextures(1, &self.handle as _)
        });
    }
}

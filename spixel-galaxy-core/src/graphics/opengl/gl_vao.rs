use gl::types::*;

use crate::graphics::{
    opengl::{GlArrayBuffer, GlBuffer, GlElementBuffer, GlElementIndex},
    vertex::{Attribute, AttributeType, Vertex},
};

pub struct GlVertexArray {
    handle: GLuint,
    attrib_count: u32,
    buffer_count: u32,
}

impl GlVertexArray {
    pub fn new() -> Self {
        let mut handle = 0;
        gl_call!("GlVertexArray create vao", {
            gl::CreateVertexArrays(1, &mut handle as _);
        });

        Self {
            handle,
            attrib_count: 0,
            buffer_count: 0,
        }
    }

    pub fn clear_format(&mut self) {
        for buf in 0..self.buffer_count {
            gl_call!("GlVertexArray::clear_format detach buffers", {
                gl::VertexArrayVertexBuffer(self.handle, buf, 0, 0, 0);
            });
        }
        self.buffer_count = 0;

        for attr in 0..self.attrib_count {
            gl_call!("GlVertexArray::clear_format disable attribs", {
                gl::DisableVertexArrayAttrib(self.handle, attr);
            });
        }
        self.attrib_count = 0;
    }

    pub fn attach_element_buffer<E, B>(&self, buffer: &B)
        where B: GlBuffer<E> + GlElementBuffer<E>,
              E: GlElementIndex {
        let vao = self.handle;
        let buffer = buffer.handle();
        gl_call!("GlVertexArray::attach_element_buffer", {
            gl::VertexArrayElementBuffer(vao, buffer);
        });
    }

    pub fn attach_vertex_buffer<B, V>(&mut self, buffer: &B, offset: usize)
        where B: GlBuffer<V> + GlArrayBuffer<V>,
              V: Vertex {
        let stride = std::mem::size_of::<V>();
        let offset = offset * stride;
        let format = V::FORMAT;
        self.attach_buffer(buffer, format, offset, stride);
    }

    pub fn attach_buffer<B, V>(
        &mut self,
        buffer: &B,
        format: &[Attribute],
        offset: usize,
        stride: usize,
    ) where B: GlBuffer<V> {
        let vao = self.handle;
        let buf_idx = self.buffer_count;
        let buf_handle = buffer.handle();
        let offset = offset as _;
        let stride = stride as _;
        gl_call!("GlVertexArray::attach_buffer glVertexArrayVertexBuffer", {
            gl::VertexArrayVertexBuffer(vao, buf_idx, buf_handle, offset, stride);
        });
        self.buffer_count += 1;

        for attr in format.iter() {
            let attr_idx = self.attrib_count;
            let gl_type = match attr.kind {
                AttributeType::F32 => gl::FLOAT,
                AttributeType::I32(..) => gl::INT,
                AttributeType::U32(..) => gl::UNSIGNED_INT,
                AttributeType::U16(..) => gl::UNSIGNED_SHORT,
                AttributeType::U8(..) => gl::UNSIGNED_BYTE,
            };
            let size = attr.size.components() as _;
            let offset = attr.offset as _;
            gl_call!("GlVertexArray set attribute format", {
                gl::EnableVertexArrayAttrib(vao, attr_idx);
                gl::VertexArrayAttribBinding(vao, attr_idx, buf_idx);

                if let Some(normalize) = attr.should_normalize() {
                    let norm = if normalize { gl::TRUE } else { gl::FALSE };
                    gl::VertexArrayAttribFormat(vao, attr_idx, size, gl_type, norm, offset);
                } else {
                    gl::VertexArrayAttribIFormat(vao, attr_idx, size, gl_type, offset);
                }

                if let Some(divisor) = attr.divisor {
                    assert_debug!(divisor > 0, "Attribute divisor cannot be 0!");
                    gl::VertexArrayBindingDivisor(vao, attr_idx, divisor);
                }
            });
            self.attrib_count += 1;
        }
    }

    pub fn bind(&self) {
        gl_call!("GlVertexArray::bind", {
            gl::BindVertexArray(self.handle);
        });
    }
}

impl Drop for GlVertexArray {
    fn drop(&mut self) {
        gl_call!("GlVertexArray::drop", {
            gl::DeleteVertexArrays(1, &self.handle as _);
        });
    }
}

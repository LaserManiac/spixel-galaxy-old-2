use gl::types::*;

pub use gl_buffer::*;
pub use gl_error::{clear_errors, get_error, init_error_callback, panic_if_error};
pub use gl_fence::GlFence;
pub use gl_framebuffer::{GlFramebuffer, GlFramebufferAttachment};
pub use gl_image_format::*;
pub use gl_renderbuffer::GlRenderbuffer;
pub use gl_shader::GlShader;
pub use gl_texture::*;
pub use gl_vao::GlVertexArray;

#[macro_use]
mod gl_error;

mod gl_buffer;
mod gl_fence;
mod gl_framebuffer;
mod gl_image_format;
mod gl_renderbuffer;
mod gl_shader;
mod gl_texture;
mod gl_vao;


pub fn print_gl_info() {
    let vendor = get_string(gl::VENDOR);
    let renderer = get_string(gl::RENDERER);
    let version = get_string(gl::VERSION);
    let shader = get_string(gl::SHADING_LANGUAGE_VERSION);
    spg_info!(
        "Renderer Info:\n\tOpenGL: {}\n\tGLSL: {}\n\tVendor: {} [{}]",
        version, shader, vendor, renderer
    );
}


pub fn get_string(name: GLenum) -> &'static str {
    unsafe {
        let ptr = gl::GetString(name);
        if ptr == std::ptr::null() {
            panic!("Invalid OpenGL string name!");
        }

        let mut end = ptr;
        while *end != 0 { end = end.offset(1); }
        let len = end as usize - ptr as usize;
        let bytes = std::slice::from_raw_parts(ptr, len);
        std::str::from_utf8_unchecked(bytes)
    }
}

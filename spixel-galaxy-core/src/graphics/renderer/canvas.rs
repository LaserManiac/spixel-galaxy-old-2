use std::sync::Arc;

use crate::graphics::opengl::*;

slotmap::new_key_type! { pub struct CanvasHandle; }


pub struct Canvas {
    pub framebuffer: GlFramebuffer,
    pub texture: Arc<GlTexture2D<Rgba8>>,
}

impl Canvas {
    pub fn new(width: u32, height: u32) -> Self {
        let framebuffer = GlFramebuffer::new();
        let texture = Arc::new(GlTexture2D::new(width, height));
        framebuffer.attach_texture(GlFramebufferAttachment::Color(0), texture.as_ref());
        framebuffer.set_draw_buffers(&[GlFramebufferAttachment::Color(0)]);

        if cfg!(feature="debug") {
            if let Some(err) = framebuffer.completeness_error() {
                panic!("Could not create canvas: {}!", err);
            }
        }

        Canvas { framebuffer, texture }
    }

    pub fn draw(&self) {
        self.framebuffer.bind();
        gl_call!("Canvas::draw", {
            gl::Viewport(0, 0, self.texture.width() as _, self.texture.height() as _);
        });
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        if self.texture.width() != width || self.texture.height() != height {
            self.texture = Arc::new(GlTexture2D::new(width, height));
            self.framebuffer.attach_texture(GlFramebufferAttachment::Color(0), self.texture.as_ref());
        }
    }
}

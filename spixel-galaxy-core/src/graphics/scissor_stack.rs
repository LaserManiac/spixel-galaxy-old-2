use crate::util::math::Rect;

pub struct ScissorStack {
    target_width: u32,
    target_height: u32,
    transform: na::Matrix4<f32>,
    stack: Vec<Rect<i32>>,
}

impl ScissorStack {
    pub fn new(target_width: u32, target_height: u32, transform: na::Matrix4<f32>) -> Self {
        Self {
            target_width,
            target_height,
            transform,
            stack: Default::default(),
        }
    }

    pub fn push(&mut self, rect: &Rect<f32>) -> bool {
        let from = self.transform * na::Point4::new(rect.x, rect.y, 0.0, 1.0);
        let to = self.transform * na::Point4::new(rect.x + rect.w, rect.y + rect.h, 0.0, 1.0);

        let x1 = ((from.x + 1.0) * 0.5 * self.target_width as f32) as i32;
        let y1 = ((from.y + 1.0) * 0.5 * self.target_height as f32) as i32;
        let x2 = ((to.x + 1.0) * 0.5 * self.target_width as f32) as i32;
        let y2 = ((to.y + 1.0) * 0.5 * self.target_height as f32) as i32;

        let x = x1.min(x2);
        let y = y1.min(y2);
        let w = x1.max(x2) - x;
        let h = y1.max(y2) - y;

        let rect = Rect::new(x, y, w, h);
        let next = if let Some(top) = self.stack.last() {
            *top & rect
        } else {
            rect
        };

        if next.w <= 0 || next.h <= 0 {
            return false;
        }

        self.stack.push(next);
        true
    }

    pub fn pop(&mut self) {
        self.stack.pop();
    }

    pub fn top(&mut self) -> Option<Rect<i32>> {
        self.stack.last().cloned()
    }
}

use std::collections::VecDeque;

use legion::prelude::*;
use shrev::*;
use slotmap::SecondaryMap;

use crate::{
    asset::{
        {Asset, Event, Get, LoadTask, Proxy, Storage},
        storage::{Key, HandleLike},
    },
    graphics::opengl::GlShader,
    util::progress::Progress,
};

/// # Asset
///
/// Source strings for a shader program.
///
#[derive(Clone)]
pub struct Shader {
    pub vertex: String,
    pub fragment: String,
    pub geometry: Option<String>,
}

impl Asset for Shader {
    /// A shader is identified by it's file name relative to the "./res" directory.
    ///
    type Id = String;

    fn loader(id: Self::Id, _: Progress, proxy: Proxy<Self>) -> LoadTask<Self> {
        Box::pin(async move {
            let path = format!("./res/{}", id);
            let mut source = async_std::fs::read_to_string(path).await
                .map_err(|err| format!("Could not load from disk: {}", err))?
                .lines()
                .map(|s| s.to_string())
                .collect::<VecDeque<_>>();

            let mut vertex = String::new();
            let mut fragment = String::new();
            let mut geometry = String::new();
            let mut version = None;

            let mut current = None;
            let mut line_idx = 0;
            while let Some(line) = source.pop_front() {
                line_idx += 1;
                if let Some(idx) = line.find("//#") {
                    let command = line[idx + 3..].trim();
                    if command.starts_with("version") {
                        if version.is_none() {
                            version = Some(command["version".len()..].trim().to_string());
                        } else {
                            Err(format!("Duplicate version specifier on line {}!", line_idx))?;
                        }
                    } else if command == "vertex" {
                        current = Some(&mut vertex);
                    } else if command == "fragment" {
                        current = Some(&mut fragment);
                    } else if command == "geometry" {
                        current = Some(&mut geometry)
                    } else if command.starts_with("include ") {
                        let file = command["include ".len()..].trim();
                        proxy.depend(file.to_string());
                        let path = format!("./res/{}", file);
                        let data = async_std::fs::read_to_string(path).await
                            .map_err(|err| format!("Could not include '{}': {}", file, err))?;
                        let lines = data.lines()
                            .map(|s| s.to_string())
                            .collect::<Vec<_>>();
                        for line in lines.into_iter().rev() {
                            source.push_front(line);
                        }
                    } else {
                        Err(format!("Unknown command '{}' on line {}!", command, line_idx))?
                    }
                } else if let Some(current) = &mut current {
                    current.push_str(&line);
                    current.push('\n');
                } else if !line.is_empty() {
                    Err(format!("Shader stage not specified before line {}!", line_idx))?;
                }
            }

            let version = version.ok_or_else(|| format!("Version not specified!"))?;
            let version = format!("#version {}\n", version);
            fragment.insert_str(0, &version);
            vertex.insert_str(0, &version);
            let geometry = if geometry.lines().count() > 1 {
                geometry.insert_str(0, &version);
                Some(geometry)
            } else {
                None
            };

            Ok(Shader { vertex, fragment, geometry })
        })
    }

    fn virtual_file_name(id: &Self::Id) -> Option<String> {
        Some(id.clone())
    }
}


/// # Resource
///
/// Stores OpenGL shader instances mapped to their handles.
///
/// __This resource must be accessed from the main thread only!__
///
#[derive(Default)]
pub struct Shaders(SecondaryMap<Key, GlShader>);

impl Shaders {
    pub fn get(&self, handle: &dyn HandleLike<Shader>) -> Option<&GlShader> {
        self.0.get(handle.key())
    }

    pub fn get_weak(&self, handle: &dyn HandleLike<Shader>) -> Option<&GlShader> {
        self.0.get(handle.key())
    }
}


/// # Resource
///
/// The event reader used to capture shader asset lifetime events.
///
struct ShaderEventReader(ReaderId<Event<Shader>>);


/// # Initializer
///
/// Initializes resources used by shader systems.
///
pub fn init_shaders(world: &mut World) {
    world.resources.insert(Shaders::default());

    let reader_id = world.resources
        .get_mut_or_default::<EventChannel<_>>()
        .unwrap()
        .register_reader();
    world.resources.insert(ShaderEventReader(reader_id));
}


/// # System
///
/// Captures shader asset lifetime events and creates or deletes OpenGL shader instances.  
/// The shaders are stored in the `Shaders` resource.
///
/// __This system must run on the main thread only!__
///
pub fn sys_shader_upload() -> Box<dyn Runnable> {
    SystemBuilder::new("ShaderUploadSystem")
        .read_resource::<Storage<Shader>>()
        .read_resource::<EventChannel<Event<Shader>>>()
        .write_resource::<ShaderEventReader>()
        .write_resource::<Shaders>()
        .build_thread_local(move |_, _, (storage, channel, reader, shaders), _| {
            for event in channel.read(&mut reader.0) {
                match event {
                    Event::Finished(handle) => match storage.get(handle) {
                        Get::Finished(Ok(shader)) => {
                            let shader = GlShader::new(
                                &shader.vertex,
                                &shader.fragment,
                                shader.geometry.as_deref(),
                            );
                            match shader {
                                Ok(shader) => {
                                    let old_shader = shaders.0.insert(handle.key(), shader);
                                    if old_shader.is_some() {
                                        spg_info!("[Shader] Recreated!");
                                    } else {
                                        spg_info!("[Shader] Created!");
                                    }
                                }
                                Err(err) => spg_warn!("[Shader] Creation error: {}", err),
                            }
                        }
                        Get::Finished(Err(err)) => {
                            spg_warn!("[Shader] Load error: {}", err);
                        }
                        _ => (),
                    },
                    Event::Deleted(handle) => match shaders.0.remove(handle.key()) {
                        Some(..) => spg_info!("[Shader] Deleted!"),
                        None => spg_warn!("[Shader] Attempted to delete nonexisting!"),
                    }
                }
            }
        })
}

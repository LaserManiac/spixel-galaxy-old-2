use serde::Deserialize;

use crate::{
    asset::{Asset, Handle, LoadTask, Proxy},
    graphics::{
        color::Color,
        image::Image,
    },
    util::{
        math::Rect,
        progress::Progress,
    },
};

pub struct Sprite {
    pub image: Handle<Image>,
    pub region: Rect<u32>,
    pub color: Color,
}

impl Asset for Sprite {
    type Id = String;

    fn loader(id: Self::Id, _: Progress, mut proxy: Proxy<Self>) -> LoadTask<Self> {
        Box::pin(async move {
            let path = format!("./res/{}", id);
            let data = async_std::fs::read_to_string(path).await
                .map_err(|err| format!("Could not load sprite from disk: {}", err))?;
            let data = serde_json::from_str::<SpriteData>(&data)
                .map_err(|err| format!("Could not parse sprite: {}", err))?;
            let image = proxy.load(data.image).await;
            let region = data.region;
            let color = data.color;
            Ok(Sprite { image, region, color })
        })
    }

    fn virtual_file_name(id: &Self::Id) -> Option<String> {
        Some(id.clone())
    }
}


#[derive(Deserialize)]
struct SpriteData {
    image: String,
    #[serde(flatten)]
    region: Rect<u32>,
    color: Color,
}

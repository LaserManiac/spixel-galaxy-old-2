use std::convert::TryFrom;

use crate::{
    asset::Handle,
    graphics::{
        color::Color,
        font::Font,
        image::Image,
    },
    util::math::Rect,
};

struct FontAdvSep {
    xadv: f32,
    yadv: f32,
    xsep: f32,
    ysep: f32,
}


#[derive(Clone)]
pub struct Text {
    pub(super) chars: Vec<CharCache>,
    pub(super) lines: Vec<LineCache>,
    pub(super) image: Handle<Image>,
    pub(super) glyph_width: f32,
    pub(super) glyph_height: f32,
    width: f32,
    height: f32,
}

impl Text {
    pub fn new(font: &Font, text: &str, size: f32, constraint: Option<f32>) -> Self {
        let scale = size / font.glyph_height as f32;
        let mut cached = Self {
            chars: Default::default(),
            lines: vec![Default::default()],
            image: font.image.clone(),
            glyph_width: font.glyph_width as f32 * scale,
            glyph_height: font.glyph_height as f32 * scale,
            width: 0.0,
            height: 0.0,
        };
        cached.push_text(font, text, scale, constraint);
        cached
    }

    fn push_text(&mut self, font: &Font, text: &str, scale: f32, constraint: Option<f32>) {
        let fas = FontAdvSep {
            xadv: font.glyph_width as f32 * scale,
            yadv: font.glyph_height as f32 * scale,
            xsep: font.separation_x as f32 * scale,
            ysep: font.separation_y as f32 * scale,
        };
        for chr in text.chars() {
            match chr {
                '\n' => self.new_line(&fas),
                '\t' => self.tabulate(4, constraint, &fas),
                ' ' => self.tabulate(1, constraint, &fas),
                chr => if let Some(region) = font.glyphs.get(&chr)
                    .or_else(|| font.glyphs.get(&'?')) {
                    self.push_char(region, constraint, &fas);
                },
            }
        }
    }

    fn tabulate(&mut self, amount: u32, constraint: Option<f32>, fas: &FontAdvSep) {
        if let Some(constraint) = constraint {
            let line = self.lines.last().unwrap();
            let separations = if line.width == 0.0 { amount - 1 } else { amount };
            let amount = amount as f32 * fas.xadv + separations as f32 * fas.xsep;
            if line.width + amount > constraint {
                self.new_line(fas);
            }
        }
        let line = self.lines.last_mut().unwrap();
        let separations = if line.width == 0.0 { amount - 1 } else { amount };
        let amount = amount as f32 * fas.xadv + separations as f32 * fas.xsep;
        line.width += amount;
        self.width = self.width.max(line.width);
    }

    fn push_char(&mut self, region: &Rect<u32>, constraint: Option<f32>, fas: &FontAdvSep) {
        if let Some(constraint) = constraint {
            let line = self.lines.last_mut().unwrap();
            let adv = if line.width == 0.0 { fas.xadv } else { fas.xadv + fas.xsep };
            if line.width + adv > constraint {
                self.new_line(fas);
            }
        }
        let line = self.lines.last_mut().unwrap();
        let sep = if line.width == 0.0 { 0.0 } else { fas.xsep };
        let adv = fas.xadv + sep;
        self.chars.push(CharCache {
            region: *region,
            pos_x: line.width + sep,
        });
        line.width += adv;
        line.end = self.chars.len();
        self.width = self.width.max(line.width);
    }

    fn new_line(&mut self, fas: &FontAdvSep) {
        let new_pos_y = self.lines.len() as f32 * (fas.yadv + fas.ysep);
        let current = self.lines.last_mut().unwrap();
        let new_start = current.end;
        let new_end = new_start;
        self.lines.push(LineCache {
            start: new_start,
            end: new_end,
            pos_y: new_pos_y,
            width: 0.0,
        });
        self.height = new_pos_y + fas.yadv;
    }

    pub fn width(&self) -> f32 {
        self.width
    }

    pub fn height(&self) -> f32 {
        self.height
    }
}


#[derive(Copy, Clone, Default)]
pub(super) struct LineCache {
    pub start: usize,
    pub end: usize,
    pub pos_y: f32,
    pub width: f32,
}


#[derive(Copy, Clone)]
pub(super) struct CharCache {
    pub pos_x: f32,
    pub region: Rect<u32>,
}


enum FormattedTextItem {
    Space,
    Tab,
    LineBreak,
    Char(char),
    Color(Color),
}

fn formatted_text_iterator<'a>(mut text: &'a str) -> impl Iterator<Item=FormattedTextItem> + 'a {
    std::iter::from_fn(move || {
        let chr = text.chars().next()?;
        text = &text[chr.len_utf8()..];
        let item = match chr {
            ' ' => FormattedTextItem::Space,
            '\t' => FormattedTextItem::Tab,
            '\n' => FormattedTextItem::LineBreak,
            '$' => {
                if let Some('{') = text.chars().next() {
                    if let Some(end_idx) = text[1..].find('}') {
                        let cmd = &text[1..end_idx + 1];
                        text = &text[end_idx + 2..];

                        if let Ok(color) = Color::try_from(cmd) {
                            return Some(FormattedTextItem::Color(color));
                        }
                    }
                }
                FormattedTextItem::Char(chr)
            }
            chr => FormattedTextItem::Char(chr),
        };
        Some(item)
    })
}

#![allow(unused)]

/// TODO: Comment

pub trait Vertex: Copy + Clone + Send + Sync + 'static {
    const FORMAT: &'static [Attribute];

    fn size() -> usize {
        Self::FORMAT.iter()
            .map(Attribute::bytes)
            .sum()
    }
}


#[derive(Copy, Clone)]
pub enum AttributeSize {
    Scalar,
    Vec2,
    Vec3,
    Vec4,
}

impl AttributeSize {
    pub fn components(&self) -> usize {
        match self {
            Self::Scalar => 1,
            Self::Vec2 => 2,
            Self::Vec3 => 3,
            Self::Vec4 => 4,
        }
    }
}


#[derive(Copy, Clone)]
pub enum AttributeType {
    F32,
    I32(AttributeNorm),
    U32(AttributeNorm),
    U16(AttributeNorm),
    U8(AttributeNorm),
}

impl AttributeType {
    pub fn bytes(&self) -> usize {
        match self {
            Self::F32 => 4,
            Self::I32(..) => 4,
            Self::U32(..) => 4,
            Self::U16(..) => 2,
            Self::U8(..) => 1,
        }
    }
}


#[derive(Copy, Clone)]
pub enum AttributeNorm {
    Normalized,
    NotNormalized,
}


#[derive(Copy, Clone)]
pub struct Attribute {
    pub size: AttributeSize,
    pub kind: AttributeType,
    pub offset: usize,
    pub divisor: Option<u32>,
}

impl Attribute {
    pub fn bytes(&self) -> usize {
        self.size.components() * self.kind.bytes()
    }

    pub fn should_normalize(&self) -> Option<bool> {
        match self.kind {
            AttributeType::F32 => Some(false),
            AttributeType::I32(n)
            | AttributeType::U32(n)
            | AttributeType::U16(n)
            | AttributeType::U8(n) => match n {
                AttributeNorm::Normalized => Some(true),
                AttributeNorm::NotNormalized => None,
            },
        }
    }
}

//! TODO: Comment

use std::{
    collections::VecDeque,
    mem::MaybeUninit,
};

use crate::{
    graphics::{
        opengl::*,
        vertex::*,
    },
    util::math::Rect,
};

pub const PAGE_SIZE: u32 = 4096;

pub const PAGE_COUNT: usize = 8;

pub const PAGE_REGIONS: usize = 128;

pub const TOTAL_REGIONS: usize = PAGE_COUNT * PAGE_REGIONS;


pub type RegionIndex = u16;

pub type PageIndex = u16;


pub struct VirtualTexture {
    region: RegionIndex,
    page: PageIndex,
}

impl VirtualTexture {
    pub fn index(&self) -> u32 {
        self.page as u32 * PAGE_REGIONS as u32 + self.region as u32
    }
}


#[repr(C)]
struct AtlasInfo {
    page_size: [u32; 2],
    _pad0: [u32; 2],
    coords: [Rect<u16>; TOTAL_REGIONS],
    transposed: [bool; TOTAL_REGIONS],
}

impl Default for AtlasInfo {
    fn default() -> Self {
        Self {
            page_size: [PAGE_SIZE, PAGE_SIZE],
            _pad0: Default::default(),
            coords: [Default::default(); TOTAL_REGIONS],
            transposed: [Default::default(); TOTAL_REGIONS],
        }
    }
}


/// # Resource
///
/// Stores paged virtual textures.
///
/// __This resource must be accessed from the main thread only!__
///
pub struct VirtualAtlas {
    texture: GlArrayTexture2D<Rgba8>,
    allocators: [RegionAllocator2D; PAGE_COUNT],
    available: [VecDeque<usize>; PAGE_COUNT],
    defrag: [bool; PAGE_COUNT],
    info: AtlasInfo,
    info_dirty: bool,
    info_ubo: GlDynamicBuffer<AtlasInfo>,
    util: DefragmentUtility,
}

impl Default for VirtualAtlas {
    fn default() -> Self {
        let texture = GlArrayTexture2D::new(PAGE_SIZE, PAGE_SIZE, PAGE_COUNT as _);
        texture.set_min_filter(TextureFilter::Nearest);
        texture.set_mag_filter(TextureFilter::Nearest);

        let mut allocators: [MaybeUninit<RegionAllocator2D>; PAGE_COUNT] = unsafe {
            MaybeUninit::uninit().assume_init()
        };
        for i in 0..PAGE_COUNT {
            let allocator = RegionAllocator2D::new(PAGE_SIZE, PAGE_SIZE);
            let ptr = allocators[i].as_mut_ptr();
            unsafe { ptr.write(allocator) };
        }
        let allocators = unsafe { std::mem::transmute(allocators) };

        let mut available: [MaybeUninit<VecDeque<usize>>; PAGE_COUNT] = unsafe {
            MaybeUninit::uninit().assume_init()
        };
        for i in 0..PAGE_COUNT {
            let deque = (0..PAGE_REGIONS).into_iter().collect();
            let ptr = available[i].as_mut_ptr();
            unsafe { ptr.write(deque) };
        }
        let available = unsafe { std::mem::transmute(available) };

        let info = Default::default();
        let info_dirty = false;
        let info_ubo = GlDynamicBuffer::new(std::slice::from_ref(&info));

        Self {
            texture,
            allocators,
            available,
            defrag: [false; PAGE_COUNT],
            info,
            info_dirty,
            info_ubo,
            util: DefragmentUtility::new(),
        }
    }
}

impl VirtualAtlas {
    pub fn allocate(&mut self, image: &image::RgbaImage) -> Option<VirtualTexture> {
        if image.width() > PAGE_SIZE || image.height() > PAGE_SIZE {
            return None;
        }

        for page in 0..PAGE_COUNT {
            if let result @ Some(_) = self.attempt_alloc_on_page(image, page) {
                return result;
            }
            if self.attempt_defragment(page) {
                if let result @ Some(_) = self.attempt_alloc_on_page(image, page) {
                    return result;
                }
            }
        }

        None
    }

    pub fn reallocate(&mut self, handle: VirtualTexture, image: &image::RgbaImage)
            -> Option<VirtualTexture> {
        let region = &self.info.coords[handle.index() as usize];
        if region.w as u32 == image.width() && region.h as u32 == image.height() {
            self.upload(image, &handle);
            Some(handle)
        } else {
            self.deallocate(handle);
            self.allocate(image)
        }
    }

    pub fn deallocate(&mut self, virtual_texture: VirtualTexture) {
        let page = virtual_texture.page as usize;
        let region = virtual_texture.region as usize;

        let available = &mut self.available[page];
        let insert_index = available.iter()
            .position(|idx| *idx > region)
            .unwrap_or(available.len());
        available.insert(insert_index, region);

        self.defrag[page] = true;
    }

    pub fn update(&mut self) {
        if self.info_dirty {
            self.info_ubo.upload(std::slice::from_ref(&self.info));
            self.info_dirty = false;
        }
    }

    pub fn bind(&self, ubo_binding: u32, texture_binding: u32) {
        if self.info_dirty {
            spg_warn!("Binding VirtualAtlas with dirty info!");
        }
        GlUniformBuffer::bind(&self.info_ubo, ubo_binding);
        self.texture.bind(texture_binding);
    }

    fn attempt_alloc_on_page(&mut self, image: &image::RgbaImage, page: usize)
                             -> Option<VirtualTexture> {
        let region = self.available[page].pop_front()?;
        let alloc_result = self.allocators[page].allocate(image.width(), image.height());
        if let Some((rect, transposed)) = alloc_result {
            let handle = VirtualTexture { region: region as _, page: page as _ };
            let index = handle.index() as usize;
            self.info.coords[index] = rect.to_primitive::<u16>().unwrap();
            self.info.transposed[index] = transposed;
            self.info_dirty = true;
            self.upload(image, &handle);
            Some(handle)
        } else {
            self.available[page].push_front(region);
            None
        }
    }

    fn upload(&self, image: &image::RgbaImage, handle: &VirtualTexture) {
        let index = handle.index() as usize;
        let layer = handle.page as _;
        let rect = &self.info.coords[index];
        let transposed = self.info.transposed[index];
        if !transposed {
            let data = image.as_ref();
            self.texture.upload_slice_2d(
                rect.x as _,
                rect.y as _,
                rect.w as _,
                rect.h as _,
                layer,
                data,
            );
        } else {
            let rotated = image::imageops::rotate90(image);
            let flipped = image::imageops::flip_horizontal(&rotated);
            let data = flipped.as_ref();
            self.texture.upload_slice_2d(
                rect.x as _,
                rect.y as _,
                rect.w as _,
                rect.h as _,
                layer,
                data,
            );
        }
    }

    fn attempt_defragment(&mut self, page: usize) -> bool {
        if !std::mem::replace(&mut self.defrag[page], false) {
            return false;
        }

        let mut taken = [true; PAGE_REGIONS];
        for idx in &self.available[page] {
            taken[*idx] = false;
        }

        let index = page * PAGE_REGIONS;
        let range = index..index + PAGE_REGIONS;
        let coords = &self.info.coords[range.clone()];
        let transposed = &self.info.transposed[range.clone()];

        let mut sorted_regions = coords.iter()
            .enumerate()
            .filter(|(idx, _)| taken[*idx])
            .collect::<Vec<_>>();
        sorted_regions.sort_by_cached_key(|(_, region)| {
            let max = region.w.max(region.h) as u32;
            let min = region.w.min(region.h) as u32;
            max * max + min
        });

        let mut new_regions = [Rect::<u16>::default(); PAGE_REGIONS];
        let mut new_transposed = [false; PAGE_REGIONS];
        let mut new_allocator = RegionAllocator2D::new(PAGE_SIZE, PAGE_SIZE);
        let mut transposed_on_defrag = [false; PAGE_REGIONS];
        for (idx, region) in sorted_regions.into_iter().rev() {
            let allocation = new_allocator.allocate(region.w as _, region.h as _);
            if let Some((region, transpose)) = allocation {
                new_regions[idx] = region.to_primitive::<u16>().unwrap();
                new_transposed[idx] = transpose;
                transposed_on_defrag[idx] = transposed[idx] ^ transpose;
            } else {
                return false;
            }
        }

        self.util.defragment(
            coords,
            &new_regions,
            &transposed_on_defrag,
            &taken,
            &self.texture,
            page,
        );
        self.allocators[page] = new_allocator;
        self.info.coords[range.clone()].copy_from_slice(&new_regions);
        self.info.transposed[range].copy_from_slice(&new_transposed);
        self.info_dirty = true;
        spg_info!("[VirtualAtlas] Defragmented page {}", page);
        true
    }
}


struct RegionAllocator2D {
    available: Vec<Rect<u32>>,
}

impl RegionAllocator2D {
    fn new(width: u32, height: u32) -> Self {
        Self {
            available: vec![Rect::new(0, 0, width, height)],
        }
    }

    fn allocate(&mut self, width: u32, height: u32) -> Option<(Rect<u32>, bool)> {
        let (slot, transposed) = self.find_optimal_slot(width, height)?;

        let rect = if !transposed {
            Rect::new(slot.x, slot.y, width, height)
        } else {
            Rect::new(slot.x, slot.y, height, width)
        };

        self.update_available_slots(&rect);

        Some((rect, transposed))
    }

    fn find_optimal_slot(&self, width: u32, height: u32) -> Option<(Rect<u32>, bool)> {
        self.available.iter()
            .flat_map(|slot| {
                std::iter::once((slot, Rect::new(slot.x, slot.y, width, height), false))
                    .chain(std::iter::once((slot, Rect::new(slot.x, slot.y, height, width), true)))
            })
            .filter_map(|(slot, rect, transposed)| {
                if slot.w >= rect.w && slot.h >= rect.h {
                    let intersection_area = self.available.iter()
                        .map(|slot| slot.intersection_area(&rect))
                        .sum::<u32>();
                    Some((slot, intersection_area, transposed))
                } else {
                    None
                }
            })
            .min_by_key(|(_, intersection_area, _)| *intersection_area)
            .map(|(slot, _, transposed)| (*slot, transposed))
    }

    fn update_available_slots(&mut self, rect: &Rect<u32>) {
        let mut to_add = Vec::new();
        for slot in self.available.drain(..) {
            if slot.intersects(rect) {
                for rect in slot.split(rect).iter() {
                    if rect.w > 0 && rect.h > 0 {
                        to_add.push(rect.to_primitive::<u32>().unwrap());
                    }
                }
            } else {
                to_add.push(slot);
            }
        };

        for rect in to_add {
            if !self.available.iter().any(|region| region.encompasses(&rect)) {
                self.available.push(rect);
            }
        }

        self.available.sort_by_cached_key(|region| region.y);
    }
}

struct DefragmentUtility {
    shader: GlShader,
    _copy_texture: GlTexture2D<Rgba8>,
    fbo: GlFramebuffer,
}

impl DefragmentUtility {
    fn new() -> Self {
        let shader = GlShader::new(DEFRAG_SHADER_VERTEX, DEFRAG_SHADER_FRAGMENT, None)
            .expect("Could not create shader for DefragmentUtility!");
        let _copy_texture = GlTexture2D::new(PAGE_SIZE as _, PAGE_SIZE as _);
        let fbo = GlFramebuffer::new();
        fbo.attach_texture(GlFramebufferAttachment::Color(0), &_copy_texture);
        Self { shader, _copy_texture, fbo }
    }

    fn defragment(
        &self,
        old: &[Rect<u16>],
        new: &[Rect<u16>],
        transposed: &[bool],
        taken: &[bool],
        texture: &GlArrayTexture2D<Rgba8>,
        page: usize,
    ) {
        let mut vertices = Vec::with_capacity(old.len() * 6);
        for index in 0..PAGE_REGIONS {
            if !taken[index] {
                continue;
            }

            let old = &old[index];
            let new = &new[index];
            let transposed = transposed[index];
            let [v0, v1, v2, v3] = DefragVertex::for_region(old, new, transposed);

            vertices.push(v0);
            vertices.push(v1);
            vertices.push(v2);

            vertices.push(v0);
            vertices.push(v2);
            vertices.push(v3);
        }

        let vbo = GlStaticBuffer::new(&vertices);
        let mut vao = GlVertexArray::new();
        vao.attach_vertex_buffer(&vbo, 0);
        vao.bind();

        self.shader.bind();
        self.shader.set_vec2(0, glm::Vec2::new(PAGE_SIZE as _, PAGE_SIZE as _));

        texture.bind(0);
        self.shader.set_i32(1, 0);
        self.shader.set_u32(2, page as _);

        self.fbo.bind();

        gl_call!("DefragmentUtility::defrag", {
            gl::Disable(gl::BLEND);
            gl::Disable(gl::SCISSOR_TEST);
            gl::Viewport(0, 0, PAGE_SIZE as _, PAGE_SIZE as _);

            gl::ClearColor(1.0, 0.0, 0.5, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);

            gl::DrawArrays(gl::TRIANGLES, 0, vertices.len() as _);
            gl::NamedFramebufferReadBuffer(self.fbo.handle(), gl::COLOR_ATTACHMENT0);
            gl::CopyTextureSubImage3D(
                texture.handle(), 0,
                0, 0, page as _,
                0, 0,
                PAGE_SIZE as _, PAGE_SIZE as _,
            );
            gl::Flush();
        });
    }
}


#[derive(Copy, Clone)]
#[repr(C)]
struct DefragVertex {
    src: [u16; 2],
    dst: [u16; 2],
}

impl Vertex for DefragVertex {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            kind: AttributeType::U16(AttributeNorm::NotNormalized),
            size: AttributeSize::Vec2,
            offset: 0,
            divisor: None,
        },
        Attribute {
            kind: AttributeType::U16(AttributeNorm::NotNormalized),
            size: AttributeSize::Vec2,
            offset: 4,
            divisor: None,
        },
    ];
}

impl DefragVertex {
    fn for_region(old: &Rect<u16>, new: &Rect<u16>, transposed: bool) -> [DefragVertex; 4] {
        let mut vertices = [
            DefragVertex {
                src: [old.x, old.y],
                dst: [new.x, new.y],
            },
            DefragVertex {
                src: [old.x, (old.y + old.h)],
                dst: [new.x, (new.y + new.h)],
            },
            DefragVertex {
                src: [(old.x + old.w), (old.y + old.h)],
                dst: [(new.x + new.w), (new.y + new.h)],
            },
            DefragVertex {
                src: [(old.x + old.w), old.y],
                dst: [(new.x + new.w), new.y],
            }
        ];

        if transposed {
            let src1 = vertices[1].src;
            vertices[1].src = vertices[3].src;
            vertices[3].src = src1;
        }

        vertices
    }
}


const DEFRAG_SHADER_VERTEX: &str = r#"
    #version 450

    in layout(location=0) uvec2 a_Src;
    in layout(location=1) uvec2 a_Dst;

    layout(location=0) uniform vec2 u_PageSize;

    out vec2 v_TexCoord;

    void main() {
        v_TexCoord = vec2(a_Src) / u_PageSize;
        vec2 position = vec2(a_Dst) / u_PageSize * 2.0 - 1.0;
        gl_Position = vec4(position, 0.0, 1.0);
    }
"#;

const DEFRAG_SHADER_FRAGMENT: &str = r#"
    #version 450

    in vec2 v_TexCoord;

    layout(location=1) uniform sampler2DArray u_Texture;
    layout(location=2) uniform uint u_PageIndex;

    out vec4 out_Color;

    void main() {
        out_Color = texture(u_Texture, vec3(v_TexCoord, u_PageIndex));
    }
"#;

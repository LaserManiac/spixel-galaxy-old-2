pub extern crate async_std;
pub extern crate futures;
pub extern crate image;
pub extern crate legion;
pub extern crate nalgebra as na;
pub extern crate nalgebra_glm as glm;
pub extern crate rand;
pub extern crate serde;
pub extern crate serde_json;
pub extern crate shrev;
pub extern crate simdnoise as noise;
#[macro_use]
extern crate spg_macro;

#[macro_use]
pub mod util;
#[macro_use]
pub mod graphics;
#[macro_use]
pub mod core;

pub mod asset;


pub fn version() -> &'static str {
    env!("CARGO_PKG_VERSION")
}

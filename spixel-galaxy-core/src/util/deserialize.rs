use serde::Deserialize;

/// Some data that can be specified either as a single value, or as a pair of values.
///
#[derive(Deserialize, Copy, Clone)]
#[serde(untagged)]
pub enum OneOrTwo<T> {
    One(T),
    Two(T, T),
}

impl<T: Clone> Into<[T; 2]> for OneOrTwo<T> {
    fn into(self) -> [T; 2] {
        match self {
            Self::One(a) => [a.clone(), a],
            Self::Two(a, b) => [a, b],
        }
    }
}


/// Some data that can be specified as a single value, as a pair, or as four distinct values.
///
/// If the value is a pair `[a, b]`, then it is resolved to `[a, b, a, b]`.
///
#[derive(Deserialize, Copy, Clone)]
#[serde(untagged)]
pub enum OneOrTwoOrFour<T> {
    OneOrTwo(OneOrTwo<T>),
    Four(T, T, T, T),
}

impl<T: Clone> Into<[T; 4]> for OneOrTwoOrFour<T> {
    fn into(self) -> [T; 4] {
        match self {
            Self::OneOrTwo(one_or_two) => {
                let [a, b]: [T; 2] = one_or_two.into();
                [a.clone(), b.clone(), a, b]
            }
            Self::Four(a, b, c, d) => [a, b, c, d],
        }
    }
}


/// For two-component values that can be specified as one parameter but also have separate
/// parameters for each component, this resolves to the proper values.
///
/// Eg. `size` can specify both `width` and `height`, but they can also be specified separately.
///
/// Standalone parameters always take priority over compound parameters.
/// If `size=[1, 4]` and `height=2`, the resulting values will be `[1, 2]`.
///
pub fn resolve_12<T: Clone>(all: Option<OneOrTwo<T>>, mut a: Option<T>, mut b: Option<T>)
                            -> [Option<T>; 2] {
    if let Some(all) = all {
        let [na, nb]: [T; 2] = all.into();
        a = a.or(Some(na));
        b = b.or(Some(nb));
    }
    [a, b]
}

/// For four-component values that can be specified as one parameter but also have separate
/// parameters for each component, this resolves to the proper values.
///
/// See `resolve_12` above.
///
pub fn resolve_124<T: Clone>(
    all: Option<OneOrTwoOrFour<T>>,
    mut a: Option<T>,
    mut b: Option<T>,
    mut c: Option<T>,
    mut d: Option<T>,
) -> [Option<T>; 4] {
    if let Some(all) = all {
        let [na, nb, nc, nd]: [T; 4] = all.into();
        a = a.or(Some(na));
        b = b.or(Some(nb));
        c = c.or(Some(nc));
        d = d.or(Some(nd));
    }
    [a, b, c, d]
}

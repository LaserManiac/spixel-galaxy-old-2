/// An interpolator function that transforms a value in range `[0.0, 1.0]` to a different value
/// in the same range.
///
pub type Interpolator = fn(f32) -> f32;

/// A linear interpolator: The value has a constant dX.
///
pub fn linear(x: f32) -> f32 {
    x
}

/// An ease-in interpolator: The value starts out fast and slows down near the end.
///
pub fn ease_in(x: f32) -> f32 {
    (x * std::f32::consts::FRAC_PI_2).sin().powf(0.5)
}

/// An ease-out interpolator: The value starts out slow and gradually speeds up towards the end.
///
pub fn ease_out(x: f32) -> f32 {
    1.0 - ((1.0 - x) * std::f32::consts::FRAC_PI_2).sin().powf(0.5)
}

/// An ease-in-out interpolator: The value starts out slow, then speeds up towards the middle, and
/// slows back down before the end.
///
pub fn ease_in_out(x: f32) -> f32 {
    (((x - 0.5) * std::f32::consts::PI).sin() * 0.5 + 0.5).powf(1.0 + (0.5 - x) * 2.0)
}


/// Used for interpolator deserialization.
///
#[derive(Copy, Clone, serde::Deserialize)]
pub enum InterpolatorKind {
    Linear,
    EaseIn,
    EaseOut,
    EaseInOut,
}

impl Default for InterpolatorKind {
    fn default() -> Self {
        Self::Linear
    }
}

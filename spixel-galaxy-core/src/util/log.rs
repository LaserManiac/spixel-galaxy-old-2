#[inline]
pub fn info(message: impl AsRef<str>) {
    println!("[INFO] {}", message.as_ref());
}

#[inline]
pub fn warn(message: impl AsRef<str>) {
    println!("[WARN] {}", message.as_ref());
}

#[inline]
pub fn error(message: impl AsRef<str>) {
    println!("[ERROR] {}", message.as_ref());
}

#[macro_export]
macro_rules! spg_info {
    ($x:expr $(, $y:expr)*) => {
        $crate::util::log::info(&format!($x $(, $y)*));
    }
}

#[macro_export]
macro_rules! spg_warn {
    ($x:expr $(, $y:expr)*) => {
        $crate::util::log::warn(&format!($x $(, $y)*));
    }
}

#[macro_export]
macro_rules! spg_error {
    ($x:expr $(, $y:expr)*) => {
        $crate::util::log::error(&format!($x $(, $y)*));
    }
}

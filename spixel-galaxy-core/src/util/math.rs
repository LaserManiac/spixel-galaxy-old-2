use std::{
    cmp::{PartialEq, PartialOrd},
    ops::*,
};

use na::{Matrix4, Point3, Unit, Vector3};
use serde::Deserialize;

pub fn max<T: PartialOrd>(a: T, b: T) -> T {
    if a >= b {
        a
    } else {
        b
    }
}


pub fn min<T: PartialOrd>(a: T, b: T) -> T {
    if a <= b {
        a
    } else {
        b
    }
}


pub fn basis_matrix(
    pos: Point3<f32>,
    xaxis: Unit<Vector3<f32>>,
    yaxis: Unit<Vector3<f32>>,
    zaxis: Unit<Vector3<f32>>,
) -> na::Matrix4<f32> {
    let dx = -xaxis.dot(&pos.coords);
    let dy = -yaxis.dot(&pos.coords);
    let dz = -zaxis.dot(&pos.coords);
    na::Matrix4::new(
        xaxis.x, xaxis.y, xaxis.z, dx,
        yaxis.x, yaxis.y, yaxis.z, dy,
        zaxis.x, zaxis.y, zaxis.z, dz,
        0.0, 0.0, 0.0, 1.0,
    )
}


pub fn basis_from_look(
    pos: Point3<f32>,
    dir: Unit<Vector3<f32>>,
    up: Unit<Vector3<f32>>,
) -> (Matrix4<f32>, Unit<Vector3<f32>>, Unit<Vector3<f32>>) {
    let right = Unit::new_normalize(dir.cross(&up));
    let up = Unit::new_normalize(right.cross(&dir));
    let basis = basis_matrix(pos, right, up, dir);
    (basis, right, up)
}


pub fn hex_digit(chr: char) -> Option<u8> {
    if chr >= 'a' && chr <= 'f' {
        Some(chr as u8 - 'a' as u8 + 10)
    } else if chr >= 'A' && chr <= 'F' {
        Some(chr as u8 - 'A' as u8 + 10)
    } else if chr >= '0' && chr <= '9' {
        Some(chr as u8 - '0' as u8)
    } else {
        None
    }
}


#[derive(Copy, Clone, serde::Deserialize)]
pub enum Alignment {
    Start,
    Center,
    End,
}

impl Alignment {
    pub fn get_aligned<T>(&self, start: T, end: T) -> T
        where T: Add<Output=T> + Div<Output=T> + num::One {
        match self {
            Alignment::Start => start,
            Alignment::End => end,
            Alignment::Center => (start + end) / (T::one() + T::one()),
        }
    }
}


#[derive(Copy, Clone, Eq, PartialEq, Hash, Deserialize)]
pub enum Orientation {
    Vertical,
    Horizontal,
}

impl Default for Orientation {
    fn default() -> Self {
        Self::Vertical
    }
}


#[derive(Copy, Clone, Default, Debug, Deserialize)]
pub struct Rect<T> {
    pub x: T,
    pub y: T,
    pub w: T,
    pub h: T,
}

impl<T> Rect<T> {
    pub fn new(x: T, y: T, w: T, h: T) -> Self {
        Self { x, y, w, h }
    }

    pub fn contains(&self, x: T, y: T) -> bool
        where T: Clone + Add<Output=T> + PartialOrd {
        x >= self.x
            && y >= self.y
            && x < self.x.clone() + self.w.clone()
            && y < self.y.clone() + self.h.clone()
    }

    pub fn encompasses(&self, other: &Rect<T>) -> bool
        where T: Clone + Add<Output=T> + PartialOrd {
        self.x <= other.x
            && self.y <= other.y
            && self.x.clone() + self.w.clone() >= other.x.clone() + other.w.clone()
            && self.y.clone() + self.h.clone() >= other.y.clone() + other.h.clone()
    }

    pub fn flip(&self) -> Rect<T> where T: Clone {
        Rect {
            x: self.x.clone(),
            y: self.y.clone(),
            w: self.h.clone(),
            h: self.w.clone(),
        }
    }

    pub fn to_primitive<B>(self) -> Option<Rect<B>>
        where T: num::NumCast,
              B: num::NumCast {
        Some(Rect {
            x: num::cast(self.x)?,
            y: num::cast(self.y)?,
            w: num::cast(self.w)?,
            h: num::cast(self.h)?,
        })
    }
}

impl Rect<u32> {
    pub fn intersects(&self, other: &Rect<u32>) -> bool {
        let dx = other.x as i32 - self.x as i32;
        let dy = other.y as i32 - self.y as i32;
        dx < self.w as i32
            && dy < self.h as i32
            && -dx < other.w as i32
            && -dy < other.h as i32
    }

    pub fn intersection_area(&self, other: &Rect<u32>) -> u32 {
        let l = self.x.max(other.x);
        let r = (self.x + self.w).min(other.x + other.w);
        if r <= l {
            return 0;
        }

        let t = self.y.max(other.y);
        let b = (self.y + self.h).min(other.y + other.h);
        if b <= t {
            return 0;
        }

        (r - l) * (b - t)
    }

    pub fn touches(&self, other: &Rect<u32>) -> bool {
        let dx = other.x as i32 - self.x as i32;
        let dy = other.y as i32 - self.y as i32;
        dx <= self.w as i32
            && dy <= self.h as i32
            && -dx <= other.w as i32
            && -dy <= other.h as i32
    }

    pub fn split(&self, other: &Rect<u32>) -> [Rect<i32>; 4] {
        let this = self.to_primitive::<i32>().unwrap();
        let other = other.to_primitive::<i32>().unwrap();

        let dxl = other.x - this.x;
        let dyt = other.y - this.y;
        let dxr = (this.x + this.w) - (other.x + other.w);
        let dyb = (this.y + this.h) - (other.y + other.h);

        let top = Rect::new(this.x, this.y, this.w, dyt);
        let left = Rect::new(this.x, this.y, dxl, this.h);
        let bottom = Rect::new(this.x, other.y + other.h, this.w, dyb);
        let right = Rect::new(other.x + other.w, this.y, dxr, this.h);

        [top, left, bottom, right]
    }
}

impl<T: Clone + Add<Output=T> + Sub<Output=T> + PartialOrd> BitAnd for Rect<T> {
    type Output = Rect<T>;

    fn bitand(self, rhs: Self) -> Self::Output {
        let x1 = max(self.x.clone(), rhs.x.clone());
        let y1 = max(self.y.clone(), rhs.y.clone());
        let x2 = min(self.x + self.w, rhs.x + rhs.w);
        let y2 = min(self.y + self.h, rhs.y + rhs.h);
        Rect {
            x: x1.clone(),
            y: y1.clone(),
            w: x2 - x1,
            h: y2 - y1,
        }
    }
}

impl<T> From<[T; 4]> for Rect<T> {
    fn from([x, y, w, h]: [T; 4]) -> Self {
        Self { x, y, w, h }
    }
}

impl<T> Into<[T; 4]> for Rect<T> {
    fn into(self) -> [T; 4] {
        [self.x, self.y, self.w, self.h]
    }
}

impl<T> Into<Sides<T>> for Rect<T> where T: Clone + Add<Output=T> {
    fn into(self) -> Sides<T> {
        Sides {
            t: self.y.clone(),
            l: self.x.clone(),
            b: self.y + self.h,
            r: self.x + self.w,
        }
    }
}


#[derive(Copy, Clone, Default, Debug, Deserialize)]
pub struct Sides<T> {
    pub t: T,
    pub l: T,
    pub b: T,
    pub r: T,
}

impl<T> Sides<T> {
    pub fn new(t: T, l: T, b: T, r: T) -> Self {
        Self { t, b, l, r }
    }
}

impl<T> From<[T; 4]> for Sides<T> {
    fn from([t, l, b, r]: [T; 4]) -> Self {
        Self { t, b, l, r }
    }
}

impl<T> Into<[T; 4]> for Sides<T> {
    fn into(self) -> [T; 4] {
        [self.t, self.l, self.b, self.r]
    }
}

impl<T> Into<Rect<T>> for Sides<T> where T: Clone + Sub<Output=T> {
    fn into(self) -> Rect<T> {
        Rect {
            x: self.l.clone(),
            y: self.t.clone(),
            w: self.r - self.l,
            h: self.b - self.t,
        }
    }
}

impl<T> Sides<&T> where T: Clone {
    pub fn cloned(&self) -> Sides<T> {
        Sides {
            t: self.t.clone(),
            l: self.l.clone(),
            b: self.b.clone(),
            r: self.r.clone(),
        }
    }
}

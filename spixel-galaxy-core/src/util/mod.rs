#[macro_use]
pub mod log;
pub mod takeable;
pub mod math;
pub mod progress;
pub mod deserialize;
pub mod interpolator;
pub mod region_map;


#[debug]
#[macro_export]
macro_rules! assert_debug {
    ($x:expr, $y:literal) => {
        assert!($x, $y);
    };
}

#[release]
#[macro_export]
macro_rules! assert_debug {
    ($x:expr, $y:literal) => {};
}


#[macro_export]
macro_rules! unwrap_or_return {
    ($x:expr) => {
        match $x {
            Some(value) => value,
            None => return,
        }
    };
    ($x:expr, $y:expr) => {
        match $x {
            Some(value) => value,
            None => return $y,
        }
    }
}


#[macro_export]
macro_rules! unwrap_or_continue {
    ($x:expr) => {
        match $x {
            Some(value) => value,
            None => continue,
        }
    }
}


pub trait Constructor {
    type Args;

    fn construct(args: Self::Args) -> Self;
}

use std::sync::{
    Arc,
    atomic::{AtomicU16, Ordering},
};

/// A thread safe progress tracker.
///
#[derive(Clone)]
pub struct Progress(Arc<AtomicU16>);

impl Default for Progress {
    fn default() -> Self {
        Self(Arc::new(AtomicU16::new(0)))
    }
}

impl Progress {
    /// Set the progress percentage.
    ///
    /// `value` is clamped to range `[0.0, 100.0]`.
    ///
    pub fn set(&self, value: f32) {
        let value = (value * 100.0) as u16;
        let value = u16::min(u16::max(value, 0), 10_000);
        self.0.store(value, Ordering::Release);
    }

    /// Get the progress percentage.
    ///
    /// The returned value will aways be in range `[0.0, 100.0]`.
    ///
    pub fn get(&self) -> f32 {
        u16::min(self.0.load(Ordering::Acquire), 10_000) as f32 / 100.0
    }
}

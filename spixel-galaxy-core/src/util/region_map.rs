use std::collections::HashSet;

pub struct RegionMap<T> {
    extent: glm::UVec3,
    position: glm::IVec3,
    density: usize,
    items: Vec<T>,
    region_cells: Vec<u32>,
    cell_regions: Vec<u32>,
}

impl<T> RegionMap<T> {
    pub fn new<'a, F>(extent: glm::UVec3, density: usize, position: glm::IVec3, mut gen: F) -> Self
        where F: FnMut(glm::IVec3, usize) -> T + 'a {
        let edge = extent * 2 + glm::UVec3::repeat(1);
        let cell_count = (edge.x * edge.y * edge.z) as usize;
        let item_count = cell_count * density;

        let mut items = Vec::with_capacity(item_count);
        let mut region_cells = vec![0; cell_count];
        let mut cell_regions = vec![0; cell_count];

        for idx in 0..cell_count as u32 {
            region_cells[idx as usize] = idx + 1;
            cell_regions[idx as usize] = idx;

            let local_x = idx % edge.x;
            let local_y = idx / edge.x % edge.y;
            let local_z = idx / (edge.x * edge.y);

            let global_x = position.x - extent.x as i32 + local_x as i32;
            let global_y = position.y - extent.y as i32 + local_y as i32;
            let global_z = position.z - extent.z as i32 + local_z as i32;

            let global = glm::IVec3::new(global_x, global_y, global_z);

            for item_idx in 0..density {
                items.push(gen(global, item_idx));
            }
        }

        Self {
            extent,
            position,
            density,
            items,
            region_cells,
            cell_regions,
        }
    }

    pub fn position(&self) -> glm::IVec3 {
        self.position
    }

    pub fn items(&self) -> &[T] {
        &self.items
    }

    pub fn cell_regions(&self) -> &[u32] {
        &self.cell_regions
    }

    pub fn extent(&self) -> glm::UVec3 {
        self.extent
    }

    pub fn density(&self) -> usize {
        self.density
    }

    pub fn view_distance(&self, region_size: f32) -> f32 {
        self.extent.x.max(self.extent.y).max(self.extent.z) as f32 * region_size
    }

    pub fn move_to<'a, F>(&mut self, position: glm::IVec3, update: F) -> bool
        where F: FnMut(glm::IVec3, bool, &mut [T]) + 'a {
        let delta = position - self.position;
        if delta != glm::zero::<glm::IVec3>() {
            self.move_by(delta, update);
            return true;
        }
        false
    }

    pub fn move_by<'a, F>(&mut self, delta: glm::IVec3, mut update: F)
        where F: FnMut(glm::IVec3, bool, &mut [T]) + 'a {
        self.position += delta;

        let edge_x = (self.extent.x * 2 + 1) as usize;
        let edge_y = (self.extent.y * 2 + 1) as usize;
        let edge_z = (self.extent.z * 2 + 1) as usize;

        let region_count = self.region_cells.len();
        let old_indices = std::mem::replace(&mut self.region_cells, vec![0; region_count]);
        let mut pending_regions = (0..region_count).into_iter().collect::<HashSet<_>>();
        let mut available_indices = (1..=region_count as u32).into_iter().collect::<HashSet<_>>();

        for region_idx in 0..region_count {
            let local_x = region_idx % edge_x;
            let local_y = region_idx / edge_x % edge_y;
            let local_z = region_idx / (edge_x * edge_y);

            let old_local_x = local_x as i32 + delta.x;
            let old_local_y = local_y as i32 + delta.y;
            let old_local_z = local_z as i32 + delta.z;

            let old_region_in_bounds = true
                && old_local_x >= 0
                && old_local_y >= 0
                && old_local_z >= 0
                && old_local_x < edge_x as i32
                && old_local_y < edge_y as i32
                && old_local_z < edge_z as i32;

            if old_region_in_bounds {
                let old_region_idx = {
                    let offset_x = old_local_x as usize;
                    let offset_y = old_local_y as usize * edge_x;
                    let offset_z = old_local_z as usize * edge_x * edge_y;
                    offset_x + offset_y + offset_z
                };
                let cell_idx = old_indices[old_region_idx];
                if cell_idx != 0 {
                    pending_regions.remove(&region_idx);
                    available_indices.remove(&cell_idx);
                    self.region_cells[region_idx] = cell_idx;
                    self.cell_regions[(cell_idx - 1) as usize] = region_idx as u32;

                    let cell = {
                        let from = (cell_idx as usize - 1) * self.density;
                        let to = from + self.density;
                        &mut self.items[from..to]
                    };

                    let global_x = self.position.x - self.extent.x as i32 + local_x as i32;
                    let global_y = self.position.y - self.extent.y as i32 + local_y as i32;
                    let global_z = self.position.z - self.extent.z as i32 + local_z as i32;

                    update(glm::IVec3::new(global_x, global_y, global_z), true, cell);
                }
            }
        }

        for (region_idx, cell_idx) in pending_regions.into_iter().zip(available_indices) {
            self.region_cells[region_idx] = cell_idx;
            self.cell_regions[(cell_idx - 1) as usize] = region_idx as u32;

            let local_x = region_idx % edge_x;
            let local_y = region_idx / edge_x % edge_y;
            let local_z = region_idx / (edge_x * edge_y);

            let global_x = self.position.x - self.extent.x as i32 + local_x as i32;
            let global_y = self.position.y - self.extent.y as i32 + local_y as i32;
            let global_z = self.position.z - self.extent.z as i32 + local_z as i32;

            let cell = {
                let from = (cell_idx as usize - 1) * self.density;
                let to = from + self.density;
                &mut self.items[from..to]
            };

            update(glm::IVec3::new(global_x, global_y, global_z), false, cell);
        }
    }
}

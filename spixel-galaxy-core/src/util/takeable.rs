use std::sync::atomic::{AtomicPtr, Ordering};

/// A wrapper around a value that can be 'taken', in which case the wrapper is left empty.
///
pub struct Takeable<T: Sync + 'static> {
    ptr: AtomicPtr<T>,
}

impl<T: Sync + 'static> Takeable<T> {
    /// Build a new takeable from the provided data.
    ///
    pub fn new(data: T) -> Self {
        let boxed = Box::new(data);
        Self::from_boxed(boxed)
    }

    /// Build a new takeable from the provided boxed data.
    ///
    pub fn from_boxed(data: Box<T>) -> Self {
        let ptr = AtomicPtr::new(Box::leak(data));
        Self { ptr }
    }

    /// Return the inner value.
    ///
    /// This method will panic if the value was already taken.
    ///
    pub fn take(&self) -> T {
        self.try_take().expect("Takeable is empty!")
    }

    /// Return the inner value, or `None` if the value was already taken.
    ///
    pub fn try_take(&self) -> Option<T> {
        let ptr = self.ptr.swap(std::ptr::null_mut(), Ordering::AcqRel);
        if ptr != std::ptr::null_mut() {
            let boxed = unsafe { Box::from_raw(ptr) };
            Some(*boxed)
        } else {
            None
        }
    }
}

/// Dropping a takeable drops the inner value if it hasn't been taken.
///
impl<T: Sync + 'static> Drop for Takeable<T> {
    fn drop(&mut self) {
        drop(self.try_take());
    }
}

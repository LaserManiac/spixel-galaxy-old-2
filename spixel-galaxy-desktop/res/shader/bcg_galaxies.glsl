//#version 450


//#vertex

layout(location=0) in vec2 a_BillboardPosition;
layout(location=1) in vec4 a_PositionSeed;

layout(location=0) uniform mat4 u_ViewProjection;
layout(location=1) uniform vec3 u_CameraRight;

out vec2 v_BillboardPosition;
out vec3 v_Color;
out float v_Brightness;

void main() {
    v_BillboardPosition = a_BillboardPosition;

    uint seed = floatBitsToUint(a_PositionSeed.w);
    float blueshift = float(seed % 123) / 123.0 * 0.25;
    v_Color = vec3(1.0 - blueshift, 0.75 - blueshift, 0.75);
    v_Brightness = 0.25 + float(seed % 321) / 321.0 * 0.5;

    vec3 position = a_PositionSeed.xyz;
    vec3 toStar = normalize(position);
    vec3 cameraUp = normalize(cross(u_CameraRight, toStar));
    vec3 billboardOffsetX = a_BillboardPosition.x * u_CameraRight;
    vec3 billboardOffsetY = a_BillboardPosition.y * cameraUp;
    float billboardScaleFactor = 0.02; //pow(1.0 - 0.9 * pow(distanceFactor, 10.0), 1.5);
    vec3 billboardOffset = (billboardOffsetX + billboardOffsetY) * billboardScaleFactor;
    vec3 vertexPosition = position + billboardOffset;
    gl_Position = u_ViewProjection * vec4(vertexPosition, 1.0);
}


//#fragment

in vec2 v_BillboardPosition;
in vec3 v_Color;
in float v_Brightness;

layout(location=0) out vec4 out_Color;

void main() {
    float glow = 1.0 - min(length(v_BillboardPosition) / 0.5, 1.0);
    float brightness = v_Brightness * pow(glow, 7.0);
    out_Color = vec4(v_Color, brightness);
}

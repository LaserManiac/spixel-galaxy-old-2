/// Calculates the vertex position for a billboard centered at `globalPos` with extent `localPos`.
/// The last two parameters are the origin point of the camera and the vector pointing right.
///
vec3 getBillboardFromRight(vec3 globalPos, vec2 localPos, vec3 cameraOrigin, vec3 cameraRight) {
    vec3 cameraUp = normalize(cross(cameraRight, globalPos - cameraOrigin));
    return globalPos + localPos.x * cameraRight + localPos.y * cameraUp;
}

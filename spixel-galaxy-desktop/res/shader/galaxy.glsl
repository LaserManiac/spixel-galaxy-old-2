//#version 450


//#vertex

layout(location=0) in vec3 a_Position;

layout(location=0) uniform mat4 u_ViewProjection;

out vec3 v_Position;

void main() {
    v_Position = a_Position;
    gl_Position = u_ViewProjection * vec4(a_Position * 10.0, 1.0);
}


//#fragment

#define M_PI 3.1415926535897932384626433832795

in vec3 v_Position;

layout(location=1) uniform sampler2D u_Noise;

layout(location=0) out vec4 out_Color;

void main() {
    float xf = (atan(v_Position.y, v_Position.x) / M_PI + 1.0) * 0.5;
    float yf = (atan(v_Position.z, length(v_Position.xy)) / M_PI * 2.0 + 1.0) * 0.5;

    const float GWIDTH = 0.6;
    const float GHEIGHT = 0.15;
    vec2 gp = vec2(
        clamp(((xf - 0.5 + GWIDTH * 0.5) / GWIDTH), 0.0, 1.0),
        clamp(((yf - 0.5 + GHEIGHT) / GHEIGHT * 0.5), 0.0, 1.0)
    );
    vec2 n1 = texture(u_Noise, gp).rg;
    vec2 n2 = texture(u_Noise, vec2(1.0) - gp).rg;
    vec2 gf = vec2(1.0) - clamp(abs(vec2(0.5) - gp) / 0.5, 0.0, 1.0);

    float glow = (1.0 - smoothstep(0.0, 1.0, length(vec2(1.0) - gf))) * 0.25;
    float lit = smoothstep(0.5 + 0.25 * (1.0 - gf.x), 1.0, gf.y) * n2.r * pow(gf.x, 2.0);
    float alpha = lit + glow * (1.0 - lit);
    float shadow = smoothstep(0.25 + 0.75 * (1.0 - gf.x), 1.0, gf.y) * n1.r * 1.25;

    vec3 shift = vec3(-0.1, 0.2, 0.3) * shadow;
    vec3 color = (vec3(1.0, 0.85, 0.75) - shift) * (1.0 - shadow);

    out_Color = vec4(color, alpha);
}
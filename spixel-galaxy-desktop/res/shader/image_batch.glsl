//#version 450

//#vertex
in layout(location=0) vec2 a_Position;
in layout(location=1) vec2 a_TexCoord;
in layout(location=2) vec4 a_Color;
in layout(location=3) uint a_TextureIndex;

layout(location=1)
uniform mat4 u_ViewProjection;

const uint PAGE_COUNT = 8;
const uint PAGE_REGIONS = 128;
const uint TOTAL_REGIONS = PAGE_COUNT * PAGE_REGIONS;

layout(std140, binding=0)
uniform AtlasInfo {
    uvec2 pageSize;
    uvec4 regions[TOTAL_REGIONS / 2];
    bvec4 transposed[TOTAL_REGIONS / 4];
} u_AtlasInfo;

flat out uint v_PageIndex;
flat out vec2 v_RegionStart;
flat out vec2 v_RegionEnd;
out vec2 v_TexCoord;
out vec4 v_Color;

void main() {
    v_PageIndex = a_TextureIndex / PAGE_REGIONS;

    uint vecIdx = a_TextureIndex / 2;
    uint cmpIdx = a_TextureIndex % 2 * 2;
    uvec2 regionPacked = uvec2(u_AtlasInfo.regions[vecIdx][cmpIdx], u_AtlasInfo.regions[vecIdx][cmpIdx + 1]);
    uvec2 regionPosition = uvec2(regionPacked.x & 0x0000FFFFu, regionPacked.x >> 16);
    uvec2 regionSize = uvec2(regionPacked.y & 0x0000FFFFu, regionPacked.y >> 16);
    vec2 pageSize = u_AtlasInfo.pageSize;
    v_RegionStart = (regionPosition + vec2(0.5)) / pageSize;
    v_RegionEnd = (regionPosition + regionSize - vec2(0.5)) / pageSize;

    uint transposed = uint(u_AtlasInfo.transposed[a_TextureIndex / 4][a_TextureIndex % 4]);
    uint not_transposed = 1 - transposed;
    float uvOffsetX = a_TexCoord[transposed];
    float uvOffsetY = a_TexCoord[not_transposed];
    v_TexCoord = (regionPosition + vec2(uvOffsetX, uvOffsetY)) / pageSize;

    v_Color = a_Color;

    gl_Position = u_ViewProjection * vec4(a_Position, 0.0, 1.0);
}

//#fragment
flat in uint v_PageIndex;
flat in vec2 v_RegionStart;
flat in vec2 v_RegionEnd;
in vec2 v_TexCoord;
in vec4 v_Color;

layout(location=0)
uniform sampler2DArray u_Atlas;

out vec4 out_Color;

void main() {
    vec2 uv = clamp(v_TexCoord, v_RegionStart, v_RegionEnd);
    vec4 smpl = texture(u_Atlas, vec3(uv, v_PageIndex));
    out_Color = smpl * v_Color;

    /*
    uint monitor = floatBitsToUint(out_Color.g);
    float factor = (gl_FragCoord.x - 200.0) / 800.0;
    if (factor < 0.0 || factor > 1.0) discard;
    uint idx = 31 - uint(factor * 32.0);
    if (fract(factor * 32.0) < (idx % 4 == 3 ? 0.2 : 0.05))
        discard;
    else if ((monitor & (0x1u << idx)) == 0)
        out_Color = vec4(1, 0, 0, 1);
    else
        out_Color = vec4(0, 1, 0, 1);
    // */
}

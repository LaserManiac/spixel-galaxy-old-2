//#version 450


//#vertex

layout(location=0) in vec3 a_CubePosition;
layout(location=1) in vec3 a_NebulaPosition;
layout(location=2) in vec3 a_NebulaSize;
layout(location=3) in uint a_RegionIndex;

layout(location=0) uniform mat4 u_ViewProjection;
layout(location=1) uniform uint u_RegionExtent;
layout(location=2) uniform float u_RegionSize;

out vec3 v_WorldPosition;
out float v_ViewDistance;
flat out uint v_TextureIndex;
out vec3 v_LocalPosition;

float viewDistance() {
    return float(u_RegionExtent) * u_RegionSize;
}

ivec3 regionCoordFromIndex(uint index) {
    uint edgeRegions = u_RegionExtent * 2 + 1;
    return ivec3(
        -int(u_RegionExtent) + uint(index % edgeRegions),
        -int(u_RegionExtent) + uint(index / edgeRegions % edgeRegions),
        -int(u_RegionExtent) + uint(index / edgeRegions / edgeRegions)
    );
}

vec3 regionOffsetFromIndex(uint index) {
    ivec3 coord = regionCoordFromIndex(index);
    return vec3(coord) * u_RegionSize;
}

void main() {
    vec3 regionOffset = regionOffsetFromIndex(a_RegionIndex);
    vec3 scaledCubePosition = a_CubePosition * a_NebulaSize * 0.1;
    vec3 vertexPosition = a_NebulaPosition + regionOffset + scaledCubePosition * 10.0;
    v_WorldPosition = vertexPosition;
    v_ViewDistance = viewDistance();
    v_TextureIndex = gl_InstanceID;
    v_LocalPosition = a_CubePosition;
    gl_Position = u_ViewProjection * vec4(vertexPosition, 1.0);
}

//#fragment

layout(location=3) uniform vec3 u_CameraPosition;
layout(location=4) uniform sampler3D u_Textures[8];

in vec3 v_WorldPosition;
in float v_ViewDistance;
flat in uint v_TextureIndex;
in vec3 v_LocalPosition;

//#include shader/region_intersect.mod.glsl

layout(location=0) out vec4 out_Color;

void main() {
    vec3 toCamera = u_CameraPosition - v_WorldPosition;
    float dstToCamera = length(toCamera);
    float dstFactor = 1.0 - dstToCamera / v_ViewDistance;
    vec4 s = texture(u_Textures[v_TextureIndex], v_LocalPosition + 0.5);
    vec3 color = vec3(s.g, 0.5, 1.0 - s.g);
    float alpha = max(s.r - 0.25, 0.0) / 0.75 * dstFactor;
    out_Color = vec4(vec3(0.0, s.g, 1.0 - s.g), 1);
}

/// Get the two intersection points between a ray and an axis-aligned, origin-centric region.
///
/// The `pos`, `dir`, and `regionExtents` vectors are assumed to be in region-local coordinates,
/// and the region is assumed to be centered around the origin.
///
/// Note that this function assumes that the provided ray intersects the cuboid.
/// If that is not the case, the returned values are undefined.
///
/// If the ray is inside the region, the returned `near` vector is the ray origin.
///
void getRegionIntersection(vec3 pos, vec3 dir, vec3 halfExtents, out vec3 near, out vec3 far) {
    vec3 lookAtNeg = vec3(greaterThan(dir, vec3(0.0)));
    vec3 lookAtPos = vec3(1.0) - lookAtNeg;
    vec3 planeDstNeg = (vec3(-halfExtents) - pos) / dir;
    vec3 planeDstPos = (vec3(halfExtents) - pos) / dir;
    vec3 dNearVec = lookAtPos * planeDstPos + lookAtNeg * planeDstNeg;
    vec3 dFarVec = lookAtNeg * planeDstPos + lookAtPos * planeDstNeg;
    float dNear = max(0.0, max(dNearVec.x, max(dNearVec.y, dNearVec.z)));
    float dFar = max(0.0, min(dFarVec.x, min(dFarVec.y, dFarVec.z)));
    near = pos + dir * dNear;
    far = pos + dir * dFar;
}

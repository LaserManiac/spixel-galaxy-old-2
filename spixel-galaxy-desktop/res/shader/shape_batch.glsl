//#version 450

//#vertex
in layout(location=0) vec3 a_Position;
in layout(location=1) vec4 a_Color;

layout(location=0) uniform mat4 u_ViewProjection;

out vec4 v_Color;

void main() {
    v_Color = a_Color;
    gl_Position = u_ViewProjection * vec4(a_Position, 1.0);
}

//#fragment
in vec4 v_Color;

out vec4 out_Color;

void main() {
    out_Color = v_Color;
}

//#version 450

//#vertex
in layout(location=0) vec3 a_Position;
in layout(location=1) vec4 a_Color;

layout(location=0) uniform mat4 u_ViewProjection;
layout(location=1) uniform mat4 u_CubeTransform;

out vec4 v_Color;
out vec3 v_LocalPosition;

void main() {
    v_Color = a_Color;
    v_LocalPosition = a_Position;
    gl_Position = u_ViewProjection * u_CubeTransform * vec4(a_Position, 1.0);
}

//#fragment
in vec4 v_Color;
in vec3 v_LocalPosition;

layout(location=1) uniform mat4 u_CubeTransform;
layout(location=2) uniform vec3 u_CameraPosition;
layout(location=3) uniform sampler3D u_SmokeMap;

//#include shader/region_intersect.mod.glsl

out vec4 out_Color;

void main() {
    vec3 localCameraPosition = (inverse(u_CubeTransform) * vec4(u_CameraPosition, 1.0)).xyz;
    vec3 localLookVec = normalize(v_LocalPosition - localCameraPosition);
    vec3 hitNear, hitFar;
    getRegionIntersection(localCameraPosition, localLookVec, vec3(0.5), hitNear, hitFar);

    const uint samples = 100;
    vec3 color = vec3(0.0);
    float alpha = 0.0;
    for(uint i = 0; i <= samples; i++) {
        vec3 samplePos = mix(hitNear, hitFar, float(i) / float(samples));
        float edgeFactor =
            (1.0 - smoothstep(0.25, 0.5, length(samplePos)))
            * (smoothstep(0.0, 0.25, length(samplePos)));
        vec4 sampleValue = texture(u_SmokeMap, samplePos + 0.5);
        //float sampleAlpha = ((max(sampleValue.r, 0.7) - 0.7) / 0.3) * 0.1;
        float sampleAlpha = sampleValue.a;
        float sampleRed = (max(sampleValue.r, 0.4) - 0.4) / 0.6;
        float density = sampleAlpha * sampleRed * edgeFactor * 0.05;
        float bf = (max(sampleValue.b, 0.5) - 0.5) / 0.5;
        vec3 makeup = vec3(
            (sampleValue.g) * bf * 0.75,
            (1.0 - sampleValue.g) * (1.0 - bf) * 0.15 + (sampleValue.g) * bf * 0.5,
            (1.0 - sampleValue.g) * bf * 1.5 + (1.0 - bf) * 0.08
        );
        makeup = vec3(sampleValue.g * 0.5, 0.2, sampleValue.b * 1.0);
        color += (1.0 - min(1.0, alpha)) * density * makeup;
        alpha = alpha + max(1.0 - alpha, 0.0) * density;
    }
    out_Color = vec4(normalize(color), alpha);
    //out_Color = vec4(texture(u_SmokeMap, v_LocalPosition + 0.5).rgb, 1.0);
}

//#version 450


//#vertex

layout(location=0) in vec2 a_BillboardPosition;
layout(location=1) in vec4 a_PositionSeed;
layout(location=2) in uint a_RegionIndex;

layout(location=0) uniform mat4  u_ViewProjection;
layout(location=1) uniform vec3  u_CameraPosition;
layout(location=2) uniform vec3  u_CameraRight;
layout(location=3) uniform uint  u_RegionExtent;
layout(location=4) uniform float u_RegionSize;

out vec2 v_BillboardPosition;
out float v_Brightness;
out vec3 v_Color;

//#include shader/billboard.mod.glsl

float viewDistance() {
    return float(u_RegionExtent) * u_RegionSize;
}

ivec3 regionCoordFromIndex(uint index) {
    uint edgeRegions = u_RegionExtent * 2 + 1;
    return ivec3(
        -int(u_RegionExtent) + uint(index % edgeRegions),
        -int(u_RegionExtent) + uint(index / edgeRegions % edgeRegions),
        -int(u_RegionExtent) + uint(index / edgeRegions / edgeRegions)
    );
}

vec3 regionOffsetFromIndex(uint index) {
    ivec3 coord = regionCoordFromIndex(index);
    return vec3(coord) * u_RegionSize;
}

void main() {
    v_BillboardPosition = a_BillboardPosition;

    uint seed = floatBitsToUint(a_PositionSeed.a);

    vec3 regionOffset = regionOffsetFromIndex(a_RegionIndex);
    vec3 starPosition = a_PositionSeed.xyz + regionOffset;
    float distance = length(u_CameraPosition - starPosition);
    float distanceFactor = 1.0 - distance / viewDistance();

    v_Brightness = distanceFactor;

    float billboardScaleFactor = pow(1.0 - 0.9 * pow(distanceFactor, 10.0), 1.5);
    vec2 scaledBillboardPosition = a_BillboardPosition * billboardScaleFactor;
    vec3 vertexPosition = getBillboardFromRight(
        starPosition,
        scaledBillboardPosition,
        u_CameraPosition,
        u_CameraRight
    );
    gl_Position = u_ViewProjection * vec4(vertexPosition, 1.0);

    float redshift = (1.0 - distanceFactor) * 0.75;
    float blueshift = float(seed % 457) / 456.0 * 0.8;
    float yellowshift = (1.0 - blueshift) * 0.25;
    v_Color = vec3(1.0 - blueshift, 1.0 - redshift - blueshift, 1.0 - redshift - yellowshift);
}


//#fragment

in vec2 v_BillboardPosition;
in float v_Brightness;
in vec3 v_Color;

layout(location=0) out vec4 out_Color;

void main() {
    float brightness = smoothstep(0.0, 0.9, v_Brightness);
    float dstFactor = 1.0 - min(length(v_BillboardPosition) / 0.5, 1.0);

    /*if (dstFactor < 0.75) {
        gl_FragDepth = 1.0;
    }*/

    float centerAlpha = 1.0;
    float glowAlpha = pow(dstFactor, 2.0) * (0.2);

    vec3 centerColor = vec3(1.0);
    vec3 glowColor = v_Color.rgb;

    float sizeFactor = 0.5 + v_Brightness * 0.45;
    float sizeTransition = sizeFactor + 0.005 + (1.0 - v_Brightness) * 0.5;

    float glowAlphaFactor = 1.0 - smoothstep(sizeFactor, sizeTransition, dstFactor);
    float glowColorFactor = 1.0 - smoothstep(sizeFactor, sizeTransition, dstFactor) * brightness;

    float alpha = mix(centerAlpha, glowAlpha, glowAlphaFactor) * brightness;
    vec3 color = mix(centerColor, glowColor, glowColorFactor);

    out_Color = vec4(color, alpha);
}

{
    "Stack": {
        "stylesheet": "template/test.stl",
        "size": "1w",
        "margin": 5,
        "padding": 5,
        "border": [0, 5],
        "border_style": ["white.spr", "00FFFF"],
        "children": {
            "Label": {
                "tags": ["title_label"],
                "width": "1w",
                "padding_top": 6,
                "padding_bottom": 4,
                "background": ["white.spr", "000020"],
                "font": "main.fnt",
                "text_size": 12,
                "text": "> Spixel Galaxy <",
                "text_color": "00FFFF",
                "text_align": "Center"
            },
            "Stack": {
                "tags": ["galaxy"],
                "size": "7w",
                "border": 2,
                "border_style": ["white.spr", "00557777"],
                "background": ["galaxy.spr", "7700FF"]
            },
            "Textbox": {
                "width": "1w",
                "border": 2,
                "border_style": ["white.spr", "005577FF"],
                "padding": [4, 4, 2, 4],
                "font": "main.fnt",
                "text_size": 8,
                "text_align": ["Start", "Center"],
                "placeholder": "Enter a meme!"
            },
            "Slider": {
                "width": "1w",
                "height": 6,
                "padding": 2,
                "border": 2,
                "border_style": ["white.spr", "00557777"],
                "knob": ["white.spr", "40B0B0"],
                "min": 0,
                "max": 100,
                "step": 1,
                "value": 0
            },
            "Stack": {
                "width": "1w",
                "orientation": "Horizontal",
                "margin_top": 2,
                "children": {
                    "Checkbox": {
                        "size": [8, "1w"],
                        "border": 2,
                        "border_style": ["white.spr", "005577FF"]
                    },
                    "Label": {
                        "tags": ["pick_label"],
                        "width": "1w",
                        "margin": 0,
                        "padding": [3, 4, 1, 4],
                        "font": "main.fnt",
                        "text": "Checkbox numero uno",
                        "text_size": 8
                    }
                }
            },
            "Stack": {
                "width": "1w",
                "orientation": "Horizontal",
                "margin_top": 2,
                "children": {
                    "Checkbox": {
                        "size": [8, "1w"],
                        "border": 2,
                        "border_style": ["white.spr", "005577FF"]
                    },
                    "Label": {
                        "tags": ["pick_label"],
                        "width": "1w",
                        "margin": 0,
                        "padding": [3, 4, 1, 4],
                        "font": "main.fnt",
                        "text": "Part II: Electric Boogaloo",
                        "text_size": 8
                    }
                }
            }
        }
    }
}
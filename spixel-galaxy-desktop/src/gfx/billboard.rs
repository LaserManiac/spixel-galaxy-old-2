use spg::graphics::{
    opengl::*,
    vertex::*,
};

/// A unit square indexed mesh centered around the origin used for billboard rendering.
///
/// __This struct must only be constructed on the main thread!__
///
pub struct BillboardMesh {
    vbo: GlStaticBuffer<glm::Vec2>,
    format: [Attribute; 1],
    size: usize,
}

impl BillboardMesh {
    pub fn new() -> Self {
        let vbo = GlStaticBuffer::new(&[
            glm::Vec2::new(-0.5, -0.5),
            glm::Vec2::new(-0.5, 0.5),
            glm::Vec2::new(0.5, 0.5),
            glm::Vec2::new(0.5, -0.5),
        ]);
        let format = [
            Attribute {
                size: AttributeSize::Vec2,
                kind: AttributeType::F32,
                offset: 0,
                divisor: None,
            }
        ];
        let size = std::mem::size_of::<glm::Vec2>();
        Self { vbo, format, size }
    }

    pub fn vbo(&self) -> &GlStaticBuffer<glm::Vec2> {
        &self.vbo
    }

    pub fn format(&self) -> &[Attribute] {
        &self.format
    }

    pub fn size(&self) -> usize {
        self.size
    }
}

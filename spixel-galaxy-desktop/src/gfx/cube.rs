use spg::graphics::{
    opengl::*,
    vertex::*,
};

/// A unit cube mesh centered around the origin with faces pointing inward.
///
/// __This struct must only be constructed on the main thread!__
///
pub struct UnitCubeMesh {
    ebo: GlStaticBuffer<u8>,
    vbo: GlStaticBuffer<glm::Vec3>,
    format: [Attribute; 1],
    size: usize,
}

impl UnitCubeMesh {
    pub fn new() -> Self {
        let ebo = GlStaticBuffer::new(&[
            // Z-
            0, 1, 2,
            0, 2, 3,
            // Z+
            4, 6, 5,
            4, 7, 6,
            // X-
            4, 1, 0,
            4, 5, 1,
            // X+
            6, 3, 2,
            6, 7, 3,
            // Y-
            7, 0, 3,
            7, 4, 0,
            // Y+
            5, 2, 1,
            5, 6, 2,
        ]);
        let vbo = GlStaticBuffer::new(&[
            glm::Vec3::new(-0.5, -0.5, -0.5),
            glm::Vec3::new(-0.5, 0.5, -0.5),
            glm::Vec3::new(0.5, 0.5, -0.5),
            glm::Vec3::new(0.5, -0.5, -0.5),
            glm::Vec3::new(-0.5, -0.5, 0.5),
            glm::Vec3::new(-0.5, 0.5, 0.5),
            glm::Vec3::new(0.5, 0.5, 0.5),
            glm::Vec3::new(0.5, -0.5, 0.5),
        ]);
        let format = [
            Attribute {
                size: AttributeSize::Vec3,
                kind: AttributeType::F32,
                offset: 0,
                divisor: None,
            }
        ];
        let size = std::mem::size_of::<glm::Vec3>();

        Self { ebo, vbo, format, size }
    }

    pub fn ebo(&self) -> &GlStaticBuffer<u8> {
        &self.ebo
    }

    pub fn vbo(&self) -> &GlStaticBuffer<glm::Vec3> {
        &self.vbo
    }

    pub fn format(&self) -> &[Attribute] {
        &self.format
    }

    pub fn size(&self) -> usize {
        self.size
    }
}

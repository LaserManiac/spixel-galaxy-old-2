extern crate nalgebra as na;
extern crate nalgebra_glm as glm;
#[macro_use]
extern crate spg;

use space::{
    {StarMapDestination, StarMapPosition},
    nebula::Nebula,
    star_coord::{STAR_REGION_SIZE_LY, StarCoord},
    star_map::StarMap,
};
use spg::{
    asset,
    core::{
        ContextView,
        state::{Event, GameState, StateConstructor, Trans},
        time::{DeltaTime, Ticker},
        window::WindowSize,
    },
    graphics::{
        batch::{
            image_batcher::ImageBatcher,
            shape_batcher::{ShapeBatcher, ShapeType},
        },
        camera::{ActiveCamera, Camera, fly_controller},
        color::Color,
        font::Font,
        image::{Image, Images},
        nine_patch::NinePatch,
        opengl::{self, GlTexture, GlTextureUpload3D},
        shader::{Shader, Shaders},
        sprite::Sprite,
        text::Text,
        virtual_atlas::VirtualAtlas,
    },
    legion::prelude::*,
    shrev::*,
    util::math::Alignment,
};

pub mod space;
pub mod gfx;


pub fn version() -> &'static str {
    env!("CARGO_PKG_VERSION")
}


struct GameData {
    #[allow(unused)]
    universe: Universe,
    world: World,
}


struct CoreState {
    seconds: f32,
    fps_ticker: Ticker,
    schedule: Schedule,
    image_batcher: ImageBatcher,
    shape_batcher: ShapeBatcher,
}

impl StateConstructor<GameData> for CoreState {
    type Args = ();

    fn construct(_: Self::Args, ctx: &mut ContextView<GameData>) -> Self {
        spg::graphics::opengl::init_error_callback();
        spg::graphics::opengl::print_gl_info();

        spg::core::ecs::init_entity_event_capture(&mut ctx.world);
        spg::core::time::init_timer(&mut ctx.world);
        spg::core::input::init_input(&mut ctx.world);
        let window_size = ctx.window().size();
        spg::core::window::init_window(&mut ctx.world, window_size);
        asset::listener::init_file_change_listener(&mut ctx.world);
        asset::init_asset_manager::<Shader>(&mut ctx.world);
        asset::init_asset_manager::<Image>(&mut ctx.world);
        asset::init_asset_manager::<Sprite>(&mut ctx.world);
        asset::init_asset_manager::<NinePatch>(&mut ctx.world);
        asset::init_asset_manager::<Font>(&mut ctx.world);
        asset::init_asset_manager::<Nebula>(&mut ctx.world);
        spg::graphics::shader::init_shaders(&mut ctx.world);
        spg::graphics::image::init_images(&mut ctx.world);
        space::init_star_map(StarCoord::default(), &mut ctx.world);

        let window_clear = move |world: &mut World| {
            let window_size = world.resources.get::<WindowSize>().unwrap();
            gl_call!("Set up rendering", {
                gl::CullFace(gl::FRONT);
                gl::Viewport(0, 0, window_size.width as _, window_size.height as _);
                gl::ClearColor(0.0, 0.0, 0.0, 1.0);
                gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            });
        };

        let camera_starmap_position_sync_before = move |world: &mut World| {
            let camera_position = {
                let active_camera = world.resources.get::<ActiveCamera>().unwrap();
                if let Some(camera) = world.get_component::<Camera>(active_camera.0) {
                    camera.position
                } else {
                    return;
                }
            };
            let mut star_map_position = world.resources.get_mut::<StarMapPosition>().unwrap();
            star_map_position.0.local = camera_position.coords;
        };

        let camera_starmap_position_sync_after = move |world: &mut World| {
            let star_map_position = {
                let star_map_position = world.resources.get_mut::<StarMapPosition>().unwrap();
                star_map_position.0.local
            };
            let active_camera = world.resources.get::<ActiveCamera>().unwrap();
            if let Some(mut camera) = world.get_component_mut::<Camera>(active_camera.0) {
                camera.position = na::Point3::from(star_map_position);
                camera.update();
            }
        };

        let schedule = Schedule::builder()
            .add_system(spg::core::ecs::sys_entity_event_capture())
            .add_system(spg::core::time::sys_timer())
            .add_system(spg::core::input::sys_input())
            .add_system(spg::core::window::sys_window_size_update())
            .add_system(asset::listener::sys_file_change_listener())
            .add_system(asset::sys_asset_manager::<Shader>("Shader"))
            .add_system(asset::sys_asset_manager::<Image>("Image"))
            .add_system(asset::sys_asset_manager::<Sprite>("Sprite"))
            .add_system(asset::sys_asset_manager::<NinePatch>("NinePatch"))
            .add_system(asset::sys_asset_manager::<Font>("Font"))
            .add_system(asset::sys_asset_manager::<Nebula>("Nebula"))
            .add_system(fly_controller::sys_fly_camera_controller())
            //.add_system(space::sys_star_map_travel_to_destination())
            .add_thread_local_fn(camera_starmap_position_sync_before)
            .add_system(space::sys_star_map_star_region_normalize())
            .add_system(space::sys_star_map_nebula_region_normalize())
            .add_thread_local_fn(camera_starmap_position_sync_after)
            .add_system(space::sys_star_map_star_update())
            .add_system(space::sys_star_map_nebula_update())
            .add_thread_local(spg::graphics::shader::sys_shader_upload())
            .add_thread_local(spg::graphics::image::sys_image_upload())
            .add_thread_local(space::sys_star_map_star_upload())
            .add_thread_local(space::sys_star_map_nebula_upload())
            .add_thread_local_fn(window_clear)
            .add_thread_local(space::sys_star_map_nebula_render())
            .add_thread_local(space::sys_star_map_star_render())
            .build();

        let window_size = ctx.window().size();
        let aspect = window_size.width as f32 / window_size.height as f32;
        let mut camera = Camera::new_perspective_3d(
            na::Point3::<f32>::from(glm::Vec3::repeat(0.5)),
            na::Unit::new_normalize(glm::Vec3::x()),
            na::Unit::new_unchecked(glm::Vec3::z()),
            aspect,
            60.0f32.to_radians(),
            0.01,
            1000.0,
        );
        let camera = ctx.world.insert((), Some((camera, )).into_iter())[0];
        ctx.world.resources.insert(ActiveCamera(camera));
        fly_controller::init_fly_camera_controlller(0.1, &mut ctx.world);

        let image_batcher = ImageBatcher::new(
            asset::load("shader/image_batch.glsl", &mut ctx.world)
        );
        let shape_batcher = ShapeBatcher::new(
            asset::load("shader/smoke.glsl", &mut ctx.world)
        );

        Self {
            schedule,
            seconds: 0.0,
            fps_ticker: Ticker::with_tps(2),
            image_batcher,
            shape_batcher,
        }
    }
}

impl GameState<GameData> for CoreState {
    fn run(&mut self, ctx: &mut ContextView<GameData>) -> Trans<GameData> {
        self.schedule.execute(&mut ctx.world);

        let delta = ctx.world.resources.get::<DeltaTime>().unwrap().as_secs();
        self.seconds = (self.seconds + delta) % 16.0;
        let fps = 1.0 / delta;
        if self.fps_ticker.tick() {
            //spg_info!("FPS: {}", fps as u32);
        }

        Trans::None
    }

    fn event(&mut self, ctx: &mut ContextView<GameData>, event: &Event) -> Option<Trans<GameData>> {
        ctx.world.resources
            .get_mut_or_default::<EventChannel<Event>>()
            .unwrap()
            .single_write(*event);

        use spg::core::input::{Key, State};
        if let Event::Key { key: Key::Escape, state: State::Pressed } = event {
            ctx.window_mut().set_minimized(true);
        } else if let Event::Key { key: Key::Space, state: State::Pressed } = event {
            let mut coord = ctx.world.resources.get::<StarMapPosition>().unwrap().0;
            use spg::rand::prelude::*;

            let rx = spg::rand::thread_rng().gen_range(-10, 10);
            let ry = spg::rand::thread_rng().gen_range(-10, 10);
            let rz = spg::rand::thread_rng().gen_range(-10, 10);
            coord += glm::IVec3::new(rx, ry, rz);

            let lx = spg::rand::thread_rng().gen_range(0.0, STAR_REGION_SIZE_LY);
            let ly = spg::rand::thread_rng().gen_range(0.0, STAR_REGION_SIZE_LY);
            let lz = spg::rand::thread_rng().gen_range(0.0, STAR_REGION_SIZE_LY);
            coord += glm::Vec3::new(lx, ly, lz);

            ctx.world.resources
                .get_mut::<StarMapDestination>()
                .unwrap()
                .set(coord.normalized(), 2.5);
        }

        if let Event::Close = event { Some(Trans::Exit) } else { None }
    }

    fn name(&self) -> &'static str {
        "CoreState"
    }
}


fn main() {
    let universe = Universe::new();
    let world = universe.create_world();
    let game_data = GameData { universe, world };

    spg_info!("Desktop version {}!", version());
    use spg::core::GameDriver;
    let window_info = spg::core::window::ConstructInfo {
        title: format!("Spixel Galaxy {}", crate::version()),
        fullscreen: true,
        ..Default::default()
    };

    spg::core::GlutinDriver::run::<GameData, CoreState>(game_data, (), window_info);
}

use legion::prelude::*;
use rand::prelude::*;
use shrev::*;
use slotmap::SecondaryMap;

use spg::{
    asset::{
        {Event, Get, Handle, load, Loader, ResultRef, Storage},
        storage::{HandleLike, Key},
    },
    core::time::DeltaTime,
    graphics::{
        camera::{ActiveCamera, Camera},
        opengl::*,
        shader::{Shader, Shaders},
        vertex::*,
    },
    util::region_map::RegionMap,
};

use crate::{
    gfx::{
        billboard::BillboardMesh,
        cube::UnitCubeMesh,
    },
    space::{
        nebula::{Nebula, NEBULA_REGION_SCALE, NEBULA_REGION_SIZE_LY, NebulaVertex},
        star_coord::{STAR_REGION_SIZE_LY, StarCoord},
    },
};

pub mod star_map;
pub mod star_coord;
pub mod nebula;


pub const NEBULA_MAX_BATCH_SIZE: usize = 8;


/// # Resource
///
/// The current position in the starmap.
///
#[derive(Copy, Clone)]
pub struct StarMapPosition(pub StarCoord);


/// # Resource
///
/// The destination coordinates for the starmap.
///
pub struct StarMapDestination {
    coord: Option<StarCoord>,
    initial: Option<StarCoord>,
    travel_time: f32,
    timer: f32,
}

impl Default for StarMapDestination {
    fn default() -> Self {
        Self {
            coord: None,
            initial: None,
            travel_time: 0.0,
            timer: 0.0,
        }
    }
}

impl StarMapDestination {
    pub fn set(&mut self, coord: StarCoord, travel_time: f32) {
        self.coord = Some(coord);
        self.initial = None;
        self.travel_time = travel_time;
        self.timer = 0.0;
    }
}


/// # Data
///
/// A single star.
///
#[derive(Copy, Clone)]
pub struct Star {
    pub position: glm::Vec3,
    pub seed: u32,
}

impl Vertex for Star {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            size: AttributeSize::Vec4,
            kind: AttributeType::F32,
            offset: 0,
            divisor: Some(1),
        }
    ];
}


/// # Resource
///
/// Stars on the star map.
///
pub struct StarMapStars {
    region_map: RegionMap<Star>,
}

impl StarMapStars {
    fn new(extent: glm::UVec3, position: glm::IVec3, density: usize) -> Self {
        Self {
            region_map: RegionMap::new(extent, density, position, gen_star)
        }
    }
}

fn gen_star(pos: glm::IVec3, idx: usize) -> Star {
    let seed = pos.x as u64 * 123456
        + pos.y as u64 * 456789
        + pos.z as u64 * 789123
        + idx as u64 * 741852;
    let mut rng = StdRng::seed_from_u64(seed);

    let x = rng.gen_range(0.0, STAR_REGION_SIZE_LY);
    let y = rng.gen_range(0.0, STAR_REGION_SIZE_LY);
    let z = rng.gen_range(0.0, STAR_REGION_SIZE_LY);

    Star {
        position: glm::Vec3::new(x, y, z),
        seed: seed as u32,
    }
}

fn update_stars(pos: glm::IVec3, moved: bool, items: &mut [Star]) {
    if !moved {
        for (idx, star) in items.iter_mut().enumerate() {
            *star = gen_star(pos, idx);
        }
    }
}


/// # Resource
///
/// An event reader used to capture star map region transition events for star update.
///
pub struct StarMapStarUpdateStarRegionTransitionEventReader(
    ReaderId<StarMapStarRegionTransitionEvent>
);


/// # Resource
///
/// Nebula region coordinates.
///
#[derive(Copy, Clone)]
pub struct StarMapNebulaRegionPosition(pub glm::IVec3);


/// # Resource
///
/// Nebulae on the star map.
///
pub struct StarMapNebulae {
    region_map: RegionMap<Option<Handle<Nebula>>>,
}

impl StarMapNebulae {
    fn new(extent: glm::UVec3, position: glm::IVec3, world: &mut World) -> Self {
        let mut loader = world.resources.get_mut().unwrap();
        let mut storage = world.resources.get_mut().unwrap();
        let generator = move |pos, idx| gen_nebula(pos, idx, &mut loader, &mut storage);
        let region_map = RegionMap::new(extent, 1, position, generator);
        Self { region_map }
    }
}

/// Queues a single nebula in a region map for loading using the given parameters.
///
fn gen_nebula(
    pos: glm::IVec3,
    idx: usize,
    loader: &mut Loader<Nebula>,
    storage: &mut Storage<Nebula>,
) -> Option<Handle<Nebula>> {
    let seed = pos.x as u64 * 123456
        + pos.y as u64 * 456789
        + pos.z as u64 * 789123
        + idx as u64 * 741852;
    let mut rng = StdRng::seed_from_u64(seed);

    // A chance for a region to contain a nebula:
    if rng.gen_ratio(1, 100) {
        Some(loader.load(pos, storage))
    } else {
        None
    }
}

/// Updates an array of nebulae in a region map using the given parameters.
///
fn update_nebula(
    pos: glm::IVec3,
    moved: bool,
    items: &mut [Option<Handle<Nebula>>],
    loader: &mut Loader<Nebula>,
    storage: &mut Storage<Nebula>,
) {
    if !moved {
        for (idx, item) in items.iter_mut().enumerate() {
            *item = gen_nebula(pos, idx, loader, storage);
        }
    }
}


/// # Resource
///
/// Data required to render nebulae on the star map.
///
/// __Main thread only!__
///
pub struct StarMapNebulaRenderData {
    vbo: GlDynamicBuffer<NebulaVertex>,
    vao: GlVertexArray,
    textures: SecondaryMap<Key, GlTexture3D<Rg8>>,
    shader: Handle<Shader>,
}

impl StarMapNebulaRenderData {
    fn new(
        nebula_map: &RegionMap<Option<Handle<Nebula>>>,
        cube: &UnitCubeMesh,
        world: &mut World,
    ) -> Self {
        let textures = Default::default();

        let nebulae = {
            let storage = world.resources.get::<Storage<Nebula>>()
                .expect("No Storage<Nebula> resource!");
            get_nebula_data(nebula_map, &storage, &textures)
        };

        let vbo = GlDynamicBuffer::with_capacity(nebula_map.items().len());
        vbo.upload(&nebulae);

        let mut vao = GlVertexArray::new();
        vao.attach_buffer(cube.vbo(), cube.format(), 0, cube.size());
        vao.attach_vertex_buffer(&vbo, 0);
        vao.attach_element_buffer(cube.ebo());

        Self {
            vbo,
            vao,
            textures,
            shader: load("shader/nebula.glsl", world),
        }
    }
}

/// Gets a vector of nebula vertices for currently loaded nebulae.
///
fn get_nebula_data(
    map: &RegionMap<Option<Handle<Nebula>>>,
    storage: &Storage<Nebula>,
    textures: &SecondaryMap<Key, GlTexture3D<Rg8>>,
) -> Vec<NebulaVertex> {
    map.items().iter()
        .enumerate()
        .filter_map(|(idx, handle)| {
            let handle = handle.as_ref()?;
            textures.get(handle.key())?;
            let nebula = storage.get(handle);

            if let Get::Finished(ResultRef::Ok(nebula)) = nebula {
                let region_index = map.cell_regions()[idx];
                return Some(NebulaVertex {
                    position: nebula.position,
                    size: nebula.size,
                    region_index,
                });
            }

            None
        })
        .collect()
}


/// # Resource
///
/// Used to capture star map region transition events for nebula region position update.
///
struct StarMapNebulaUpdateStarRegionTransitionEventReader(
    ReaderId<StarMapStarRegionTransitionEvent>
);


/// # Resource
///
/// Used to capture nebula region transition events for nebula update.
///
struct StarMapNebulaUpdateNebulaRegionTransitionEventReader(
    ReaderId<StarMapNebulaRegionTransitionEvent>
);


/// # Resource
///
/// An event reader used to capture nebula asset events for nebula upload.
///
struct StarMapNebulaUploadNebulaAssetEventReader(
    ReaderId<Event<Nebula>>
);


/// # Resource
///
/// An event reader used to capture nebula region transition events for nebula upload.
///
struct StarMapNebulaUploadNebulaRegionTransitionEventReader(
    ReaderId<StarMapNebulaRegionTransitionEvent>
);


/// # Resource
///
/// Common OpenGL buffers used for starmap rendering.
///
/// __Main thread only!__
///
pub struct StarMapCommonBuffers {
    cube: UnitCubeMesh,
    billboard: BillboardMesh,
}

impl Default for StarMapCommonBuffers {
    fn default() -> Self {
        Self {
            cube: UnitCubeMesh::new(),
            billboard: BillboardMesh::new(),
        }
    }
}


/// # Resource
///
/// Data required to render stars on the starmap.
///
/// __Main thread only!__
///
pub struct StarMapStarRenderData {
    vbo: GlDynamicBuffer<Star>,
    cell_region_vbo: GlDynamicBuffer<u32>,
    vao: GlVertexArray,
    rid: ReaderId<StarMapStarRegionTransitionEvent>,
    shader: Handle<Shader>,
}

impl StarMapStarRenderData {
    fn new(world: &mut World, stars: &RegionMap<Star>, billboard: &BillboardMesh) -> Self {
        let rid = world.resources
            .get_mut_or_default::<EventChannel<_>>()
            .unwrap()
            .register_reader();
        let vbo = GlDynamicBuffer::new(stars.items());

        let cell_region_vbo = GlDynamicBuffer::new(stars.cell_regions());
        let cell_region_format = [
            Attribute {
                size: AttributeSize::Scalar,
                kind: AttributeType::U32(AttributeNorm::NotNormalized),
                offset: 0,
                divisor: Some(stars.density() as _),
            }
        ];
        let cell_region_size = std::mem::size_of::<u32>();

        let mut vao = GlVertexArray::new();
        vao.attach_buffer(billboard.vbo(), billboard.format(), 0, billboard.size());
        vao.attach_vertex_buffer(&vbo, 0);
        vao.attach_buffer(&cell_region_vbo, &cell_region_format, 0, cell_region_size);
        let shader = load("shader/star.glsl", world);
        Self { vbo, cell_region_vbo, vao, rid, shader }
    }
}


/// # Event
///
/// Emitted when the starmap destination is changed or reached.
///
#[derive(Copy, Clone)]
pub enum StarMapDestinationEvent {
    DestinationChanged,
    DestinationReached,
}


/// # Event
///
/// Emitted when a star map star region transition occurs.
/// Holds the region coord delta vector.
///
#[derive(Copy, Clone)]
pub struct StarMapStarRegionTransitionEvent(pub glm::IVec3);


/// # Event
///
/// Emitted when a star map nebula region transition occurs.
/// Holds the region coord delta vector.
///
#[derive(Copy, Clone)]
pub struct StarMapNebulaRegionTransitionEvent(pub glm::IVec3);


/// # Initializer
///
/// Initializes resources used by starmap related systems.
///
pub fn init_star_map(coord: StarCoord, world: &mut World) {
    world.resources.insert(StarMapPosition(coord));
    world.resources.insert(StarMapDestination::default());
    world.resources.insert(EventChannel::<StarMapDestinationEvent>::default());

    world.resources.insert(EventChannel::<StarMapStarRegionTransitionEvent>::default());
    world.resources.insert(EventChannel::<StarMapNebulaRegionTransitionEvent>::default());

    let (star_update_star_reader, nebula_update_star_reader) = {
        let mut channel = world.resources
            .get_mut::<EventChannel<StarMapStarRegionTransitionEvent>>()
            .unwrap();
        let star_rid = channel.register_reader();
        let nebula_rid = channel.register_reader();
        (
            StarMapStarUpdateStarRegionTransitionEventReader(star_rid),
            StarMapNebulaUpdateStarRegionTransitionEventReader(nebula_rid),
        )
    };
    world.resources.insert(star_update_star_reader);
    world.resources.insert(nebula_update_star_reader);

    let (nebula_update_nebula_reader, nebula_upload_nebula_reader) = {
        let mut channel = world.resources
            .get_mut::<EventChannel<StarMapNebulaRegionTransitionEvent>>()
            .unwrap();
        let update_rid = channel.register_reader();
        let upload_rid = channel.register_reader();
        (
            StarMapNebulaUpdateNebulaRegionTransitionEventReader(update_rid),
            StarMapNebulaUploadNebulaRegionTransitionEventReader(upload_rid),
        )
    };
    world.resources.insert(nebula_update_nebula_reader);
    world.resources.insert(nebula_upload_nebula_reader);

    let nebula_upload_nebula_reader = {
        let mut channel = world.resources
            .get_mut::<EventChannel<Event<Nebula>>>()
            .unwrap();
        let rid = channel.register_reader();
        StarMapNebulaUploadNebulaAssetEventReader(rid)
    };
    world.resources.insert(nebula_upload_nebula_reader);

    let common_buffers = StarMapCommonBuffers::default();

    let stars = StarMapStars::new(glm::UVec3::repeat(15), coord.region, 3);
    let star_render_data = StarMapStarRenderData::new(
        world,
        &stars.region_map,
        &common_buffers.billboard,
    );
    world.resources.insert(star_render_data);
    world.resources.insert(stars);

    let nebula_region_position = StarMapNebulaRegionPosition(glm::zero());
    let nebulae = StarMapNebulae::new(glm::UVec3::repeat(5), nebula_region_position.0, world);
    let nebula_render_data = StarMapNebulaRenderData::new(
        &nebulae.region_map,
        &common_buffers.cube,
        world,
    );
    world.resources.insert(nebula_region_position);
    world.resources.insert(nebula_render_data);
    world.resources.insert(nebulae);

    world.resources.insert(common_buffers);
}


/// # System
///
/// Moves the starmap position in the `StarMapPosition` resource towards the position specified in
/// the `StarMapDestination` resource over the time specified travel time.
///
pub fn sys_star_map_travel_to_destination() -> Box<dyn Schedulable> {
    SystemBuilder::new("StarMapTravelToDestinationSystem")
        .read_resource::<DeltaTime>()
        .write_resource::<StarMapPosition>()
        .write_resource::<StarMapDestination>()
        .write_resource::<EventChannel<StarMapDestinationEvent>>()
        .build(move |_, _, (delta, position, destination, channel), _| {
            if let Some(coord) = destination.coord {
                if destination.initial.is_none() {
                    destination.initial = Some(position.0);
                    channel.single_write(StarMapDestinationEvent::DestinationChanged);
                }

                if let Some(initial) = destination.initial {
                    destination.timer += delta.as_secs();
                    let factor = destination.timer / destination.travel_time;
                    let clamped = factor.max(0.0).min(1.0);
                    let new = initial.lerp(&coord, clamped);
                    let delta = (new - position.0).to_f32_vec();
                    position.0 += delta;

                    if factor >= 1.0 {
                        destination.coord = None;
                        channel.single_write(StarMapDestinationEvent::DestinationReached);
                    }
                }
            }
        })
}


/// # System
///
/// Normalizes the coordinates stored inside the `StarMapPosition` resource.
/// Emits a `StarMapRegionTransitionEvent` when the current region changes.
///
pub fn sys_star_map_star_region_normalize() -> Box<dyn Schedulable> {
    SystemBuilder::new("StarMapStarRegionNormalizeSystem")
        .write_resource::<StarMapPosition>()
        .write_resource::<EventChannel<StarMapStarRegionTransitionEvent>>()
        .build(move |_, _, (position, channel), _| {
            let delta = position.0.normalize();
            if delta != glm::zero::<glm::IVec3>() {
                let event = StarMapStarRegionTransitionEvent(delta);
                channel.single_write(event);
            }
        })
}

/// # System
///
/// Capture star region transition events and update the nebula region position.
/// Emit `NebulaRegionTransitionEvent`s when the nebula region position changes.
///
pub fn sys_star_map_nebula_region_normalize() -> Box<dyn Schedulable> {
    SystemBuilder::new("StarMapNebulaRegionNormalizeSystem")
        .read_resource::<StarMapPosition>()
        .read_resource::<EventChannel<StarMapStarRegionTransitionEvent>>()
        .write_resource::<StarMapNebulaUpdateStarRegionTransitionEventReader>()
        .write_resource::<StarMapNebulaRegionPosition>()
        .write_resource::<EventChannel<StarMapNebulaRegionTransitionEvent>>()
        .build(move |_, _, (star_position, star_cnl, reader, nebula_position, nebula_cnl), _| {
            if star_cnl.read(&mut reader.0).count() > 0 {
                let nebula_position_new = get_nebula_region_position(star_position.0.region);
                let delta = nebula_position_new - nebula_position.0;
                if delta != glm::zero::<glm::IVec3>() {
                    nebula_position.0 = nebula_position_new;
                    nebula_cnl.single_write(StarMapNebulaRegionTransitionEvent(delta));
                }
            }
        })
}


/// Calculates the nebula region position from the star map region position.
///
pub fn get_nebula_region_position(star_map_region_pos: glm::IVec3) -> glm::IVec3 {
    let star_position: glm::Vec3 = glm::convert(star_map_region_pos);
    glm::IVec3::new(
        (star_position.x / NEBULA_REGION_SCALE as f32).floor() as i32,
        (star_position.y / NEBULA_REGION_SCALE as f32).floor() as i32,
        (star_position.z / NEBULA_REGION_SCALE as f32).floor() as i32,
    )
}


/// Converts a local position from star map space to nebula space.
///
fn to_nebula_local_space(star_map_pos: &StarCoord) -> glm::Vec3 {
    let nebula_reg = get_nebula_region_position(star_map_pos.region);
    let star_reg = star_map_pos.region;
    let region_remainder = glm::Vec3::new(
        (star_reg.x - nebula_reg.x * NEBULA_REGION_SCALE as i32) as f32 * STAR_REGION_SIZE_LY,
        (star_reg.y - nebula_reg.y * NEBULA_REGION_SCALE as i32) as f32 * STAR_REGION_SIZE_LY,
        (star_reg.z - nebula_reg.z * NEBULA_REGION_SCALE as i32) as f32 * STAR_REGION_SIZE_LY,
    );
    region_remainder + star_map_pos.local
}


/// # System
///
/// Regenerate starmap stars when a region transition occurs.
///
pub fn sys_star_map_star_update() -> Box<dyn Schedulable> {
    SystemBuilder::new("StarMapStarUpdateSystem")
        .read_resource::<EventChannel<StarMapStarRegionTransitionEvent>>()
        .write_resource::<StarMapStarUpdateStarRegionTransitionEventReader>()
        .write_resource::<StarMapStars>()
        .build(move |_, _, (channel, reader, stars), _| {
            for StarMapStarRegionTransitionEvent(delta) in channel.read(&mut reader.0) {
                stars.region_map.move_by(*delta, &mut update_stars);
                //spg_info!("[Star] Region moved by {},{},{}", delta.x, delta.y, delta.z);
            }
        })
}


/// # System
///
/// Captures `StarMapNebulaRegionTransitionEvent`s and updates the nebula region map.
///
pub fn sys_star_map_nebula_update() -> Box<dyn Schedulable> {
    SystemBuilder::new("StarMapNebulaUpdateSystem")
        .read_resource::<EventChannel<StarMapNebulaRegionTransitionEvent>>()
        .write_resource::<StarMapNebulae>()
        .write_resource::<StarMapNebulaUpdateNebulaRegionTransitionEventReader>()
        .write_resource::<Loader<Nebula>>()
        .write_resource::<Storage<Nebula>>()
        .build(move |_, _, (channel, nebulae, reader, loader, storage), _| {
            let mut updater = move |p, m, i: &mut _| update_nebula(p, m, i, loader, storage);
            for StarMapNebulaRegionTransitionEvent(delta) in channel.read(&mut reader.0) {
                nebulae.region_map.move_by(*delta, &mut updater);
                spg_info!("[Nebula] Region moved by {},{},{}", delta.x, delta.y, delta.z);
            }
        })
}


/// # System
///
/// Captures region transition events and uploads updated star data to the GPU.
///
/// __This system must be run on the main thread only!__
///
pub fn sys_star_map_star_upload() -> Box<dyn Runnable> {
    SystemBuilder::new("StarMapStarUploadSystem")
        .read_resource::<EventChannel<StarMapStarRegionTransitionEvent>>()
        .read_resource::<StarMapStars>()
        .write_resource::<StarMapStarRenderData>()
        .build_thread_local(move |_, _, (channel, stars, render_data), _| {
            for _ in channel.read(&mut render_data.rid) {
                render_data.vbo.upload(stars.region_map.items());
                render_data.cell_region_vbo.upload(stars.region_map.cell_regions());
            }
        })
}


/// # System
///
/// Captures nebula asset load events and uploads the noise textures to the GPU.
///
/// __Main thread only!__
///
pub fn sys_star_map_nebula_upload() -> Box<dyn Runnable> {
    SystemBuilder::new("StarMapNebulaUploadSystem")
        .read_resource::<EventChannel<Event<Nebula>>>()
        .read_resource::<EventChannel<StarMapNebulaRegionTransitionEvent>>()
        .read_resource::<Storage<Nebula>>()
        .read_resource::<StarMapNebulae>()
        .write_resource::<StarMapNebulaUploadNebulaAssetEventReader>()
        .write_resource::<StarMapNebulaUploadNebulaRegionTransitionEventReader>()
        .write_resource::<StarMapNebulaRenderData>()
        .build_thread_local(move |_, _, res, _| {
            let (asset_cnl, trans_cnl, storage, nebulae, asset_rid, trans_rid, render_data) = res;
            let mut update_data = false;
            for event in asset_cnl.read(&mut asset_rid.0) {
                match event {
                    Event::Finished(handle) => {
                        if let Get::Finished(ResultRef::Ok(nebula)) = storage.get(handle) {
                            let texture = {
                                let sx = nebula.noise_size.x;
                                let sy = nebula.noise_size.y;
                                let sz = nebula.noise_size.z;
                                let texture = GlTexture3D::<Rg8>::new(sx, sy, sz);
                                texture.set_mag_filter(TextureFilter::Nearest);
                                texture.set_wrap_x(TextureWrap::Clamp);
                                texture.set_wrap_y(TextureWrap::Clamp);
                                texture.set_wrap_z(TextureWrap::Clamp);
                                texture.upload_3d(0, 0, 0, sx, sy, sz, &nebula.noise_data);
                                spg_info!("[Nebula] Loaded with size {}x{}x{}!", sx, sy, sz);
                                texture
                            };
                            render_data.textures.insert(handle.key(), texture);
                            update_data = true;
                        }
                    }
                    Event::Deleted(handle) => {
                        spg_info!("[Nebula] Deleted!");
                        render_data.textures.remove(handle.key());
                        update_data = true;
                    }
                }
            }

            for _ in trans_cnl.read(&mut trans_rid.0) {
                update_data = true;
            }

            if update_data {
                let nebulae = get_nebula_data(&nebulae.region_map, storage, &render_data.textures);
                render_data.vbo.upload(&nebulae);
            }
        })
}


/// # System
///
/// Renders the star map stars.
///
/// __This system must be run from the main thread only!__
///
pub fn sys_star_map_star_render() -> Box<dyn Runnable> {
    SystemBuilder::new("StarMapStarRenderSystem")
        .read_resource::<StarMapStars>()
        .read_resource::<StarMapStarRenderData>()
        .read_resource::<ActiveCamera>()
        .read_resource::<Shaders>()
        .read_component::<Camera>()
        .build_thread_local(move |_, world, (stars, render_data, active_camera, shaders), _| {
            if let Some(shader) = shaders.get(&render_data.shader) {
                if let Some(camera) = world.get_component::<Camera>(active_camera.0) {
                    // Extent should be the same for all axes!
                    let extent = stars.region_map.extent().x;
                    let count = stars.region_map.items().len() as _;

                    render_data.vao.bind();

                    shader.bind();
                    shader.set_mat4(0, camera.combined());
                    shader.set_vec3(1, camera.position.coords);
                    shader.set_vec3(2, camera.get_right().into_inner());
                    shader.set_u32(3, extent);
                    shader.set_f32(4, STAR_REGION_SIZE_LY);

                    gl_call!("StarMapStarRenderSystem", {
                        gl::Disable(gl::DEPTH_TEST);
                        gl::Disable(gl::CULL_FACE);
                        gl::Enable(gl::BLEND);
                        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                        // Rendering a billboard which is defined as a fan with 4 vertices.
                        gl::DrawArraysInstanced(gl::TRIANGLE_FAN, 0, 4, count);
                    });
                }
            }
        })
}


/// # System
///
/// Renders all loaded star map nebulae.
///
/// __Main thread onyl!__
///
pub fn sys_star_map_nebula_render() -> Box<dyn Runnable> {
    SystemBuilder::new("StarMapNebulaRenderSystem")
        .read_resource::<StarMapNebulae>()
        .read_resource::<StarMapNebulaRenderData>()
        .read_resource::<StarMapPosition>()
        .read_resource::<Shaders>()
        .read_resource::<ActiveCamera>()
        .read_component::<Camera>()
        .build_thread_local(move |_, world, resources, _| {
            let (nebulae, render_data, position, shaders, active_camera) = resources;
            if let Some(shader) = shaders.get(&render_data.shader) {
                if let Some(camera) = world.get_component::<Camera>(active_camera.0) {
                    // Extent should be the same for all axes!
                    let extent = nebulae.region_map.extent().x;
                    // A cube has 6 * 2 * 3 = 36 elements.
                    let element_count = 6 * 2 * 3;

                    let camera = {
                        let mut camera = Camera::clone(&camera);
                        camera.position = na::Point3::from(to_nebula_local_space(&position.0));
                        camera.update();
                        camera
                    };

                    render_data.vao.bind();

                    shader.bind();
                    shader.set_mat4(0, camera.combined());
                    shader.set_u32(1, extent);
                    shader.set_f32(2, NEBULA_REGION_SIZE_LY);
                    shader.set_vec3(3, camera.position.coords);
                    for idx in 0..NEBULA_MAX_BATCH_SIZE {
                        shader.set_i32((4 + idx) as _, idx as _);
                    }

                    let loaded_nebulae = nebulae.region_map.items().iter()
                        .filter_map(|handle| handle.as_ref()
                            .map(|handle| render_data.textures.get(handle.key()))
                            .flatten())
                        .collect::<Vec<_>>();
                    for (chunk_idx, chunk) in loaded_nebulae.chunks(NEBULA_MAX_BATCH_SIZE)
                        .enumerate() {
                        for (texture_idx, texture) in chunk.iter().enumerate() {
                            texture.bind(texture_idx as _);
                        }

                        let instance_count = chunk.len() as _;
                        let base_instance = (chunk_idx * NEBULA_MAX_BATCH_SIZE) as _;

                        gl_call!("StarMapNebulaRenderSystem", {
                            gl::Enable(gl::DEPTH_TEST);
                            gl::Enable(gl::CULL_FACE);
                            gl::CullFace(gl::FRONT);
                            gl::Enable(gl::BLEND);
                            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                            gl::DrawElementsInstancedBaseInstance(
                                gl::TRIANGLES,
                                element_count,
                                gl::UNSIGNED_BYTE,
                                std::ptr::null(),
                                instance_count,
                                base_instance,
                            );
                        });
                    }
                }
            }
        })
}

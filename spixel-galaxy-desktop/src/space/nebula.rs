use rand::prelude::*;

use spg::{
    asset::{Asset, LoadTask, Progress, Proxy},
    graphics::{
        opengl::*,
        vertex::*,
    },
};

use crate::space::star_coord::STAR_REGION_SIZE_LY;

pub const NEBULA_REGION_SCALE: u32 = 10;
pub const NEBULA_REGION_SIZE_LY: f32 = STAR_REGION_SIZE_LY * NEBULA_REGION_SCALE as f32;

pub struct Nebula {
    pub position: glm::Vec3,
    pub size: glm::Vec3,
    pub noise_size: glm::UVec3,
    pub noise_data: Vec<u8>,
}

impl Asset for Nebula {
    type Id = glm::IVec3;

    fn loader(pos: Self::Id, progress: Progress, proxy: Proxy<Self>) -> LoadTask<Self> {
        spg::futures::FutureExt::boxed(async move {
            let seed = pos.x as u64 * 123456 + pos.y as u64 * 456789 + pos.z as u64 * 789123;
            let mut rng = StdRng::seed_from_u64(seed);

            let position = glm::Vec3::new(
                rng.gen::<f32>() * NEBULA_REGION_SIZE_LY,
                rng.gen::<f32>() * NEBULA_REGION_SIZE_LY,
                rng.gen::<f32>() * NEBULA_REGION_SIZE_LY,
            );

            let size = glm::Vec3::new(
                rng.gen_range(0.2 as f32, 1.0) * NEBULA_REGION_SIZE_LY,
                rng.gen_range(0.2 as f32, 1.0) * NEBULA_REGION_SIZE_LY,
                rng.gen_range(0.2 as f32, 1.0) * NEBULA_REGION_SIZE_LY,
            );

            let noise_size = glm::UVec3::new(
                size.x.ceil() as _,
                size.y.ceil() as _,
                size.z.ceil() as _,
            );

            let noise_data = {
                let offset_x = rng.gen_range(0.0 as f32, 1_000_000.0);
                let offset_y = rng.gen_range(0.0 as f32, 1_000_000.0);
                let offset_z = rng.gen_range(0.0 as f32, 1_000_000.0);

                let shape = {
                    let mut builder = spg::noise::NoiseBuilder::fbm_3d_offset(
                        offset_x, noise_size.x as usize,
                        offset_y, noise_size.y as usize,
                        offset_z, noise_size.z as usize,
                    );
                    let frequency = rng.gen_range(0.01, 0.05);
                    let gain = rng.gen_range(0.2, 0.5);
                    let lacunarity = rng.gen_range(2.0, 10.0);
                    builder
                        .with_freq(frequency)
                        .with_gain(gain)
                        .with_lacunarity(lacunarity)
                        .with_octaves(3)
                        .generate_scaled(0.0, 1.0)
                };

                let color = {
                    let mut builder = spg::noise::NoiseBuilder::ridge_3d_offset(
                        offset_x, noise_size.x as usize,
                        offset_y, noise_size.y as usize,
                        offset_z, noise_size.z as usize,
                    );
                    let frequency = rng.gen_range(0.001, 0.05);
                    let gain = rng.gen_range(0.2, 0.5);
                    let lacunarity = rng.gen_range(4.0, 25.0);
                    builder
                        .with_freq(frequency)
                        .with_gain(gain)
                        .with_lacunarity(lacunarity)
                        .with_octaves(2)
                        .generate_scaled(0.0, 1.0)
                };

                shape.into_iter().zip(color)
                    .flat_map(|(shape, color)| {
                        let shape = (shape * 255.0) as u8;
                        let color = (color * 255.0) as u8;
                        std::iter::once(shape).chain(std::iter::once(color))
                    })
                    .collect::<Vec<_>>()
            };

            Ok(Self { position, size, noise_size, noise_data })
        })
    }
}


/// # Data
///
/// A vertex type used to render a nebula.
///
#[derive(Copy, Clone)]
#[repr(packed)]
pub struct NebulaVertex {
    pub position: glm::Vec3,
    pub size: glm::Vec3,
    pub region_index: u32,
}

impl Vertex for NebulaVertex {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            size: AttributeSize::Vec3,
            kind: AttributeType::F32,
            offset: 0,
            divisor: Some(1),
        },
        Attribute {
            size: AttributeSize::Vec3,
            kind: AttributeType::F32,
            offset: 4 * 3,
            divisor: Some(1),
        },
        Attribute {
            size: AttributeSize::Scalar,
            kind: AttributeType::U32(AttributeNorm::NotNormalized),
            offset: 4 * 3 + 4 * 3,
            divisor: Some(1),
        },
    ];
}

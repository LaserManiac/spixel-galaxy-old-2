//! TODO: Comment

use spg::glm;

/// The edge size of a star coordinates region measured in light years.
///
pub const STAR_REGION_SIZE_LY: f32 = 10.0;

/// The halved edge size of a star coordinates region measured in light years.
///
pub const REGION_HALF_SIZE_LY: f32 = STAR_REGION_SIZE_LY * 0.5;


#[derive(Copy, Clone)]
pub struct StarCoord {
    pub region: glm::IVec3,
    pub local: glm::Vec3,
}

impl Default for StarCoord {
    fn default() -> Self {
        Self {
            region: glm::IVec3::repeat(0),
            local: glm::Vec3::repeat(REGION_HALF_SIZE_LY),
        }
    }
}

impl StarCoord {
    pub fn local(x: f32, y: f32, z: f32) -> Self {
        Self {
            region: glm::IVec3::repeat(0),
            local: glm::Vec3::new(x, y, z),
        }
    }

    pub fn region(x: i32, y: i32, z: i32) -> Self {
        Self {
            region: glm::IVec3::new(x, y, z),
            local: glm::Vec3::repeat(0.0),
        }
    }

    pub fn normalize(&mut self) -> glm::IVec3 {
        let rs = STAR_REGION_SIZE_LY;
        let dx = (self.local.x / rs).floor();
        let dy = (self.local.y / rs).floor();
        let dz = (self.local.z / rs).floor();
        let region_delta = glm::IVec3::new(dx as _, dy as _, dz as _);
        self.local -= glm::Vec3::new(dx, dy, dz) * rs;
        self.region += region_delta;
        region_delta
    }

    pub fn normalized(&self) -> StarCoord {
        let mut normalized = *self;
        normalized.normalize();
        normalized
    }

    pub fn to_f32_vec(&self) -> glm::Vec3 {
        glm::Vec3::new(
            self.region.x as f32 * STAR_REGION_SIZE_LY + self.local.x,
            self.region.y as f32 * STAR_REGION_SIZE_LY + self.local.y,
            self.region.z as f32 * STAR_REGION_SIZE_LY + self.local.z,
        )
    }

    pub fn lerp(&self, other: &StarCoord, a: f32) -> StarCoord {
        let delta = *other - *self;
        let offset = delta.to_f32_vec() * a;
        let new = *self + offset;
        new.normalized()
    }
}

/// Add star coordinates.
///
impl std::ops::Add for StarCoord {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            region: self.region + rhs.region,
            local: self.local + rhs.local,
        }
    }
}

/// Add assign star coordinates.
///
impl std::ops::AddAssign for StarCoord {
    fn add_assign(&mut self, rhs: Self) {
        self.region += rhs.region;
        self.local += rhs.local;
    }
}

/// Subtract star coordinates.
///
impl std::ops::Sub for StarCoord {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            region: self.region - rhs.region,
            local: self.local - rhs.local,
        }
    }
}

/// Subtract assign star coordinates.
///
impl std::ops::SubAssign for StarCoord {
    fn sub_assign(&mut self, rhs: Self) {
        self.region -= rhs.region;
        self.local -= rhs.local;
    }
}

/// Add local coordinates to star coordinates.
///
impl std::ops::Add<glm::Vec3> for StarCoord {
    type Output = Self;

    fn add(self, rhs: glm::Vec3) -> Self::Output {
        Self {
            region: self.region,
            local: self.local + rhs,
        }
    }
}

/// Add assign local coordinates to star coordinates.
///
impl std::ops::AddAssign<glm::Vec3> for StarCoord {
    fn add_assign(&mut self, rhs: glm::Vec3) {
        self.local += rhs;
    }
}

/// Subtract local coordinates from star coordinates.
///
impl std::ops::Sub<glm::Vec3> for StarCoord {
    type Output = Self;

    fn sub(self, rhs: glm::Vec3) -> Self::Output {
        Self {
            region: self.region,
            local: self.local - rhs,
        }
    }
}

/// Subtract assign local coordinates from star coordinates.
///
impl std::ops::SubAssign<glm::Vec3> for StarCoord {
    fn sub_assign(&mut self, rhs: glm::Vec3) {
        self.local -= rhs;
    }
}

/// Add region coordinates to star coordinates.
///
impl std::ops::Add<glm::IVec3> for StarCoord {
    type Output = Self;

    fn add(self, rhs: glm::IVec3) -> Self::Output {
        Self {
            region: self.region + rhs,
            local: self.local,
        }
    }
}

/// Add region local coordinates to star coordinates.
///
impl std::ops::AddAssign<glm::IVec3> for StarCoord {
    fn add_assign(&mut self, rhs: glm::IVec3) {
        self.region += rhs;
    }
}

/// Subtract region coordinates from star coordinates.
///
impl std::ops::Sub<glm::IVec3> for StarCoord {
    type Output = Self;

    fn sub(self, rhs: glm::IVec3) -> Self::Output {
        Self {
            region: self.region - rhs,
            local: self.local,
        }
    }
}

/// Subtract region local coordinates from star coordinates.
///
impl std::ops::SubAssign<glm::IVec3> for StarCoord {
    fn sub_assign(&mut self, rhs: glm::IVec3) {
        self.region -= rhs;
    }
}

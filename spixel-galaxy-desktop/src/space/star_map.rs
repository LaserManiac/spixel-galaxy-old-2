//! TODO: Comment

use std::collections::HashSet;

use legion::prelude::*;
use rand::prelude::*;
use shrev::*;

use spg::{
    asset::{Event, Handle, load, Loader, Storage},
    graphics::{
        camera::Camera,
        opengl::*,
        shader::{Shader, Shaders},
        vertex::*,
    },
    util::region_map::RegionMap,
};

use crate::{
    gfx::{
        billboard::BillboardMesh,
        cube::UnitCubeMesh,
    },
    space::{
        nebula::{Nebula, NebulaVertex},
        star_coord::{STAR_REGION_SIZE_LY, StarCoord},
    },
};

#[derive(Copy, Clone, Default)]
#[repr(packed)]
pub struct Star {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub seed: u32,
}

impl Vertex for Star {
    const FORMAT: &'static [Attribute] = &[
        Attribute {
            size: AttributeSize::Vec4,
            kind: AttributeType::F32,
            offset: 0,
            divisor: Some(1),
        }
    ];
}


pub struct StarMap {
    cube_mesh: UnitCubeMesh,
    cube_vao: GlVertexArray,

    billboard_mesh: BillboardMesh,

    bcg_galaxies_vbo: GlStaticBuffer<glm::Vec4>,
    bcg_galaxies_vao: GlVertexArray,
    bcg_galaxies_shader: Handle<Shader>,

    galaxy_noise: GlTexture2D<Rg8>,
    galaxy_shader: Handle<Shader>,

    stars_dirty: bool,
    stars: RegionMap<Star>,
    stars_vbo: GlDynamicBuffer<Star>,
    stars_cell_region_vbo: GlDynamicBuffer<u32>,
    stars_vao: GlVertexArray,
    stars_shader: Handle<Shader>,

    nebulae_dirty: bool,
    nebulae: RegionMap<Option<Handle<Nebula>>>,
    nebulae_vbo: GlDynamicBuffer<NebulaVertex>,
    nebulae_vao: GlVertexArray,
    nebulae_shader: Handle<Shader>,
}

impl StarMap {
    pub fn new(region_extent: u32, region_density: u32, world: &mut World) -> Self {
        // ==== Billboard mesh ====
        let billboard_mesh = BillboardMesh::new();


        // ==== Unit cube mesh ====
        let cube_mesh = UnitCubeMesh::new();
        let mut cube_vao = GlVertexArray::new();
        cube_vao.attach_buffer(cube_mesh.vbo(), cube_mesh.format(), 0, cube_mesh.size());
        cube_vao.attach_element_buffer(cube_mesh.ebo());


        // ==== Background galaxy noise ====
        let galaxy_noise = {
            const NW: u32 = 8192;
            const NH: u32 = 2048;
            let noise_a = spg::noise::NoiseBuilder::fbm_2d(NW as _, NH as _)
                .with_freq(0.002)
                .with_lacunarity(2.5)
                .with_gain(0.4)
                .with_octaves(5)
                .generate_scaled(0.0, 1.0);
            let noise_b = spg::noise::NoiseBuilder::fbm_2d(NW as _, NH as _)
                .with_freq(0.01)
                .with_lacunarity(3.0)
                .with_gain(0.3)
                .with_octaves(3)
                .generate_scaled(0.0, 1.0);
            let noise = noise_a.into_iter()
                .zip(noise_b)
                .flat_map(|(a, b)|
                    std::iter::once((a * 255.0) as u8)
                        .chain(std::iter::once((b * 255.0) as u8)))
                .collect::<Vec<_>>();
            let texture = GlTexture2D::new(NW, NH);
            texture.set_mag_filter(TextureFilter::Linear);
            texture.upload_2d(0, 0, NW, NH, &noise);
            texture
        };


        // ==== Background galaxies ====
        let bcg_galaxies_vbo = {
            let mut rng = spg::rand::rngs::StdRng::seed_from_u64(0);
            let galaxies = (0..2000).into_iter()
                .map(|idx| {
                    let x = rng.gen::<f32>() * 2.0 - 1.0;
                    let y = rng.gen::<f32>() * 2.0 - 1.0;
                    let z = rng.gen::<f32>() * 2.0 - 1.0;
                    let pos = glm::normalize(&glm::Vec3::new(x, y, z));
                    let seed: f32 = unsafe { std::mem::transmute(idx) };
                    glm::Vec4::new(pos.x, pos.y, pos.z, seed)
                })
                .collect::<Vec<_>>();
            GlStaticBuffer::new(&galaxies)
        };
        let bcg_galaxies_format = [
            Attribute {
                size: AttributeSize::Vec4,
                kind: AttributeType::F32,
                offset: 0,
                divisor: Some(1),
            }
        ];
        let bcg_galaxies_size = std::mem::size_of::<glm::Vec4>();
        let mut bcg_galaxies_vao = GlVertexArray::new();
        bcg_galaxies_vao.attach_buffer(
            billboard_mesh.vbo(),
            billboard_mesh.format(),
            0,
            billboard_mesh.size(),
        );
        bcg_galaxies_vao.attach_buffer(
            &bcg_galaxies_vbo,
            &bcg_galaxies_format,
            0,
            bcg_galaxies_size,
        );


        // ==== Stars ====
        let mut stars = RegionMap::new(
            glm::UVec3::repeat(region_extent),
            region_density as usize,
            glm::zero(),
            &mut Self::gen_star,
        );

        let stars_vbo = GlDynamicBuffer::new(stars.items());
        let stars_dirty = true;

        let stars_cell_region_vbo = GlDynamicBuffer::new(stars.cell_regions());
        let stars_cell_region_size = std::mem::size_of::<u32>();
        let stars_cell_region_format = [
            Attribute {
                size: AttributeSize::Scalar,
                kind: AttributeType::U32(AttributeNorm::NotNormalized),
                offset: 0,
                divisor: Some(region_density),
            }
        ];

        let mut stars_vao = GlVertexArray::new();
        stars_vao.attach_buffer(
            billboard_mesh.vbo(),
            billboard_mesh.format(),
            0,
            billboard_mesh.size(),
        );
        stars_vao.attach_vertex_buffer(&stars_vbo, 0);
        stars_vao.attach_buffer(
            &stars_cell_region_vbo,
            &stars_cell_region_format,
            0,
            stars_cell_region_size,
        );


        // ==== Nebulae ====
        let mut nebulae = RegionMap::new(
            glm::UVec3::repeat(10),
            1,
            glm::zero(),
            &mut Self::gen_nebula,
        );

        let nebulae_vbo = GlDynamicBuffer::with_capacity(nebulae.cell_regions().len());
        let nebulae_dirty = true;

        let mut nebulae_vao = GlVertexArray::new();
        nebulae_vao.attach_buffer(
            cube_mesh.vbo(),
            cube_mesh.format(),
            0,
            cube_mesh.size(),
        );
        nebulae_vao.attach_vertex_buffer(&nebulae_vbo, 0);
        nebulae_vao.attach_element_buffer(cube_mesh.ebo());


        Self {
            cube_mesh,
            cube_vao,

            billboard_mesh,

            galaxy_noise,
            galaxy_shader: load("shader/galaxy.glsl", world),

            bcg_galaxies_vbo,
            bcg_galaxies_vao,
            bcg_galaxies_shader: load("shader/bcg_galaxies.glsl", world),

            stars_dirty,
            stars,
            stars_vbo,
            stars_cell_region_vbo,
            stars_vao,
            stars_shader: load("shader/star.glsl", world),

            nebulae_dirty,
            nebulae,
            nebulae_vbo,
            nebulae_vao,
            nebulae_shader: load("shader/nebula.glsl", world),
        }
    }

    pub fn stars(&self) -> &[Star] {
        self.stars.items()
    }

    pub fn view_distance(&self) -> f32 {
        self.stars.view_distance(STAR_REGION_SIZE_LY)
    }

    pub fn update(&mut self, coord: &StarCoord) {
        let start = std::time::Instant::now();

        self.stars_dirty = self.stars.move_to(coord.region, &mut Self::update_stars);
        self.nebulae_dirty = self.nebulae.move_to(coord.region, &mut Self::update_nebula);

        if self.stars_dirty || self.nebulae_dirty {
            let elapsed = std::time::Instant::now() - start;
            let c = coord.region;
            let us = elapsed.as_micros();
            spg_info!("Moved starmap to [{},{},{}] in {}us", c.x, c.y, c.z, us);
        }
    }

    pub fn draw(&mut self, camera: &Camera, shaders: &Shaders) {
        let centered_transform = camera.combined() * glm::translation(&camera.position.coords);
        let camera_right = camera.get_right().into_inner();

        if let Some(shader) = shaders.get(&self.bcg_galaxies_shader) {
            self.bcg_galaxies_vao.bind();
            shader.bind();
            shader.set_mat4(0, &centered_transform);
            shader.set_vec3(1, camera_right);
            let count = self.bcg_galaxies_vbo.size() as _;
            gl_call!("StarMap::draw background galaxies", {
                gl::Disable(gl::DEPTH_TEST);
                gl::Enable(gl::CULL_FACE);
                gl::Enable(gl::BLEND);
                gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                gl::DrawArraysInstanced(gl::TRIANGLE_FAN, 0, 4, count);
            });
        }

        if let Some(shader) = shaders.get(&self.galaxy_shader) {
            self.galaxy_noise.bind(0);
            self.cube_vao.bind();
            shader.bind();
            shader.set_mat4(0, &centered_transform);
            shader.set_i32(1, 0);
            let count = self.cube_mesh.ebo().size() as _;
            gl_call!("StarMap::draw galaxy", {
                gl::Disable(gl::DEPTH_TEST);
                gl::Enable(gl::CULL_FACE);
                gl::Enable(gl::BLEND);
                gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                gl::DrawElements(gl::TRIANGLES, count, gl::UNSIGNED_BYTE, std::ptr::null());
            });
        }

        if let Some(shader) = shaders.get(&self.stars_shader) {
            if self.stars_dirty {
                self.stars_vbo.upload(&self.stars.items());
                self.stars_cell_region_vbo.upload(&self.stars.cell_regions());
                self.stars_dirty = false;
            }
            self.stars_vao.bind();
            shader.bind();
            shader.set_mat4(0, camera.combined());
            shader.set_vec3(1, camera.position.coords);
            shader.set_vec3(2, camera_right);
            shader.set_u32(3, self.stars.extent().x); // Extent should be a cube!
            shader.set_f32(4, STAR_REGION_SIZE_LY);
            let count = self.stars.items().len() as _;
            gl_call!("StarMap::draw stars", {
                gl::Disable(gl::DEPTH_TEST);
                gl::Enable(gl::CULL_FACE);
                gl::Enable(gl::BLEND);
                gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                gl::DrawArraysInstanced(gl::TRIANGLE_FAN, 0, 4, count);
            });
        }

        /*if let Some(shader) = shaders.get(&self.nebulae_shader) {
            if self.nebulae_dirty {
                let nebulae = self.nebulae.items()
                    .iter()
                    .zip(self.nebulae.cell_regions())
                    .filter_map(|(nebula, cell_region)| {
                        nebula.as_ref().map(|nebula| {
                            NebulaVertex {
                                position: [
                                    nebula.position.x,
                                    nebula.position.y,
                                    nebula.position.z,
                                ],
                                size: [
                                    nebula.size.x,
                                    nebula.size.y,
                                    nebula.size.z,
                                ],
                                texture_index: 0,
                                region_index: *cell_region,
                            }
                        })
                    })
                    .collect::<Vec<_>>();
                self.nebulae_vbo.upload(&nebulae);
                self.nebulae_dirty = false;
            }
            self.nebulae_vao.bind();
            shader.bind();
            shader.set_mat4(0, camera.combined());
            shader.set_u32(1, self.nebulae.extent().x); // Extent should be a cube!
            shader.set_f32(2, REGION_SIZE_LY);
            let element_count = self.cube_mesh.ebo().size() as _;
            let instance_count = self.nebulae.items().iter().filter(|n| n.is_some()).count() as _;
            gl_call!("StarMap::draw nebulae", {
                gl::Disable(gl::DEPTH_TEST);
                gl::Enable(gl::CULL_FACE);
                gl::Enable(gl::BLEND);
                gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                gl::DrawElementsInstanced(
                    gl::TRIANGLE_FAN,
                    element_count,
                    gl::UNSIGNED_BYTE,
                    std::ptr::null(),
                    instance_count
                );
            });
        }*/
    }

    fn gen_star(pos: glm::IVec3, idx: usize) -> Star {
        let seed = pos.x as u64 * 987654
            + pos.y as u64 * 456321
            + pos.z as u64 * 789654
            + idx as u64 * 987541;
        let mut rng = StdRng::seed_from_u64(seed);
        Star {
            x: rng.gen::<f32>() * STAR_REGION_SIZE_LY,
            y: rng.gen::<f32>() * STAR_REGION_SIZE_LY,
            z: rng.gen::<f32>() * STAR_REGION_SIZE_LY,
            seed: rng.gen(),
        }
    }

    fn update_stars(pos: glm::IVec3, moved: bool, cell: &mut [Star]) {
        if !moved {
            for (idx, star) in cell.iter_mut().enumerate() {
                *star = Self::gen_star(pos, idx);
            }
        }
    }

    fn gen_nebula(pos: glm::IVec3, _: usize) -> Option<Handle<Nebula>> {
        let seed = pos.x as u64 * 987654
            + pos.y as u64 * 456321
            + pos.z as u64 * 789654;
        let mut rng = StdRng::seed_from_u64(seed);
        if rng.gen_ratio(1, 2500) {
            None
        } else {
            None
        }
    }

    fn update_nebula(pos: glm::IVec3, moved: bool, cell: &mut [Option<Handle<Nebula>>]) {
        if !moved {
            cell[0] = Self::gen_nebula(pos, 0);
        }
    }
}

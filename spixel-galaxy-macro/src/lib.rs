extern crate proc_macro;

use std::str::FromStr;

use proc_macro::TokenStream;


#[proc_macro_attribute]
pub fn debug(attr: TokenStream, item: TokenStream) -> TokenStream {
    if attr.into_iter().count() != 0 {
        panic!("The 'client' attribute does not take any parameters!");
    }
    let mut attr = TokenStream::from_str("#[cfg(feature = \"debug\")]").unwrap();
    attr.extend(item);
    attr
}

#[proc_macro_attribute]
pub fn release(attr: TokenStream, item: TokenStream) -> TokenStream {
    if attr.into_iter().count() != 0 {
        panic!("The 'client' attribute does not take any parameters!");
    }
    let mut attr = TokenStream::from_str("#[cfg(not(feature = \"debug\"))]").unwrap();
    attr.extend(item);
    attr
}
